<?php
    class Main
    {
        public function getWebsiteStatus($host) {
            if (@explode(';', $host)[1] == "maint") {
                return "2";
            }
            if ($socket =@ fsockopen($host, 80, $errno, $errstr, 30)) {
                fclose($socket);
                return "1";
            } else {
                return "0";
            }
        }

    }