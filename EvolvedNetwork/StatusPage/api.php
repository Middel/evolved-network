<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: *");
    header("Content-type: application/json; charset=utf-8");

    include "config.php";
    include "functions.php";
    $status = new Main();

    foreach ($websites as $website => $url) {
        $websiteslist[] = array (
            "website" => $website,
            "status" => $status->getWebsiteStatus($url)
        );
    }

    $return[] = array (
        "websites" => $websiteslist
    );

    echo json_encode($return);