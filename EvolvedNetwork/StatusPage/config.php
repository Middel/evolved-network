<?php
    $websites = [
        "evolved-network.com" => "evolved-network.com",
        "evolvedpve.com" => "evolvedpve.com",
        "evolvedwow.com" => "evolvedwow.com",
        "EvolvedPVE (Rust)" => "evolvedpve.com",
        "EvolvedWoW (World of Warcraft)" => "evolvedwow.com",
    ];

    $types = [
        "Online" => "<p class=\"online\">Online</p>",
        "Maint" => "<p class=\"maint\">Restarting</p>",
        "Offline" => "<p class=\"offline\">Offline</p>"
    ];
    return $ips;