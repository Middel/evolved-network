using Oxide.Game.Rust.Cui;
using Oxide.Core.Plugins;
using System;
using System.Linq;
using Oxide.Core;
using Oxide.Core.Libraries.Covalence;
using System.Text;
using UnityEngine;
using Newtonsoft.Json;
using Oxide.Plugins;
using System.Collections.Generic;

namespace Oxide.Plugins
{
    [Info("HomeProtection", "Skillz", "1.0.0")]
    [Description("Provides home protection to bases at the cost of a resource.")]
    partial class HomeProtection : CovalencePlugin
    {
        public static HomeProtection PLUGIN = null;

        [PluginReference]
        private readonly Plugin ImageLibrary;

        [PluginReference]
        private readonly Plugin Economics;

        [PluginReference]
        private readonly Plugin ServerRewards;

        [PluginReference]
        private readonly Plugin CustomStatusFramework;

        [PluginReference]
        private readonly Plugin Notify;

        [PluginReference]
        private readonly Plugin ZoneManager;

        [PluginReference]
        private readonly Plugin Clans;

        private bool PluginLoaded = true;

        private bool ShowConfigWarnings = true;

        public const string PermissionAdmin = "homeprotection.admin";
        public readonly string PermissionLevel = "homeprotection.level.";

        private void OnServerInitialized()
        {
            PLUGIN = this;
            DependencyCheck();
            if (!PluginLoaded) { return; }
            if (!permission.PermissionExists(PermissionAdmin, this))
            {
                permission.RegisterPermission(PermissionAdmin, this);
            }
            AddCovalenceCommand(config.Commands.Admin, nameof(CmdController));
            AddCovalenceCommand(config.Commands.Levels, nameof(UILevelsShow));
            AddCovalenceCommand(config.Commands.Protection, nameof(TcInfoShowPlayer));
            InitProtectionLevels();
            LoadImages();
            LoadProtectedCupboards();
            FindNewCupboards();
            InitIntegrations();
            SubscribePlugins();
            if (config.EnableLogging)
            {
                PrintWarning("WARNING: You have logging ENABLED, this will generate large amount of logs for every tool cupboard on your server. It is recommended to have this enabled only for debugging purposes.");
            }
        }

        private void UnsubscribePlugins()
        {
            Unsubscribe(nameof(OnServerSave));
            Unsubscribe(nameof(OnPluginLoaded));
            Unsubscribe(nameof(OnPluginUnloaded));
            Unsubscribe(nameof(OnEntitySpawned));
            Unsubscribe(nameof(OnEntityBuilt));
            Unsubscribe(nameof(OnEntityKill));
            Unsubscribe(nameof(OnEntityDeath));
            Unsubscribe(nameof(OnCupboardAuthorize));
            Unsubscribe(nameof(OnCupboardClearList));
            Unsubscribe(nameof(OnCupboardDeauthorize));
            Unsubscribe(nameof(OnEntityTakeDamage));
            Unsubscribe(nameof(OnLootEntity));
            Unsubscribe(nameof(OnLootEntityEnd));
            Unsubscribe(nameof(OnLootNetworkUpdate));
            Unsubscribe(nameof(OnPlayerConnected));
            Unsubscribe(nameof(OnPlayerDisconnected));
            Unsubscribe(nameof(OnUserPermissionGranted));
            Unsubscribe(nameof(OnUserPermissionRevoked));
            Unsubscribe(nameof(OnGroupPermissionGranted));
            Unsubscribe(nameof(OnGroupPermissionRevoked));
            Unsubscribe(nameof(OnPlayerKicked));
        }

        private void SubscribePlugins()
        {
            Subscribe(nameof(OnServerSave));
            Subscribe(nameof(OnPluginLoaded));
            Subscribe(nameof(OnPluginUnloaded));
            Subscribe(nameof(OnEntitySpawned));
            Subscribe(nameof(OnEntityBuilt));
            Subscribe(nameof(OnEntityKill));
            Subscribe(nameof(OnEntityDeath));
            Subscribe(nameof(OnCupboardAuthorize));
            Subscribe(nameof(OnCupboardClearList));
            Subscribe(nameof(OnCupboardDeauthorize));
            Subscribe(nameof(OnEntityTakeDamage));
            Subscribe(nameof(OnLootEntity));
            Subscribe(nameof(OnLootEntityEnd));
            Subscribe(nameof(OnLootNetworkUpdate));
            Subscribe(nameof(OnPlayerConnected));
            Subscribe(nameof(OnPlayerDisconnected));
            Subscribe(nameof(OnUserPermissionGranted));
            Subscribe(nameof(OnUserPermissionRevoked));
            Subscribe(nameof(OnGroupPermissionGranted));
            Subscribe(nameof(OnGroupPermissionRevoked));
            Subscribe(nameof(OnPlayerKicked));
        }

        private void Unload()
        {
            SaveProtectedCupboards();
            UnloadIntegrations();
            UnsubscribePlugins();
        }

        void OnServerSave()
        {
            SaveProtectedCupboards();
        }

        void OnServerShutdown()
        {
            SaveProtectedCupboards();
        }

        void OnPluginLoaded(Plugin name)
        {
            if (name?.Name == this.Name) { return; }
            ShowConfigWarnings = false;
            LoadConfig();
            DependencyCheck();
            InitIntegrations();
        }

        void OnPluginUnloaded(Plugin name)
        {
            if (name?.Name == this.Name) { return; }
            DependencyCheck();
        }

        private void DependencyCheck()
        {
            timer.In(1f, () => // delay incase plugin has been reloaded
            {
                if (!ImageLibrary?.IsLoaded ?? true)
                {
                    PrintError($"The required dependency ImageLibary is not installed, {Name} will not work properly without it.");
                    PluginLoaded = false;
                    return;
                }
                var format = "Integration with '{0}' is set to 'true' in your configuration file, however this plugin is not installed on this server. This option will be temporarily set to 'false' to avoid issues. Once this plugin is installed, you will no longer see this warning and your configuration settings will be followed.";
                if (config.Integration.ServerRewards && (!ServerRewards?.IsLoaded ?? true))
                {
                    PrintWarning(String.Format(format, "ServerRewards"));
                    config.Integration.ServerRewards = false;
                }
                if (config.Integration.Economics && (!Economics?.IsLoaded ?? true))
                {
                    PrintWarning(String.Format(format, "Economics"));
                    config.Integration.Economics = false;
                }
                if (config.Integration.CustomStatusFramework && (!CustomStatusFramework?.IsLoaded ?? true))
                {
                    PrintWarning(String.Format(format, "Custom Status Framework"));
                    config.Integration.CustomStatusFramework = false;
                }
                if (config.Integration.Clans && (!Clans?.IsLoaded ?? true))
                {
                    PrintWarning(String.Format(format, "Clans"));
                    config.Integration.Clans = false;
                }
                if (config.Integration.Economics && config.Integration.ServerRewards)
                {
                    PrintWarning("Integration is set to 'true' for both 'Economics' and 'ServerRewards' in your configuration file. Only one of these plugins can be used for currency, please set only one of these to 'true' to avoid issues. Both will be disabled until this is resolved.");
                    config.Integration.ServerRewards = false;
                    config.Integration.Economics = false;
                }
            });
        }

        private void InitIntegrations()
        {
            try
            {
                if (config.Integration.CustomStatusFramework)
                {
                    CustomStatusFrameworkHelper.CreateProtectionStatus();
                }
            } catch (Exception) { }
        }

        private void UnloadIntegrations()
        {
            if (config.Integration.CustomStatusFramework)
            {
                CustomStatusFrameworkHelper.DeleteProtectionStatus();
            }
        }

        private void InitProtectionLevels()
        {
            try
            {
                foreach(var level in config.Protection.ProtectionLevels)
                {
                    var permString = PermissionLevel + level.Rank;
                    if (!permission.PermissionExists(permString, this))
                    {
                        permission.RegisterPermission(permString, this);
                    }
                    if (config.EnableConsoleMessages)
                    {
                        Puts($"Registered permission {permString}");
                    }
                }
            }
            catch
            {
                PrintError("Failed to load protection levels from config file, make sure they are properly formatted.");
            }
        }

        private void LoadImages()
        {
            ImageLibrary.Call<bool>("AddImage", config.Images.StatusProtected, $"status.protected", 0UL);
            ImageLibrary.Call<bool>("AddImage", config.Images.StatusUnprotected, $"status.unprotected", 0UL);
            ImageLibrary.Call<bool>("AddImage", config.Images.StatusInfo, $"status.info", 0UL);
            ImageLibrary.Call<bool>("AddImage", config.Images.StatusToggle, $"status.toggle", 0UL);
            ImageLibrary.Call<bool>("AddImage", config.Images.StatusRefresh, $"status.refresh", 0UL);
            ImageLibrary.Call<bool>("AddImage", config.Images.InfoOwners, $"rp.owners", 0UL);
            ImageLibrary.Call<bool>("AddImage", config.Images.InfoCosts, $"rp.costs", 0UL);
            ImageLibrary.Call<bool>("AddImage", config.Images.InfoCheck, $"rp.check", 0UL);
            ImageLibrary.Call<bool>("AddImage", config.Images.InfoCross, $"rp.cross", 0UL);
        }

        private void FindNewCupboards()
        {
            NextTick(() =>
            {
                var privs = BaseNetworkable.FindObjectsOfType<BuildingPrivlidge>().Where(x => x != null && x.net != null && !ProtectedCupboardManager.ProtectedCupboardExists(x.net.ID)).ToList();
                var count = 0;
                if (privs.Count > 0)
                {
                    foreach (var priv in privs)
                    {
                        try
                        {
                            if (priv != null)
                            {
                                if (AddProtectedCupboard(priv))
                                {
                                    count++;
                                }
                            }
                        }
                        catch (Exception) { }
                    }
                    if (config.EnableConsoleMessages)
                    {
                        Puts($"Loaded {count}/{privs.Count} new cupboards");
                    }
                }
            });
        }

        private void LoadProtectedCupboards()
        {
            NextTick(() =>
            {
                var existing = new Dictionary<uint, ProtectedCupboard>();
                try
                {
                    existing = LoadDataFile<Dictionary<uint, ProtectedCupboard>>("ProtectedCupboards");
                    if (existing != null)
                    {
                        int count = 0;
                        foreach (var kvp in existing)
                        {
                            try
                            {
                                var priv = kvp.Value.BuildingPrivlidge;
                                var tc = kvp.Value;
                                if (priv != null)
                                {
                                    if (AddProtectedCupboard(priv, tc))
                                    {
                                        count++;
                                    }
                                }
                            }
                            catch (Exception) { }
                        }
                        if (existing.Count > 0 && config.EnableConsoleMessages)
                        {
                            Puts($"Loaded {count}/{existing.Count} existing cupboards");
                        }
                    }
                }
                catch (Exception)
                {
                    PrintError("Failed to load protected cupboards");
                }
            });
        }

        private void SaveProtectedCupboards()
        {
            try
            {
                var allCupboards = ProtectedCupboardManager._protectedCupboards;
                SaveDataFile("ProtectedCupboards", allCupboards);
                if (config.EnableConsoleMessages)
                {
                    Puts($"Saved {allCupboards.Count} cupboards");
                }
            }
            catch
            {
                PrintError("Failed to save protected cupboards");
            }
        }
    }
}

namespace Oxide.Plugins
{
	partial class HomeProtection
	{
        
        /*
         * Returns 0 if entity unprotected and 100 if entity fully protected
         */
        private float GetProtectionPercent(BaseEntity entity)
        {
            if (entity == null) { return 0; }
            var priv = entity.GetBuildingPrivilege();
            if (priv == null) { return 0; }
            var tc = ProtectedCupboardManager.GetByID(priv.net.ID);
            if (tc == null) { return 0; }
            return IsProtectedEntity(entity, tc) ? tc.CurrentProtectionPercent : 0;
        }

        /*
         * Returns the protection level rank of the player
         */
        private int GetProtectionLevel(BasePlayer basePlayer)
        {
            if (basePlayer == null) { return ProtectionLevel.NONE.Rank; }
            return ProtectionLevel.GetProtectionLevelOfPlayer(basePlayer).Rank;
        }

        /*
         * Returns a list of the owners of the structure associated with the given entity
         */
        private List<BasePlayer> GetOwners(BaseEntity entity)
        {
            var retVal = new List<BasePlayer>();
            if (entity == null) { return retVal; }
            var priv = entity.GetBuildingPrivilege();
            if (priv == null) { return retVal; }
            var tc = ProtectedCupboardManager.GetByID(priv.net.ID);
            if (tc == null) { return retVal; }
            return tc.Owners;
        }

        /*
         * Returns the player who is the founder of the structure associated with the given entity. Can be null.
         */
        private BasePlayer GetFounder(BaseEntity entity)
        {
            BasePlayer retVal = null;
            if (entity == null) { return retVal; }
            var priv = entity.GetBuildingPrivilege();
            if (priv == null) { return retVal; }
            var tc = ProtectedCupboardManager.GetByID(priv.net.ID);
            if (tc == null) { return retVal; }
            return tc.Founder;
        }

        /*
         * Returns the protection balance of the structure associated with the given entity
         */
        private float GetProtectionBalance(BaseEntity entity)
        {
            var retVal = 0;
            if (entity == null) { return retVal; }
            var priv = entity.GetBuildingPrivilege();
            if (priv == null) { return retVal; }
            var tc = ProtectedCupboardManager.GetByID(priv.net.ID);
            if (tc == null) { return retVal; }
            return tc.Balance;
        }

        /*
         * Returns the hours of protection remaining of the structure associated with the given entity
         */
        private float GetProtectionHours(BaseEntity entity)
        {
            var retVal = 0;
            if (entity == null) { return retVal; }
            var priv = entity.GetBuildingPrivilege();
            if (priv == null) { return retVal; }
            var tc = ProtectedCupboardManager.GetByID(priv.net.ID);
            if (tc == null) { return retVal; }
            return tc.HoursOfProtection;
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        private readonly string[] CHANGELOG = new string[]
        {
            "Fixed issue where balances were not saved upon server shutdown, which caused balances to be unintentionally cleared.",
            "Suppressed messages where no attacker entity is dealing damage"
        };
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        public static class CollectionManager
        {
            public static readonly int COLLECTION_INTERVAL = 5;

            public static Dictionary<uint, Timer> _activeTimers = new Dictionary<uint, Timer>();

            public static void RunCollectionLoop(ProtectedCupboard tc)
            {
                if (tc == null)
                {
                    return;
                }
                var entityID = tc?.EntityID;
                try
                {
                    var timer = PLUGIN.timer.In(COLLECTION_INTERVAL, () =>
                    {
                        PLUGIN.CollectProtectionCost(tc);
                    });
                    if (_activeTimers.ContainsKey(tc.EntityID))
                    {
                        _activeTimers[tc.EntityID].Destroy();
                    }
                    _activeTimers[tc.EntityID] = timer;
                } catch(Exception ex)
                {
                    PLUGIN.PrintWarning($"Failed to run timer for TC.EntityID={entityID} IsNull={tc == null}");
                }
            }

            public static void StopCollectionLoop(ProtectedCupboard tc)
            {
                if (_activeTimers.ContainsKey(tc.EntityID))
                {
                    var timer = _activeTimers[tc.EntityID];
                    timer.Destroy();
                    _activeTimers.Remove(tc.EntityID);
                }
            }
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection : CovalencePlugin
    {
        public class CommandInfo
        {
            public string Command { get; set; }
            public CommandArgument[] Arguments { get; set; } = new CommandArgument[0];
            public string Description { get; set; }
            public string Method { get; set; }
            public int Rank = 999;
            public bool SkipOptional = false;
            public string Permission
            {
                get
                {
                    return Permissions == null ? null : Permissions.FirstOrDefault();
                }
                set
                {
                    Permissions = new string[1] { value };
                }
            }
            public string[] Permissions { get; set; } = new string[0];
            public bool AdminOnly
            {
                get
                {
                    return Permissions.Any(x => x.Contains("admin"));
                }
            }
            public int CommandWordCount
            {
                get
                {
                    return Command.Split(' ').Length;
                }
            }

            public int RequiredArgCount { 
                get
                {
                    return Arguments.Where(x => !x.Optional).Count();
                } 
            }

            public int TotalArgCount
            {
                get
                {
                    return Arguments.Length;
                }
            }

            public string ArgString
            {
                get
                {
                    return $"{string.Join(" ", Arguments.Select(x => x.ToString()))}";
                }
            }

            public void Execute(IPlayer player, string command, string[] args)
            {
                PLUGIN.Call(Method, player, command, args);
            }

            public ValidationResponse Validate(params string[] args) => Validate(false, args);

            public ValidationResponse Validate(bool isServer, params string[] args)
            {
                if (args.Length < RequiredArgCount || args.Length > TotalArgCount)
                {
                    return new ValidationResponse(ValidationStatusCode.INVALID_LENGTH, RequiredArgCount, TotalArgCount);
                }
                var ArgsToCheck = Arguments;
                if (SkipOptional && !isServer && args.Length == RequiredArgCount)
                {
                    ArgsToCheck = Arguments.Where(x => !x.Optional).ToArray();
                }
                int i = 0;
                foreach(var arg in args)
                {
                    var Argument = ArgsToCheck[i];
                    var resp = Argument.Validate(arg);
                    if (!resp.IsValid)
                    {
                        switch(resp.StatusCode)
                        {
                            case ValidationStatusCode.INVALID_VALUE:
                            case ValidationStatusCode.PLAYER_NOT_FOUND:
                                resp.SetData(arg);
                                break;
                        }
                        return resp;
                    }
                    i++;
                }
                return new ValidationResponse();
            }
        }

        public class CommandArgument
        {
            public static readonly CommandArgument PLAYER_NAME = new CommandArgument
            {
                Parameter = "player",
                Validate = (value) =>
                {
                    return BasePlayer.FindAwakeOrSleeping(value) == null ? new ValidationResponse(ValidationStatusCode.PLAYER_NOT_FOUND) : new ValidationResponse(ValidationStatusCode.SUCCESS);
                }
            };

            public string Parameter { get; set; }
            public bool Optional { get; set; } = false;
            public string[] AllowedValues
            {
                set
                {
                    Validate = (given) =>
                    {
                        var expected = value;
                        return expected.Any(x => x.ToLower() == given.ToLower()) ? new ValidationResponse() : new ValidationResponse(ValidationStatusCode.VALUE_NOT_ALLOWED, given, expected);
                    };
                }
            }
            public Func<string, ValidationResponse> Validate { get; set; } = ((value) => { return new ValidationResponse(); });

            public override string ToString()
            {
                return $"<{Parameter}{(Optional ? "?" : string.Empty)}>";
            }
        }

        public class ValidationResponse
        {
            public bool IsValid
            {
                get
                {
                    return StatusCode == ValidationStatusCode.SUCCESS;
                }
            }
            public ValidationStatusCode StatusCode { get; }
            public object[] Data { get; private set; } = new object[0];

            public ValidationResponse()
            {
                StatusCode = ValidationStatusCode.SUCCESS;
            }

            public ValidationResponse(ValidationStatusCode statusCode)
            {
                StatusCode = statusCode;
            }

            public ValidationResponse(ValidationStatusCode statusCode, params object[] data)
            {
                StatusCode = statusCode;
                Data = data;
            }

            public void SetData(params object[] data)
            {
                Data = data;
            }
        }

        public enum ValidationStatusCode
        {
            SUCCESS,
            INVALID_LENGTH,
            INVALID_VALUE,
            PLAYER_NOT_FOUND,
            VALUE_NOT_ALLOWED
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        #region Premade Arguments
        private static readonly CommandArgument REQUIRED_TC_ID_ARGUMENT = new CommandArgument
        {
            Parameter = "tc id",
            Validate = (given) =>
            {
                uint id = 0;
                if (!uint.TryParse(given, out id))
                {
                    return new ValidationResponse(ValidationStatusCode.INVALID_VALUE, given);
                }
                var tc = ProtectedCupboardManager.GetByID(id);
                return tc == null ? new ValidationResponse(ValidationStatusCode.INVALID_VALUE, given) : new ValidationResponse();
            }
        };
        private static readonly CommandArgument OPTIONAL_TC_ID_ARGUMENT = new CommandArgument
        {
            Parameter = "tc id",
            Optional = true,
            Validate = (given) =>
            {
                uint id = 0;
                if (!uint.TryParse(given, out id))
                {
                    return new ValidationResponse(ValidationStatusCode.INVALID_VALUE, given);
                }
                var tc = ProtectedCupboardManager.GetByID(id);
                return tc == null ? new ValidationResponse(ValidationStatusCode.INVALID_VALUE, given) : new ValidationResponse();
            }
        };
        private static readonly CommandArgument REQUIRED_PLAYER = new CommandArgument
        {
            Parameter = "player",
            Validate = (given) =>
            {
                var found = BasePlayer.FindAwakeOrSleeping(given);
                return found == null ? new ValidationResponse(ValidationStatusCode.PLAYER_NOT_FOUND, given) : new ValidationResponse();
            }
        };
        private static readonly CommandArgument OPTIONAL_PLAYER = new CommandArgument
        {
            Parameter = "player",
            Optional = true,
            Validate = (given) =>
            {
                var found = BasePlayer.FindAwakeOrSleeping(given);
                return found == null ? new ValidationResponse(ValidationStatusCode.PLAYER_NOT_FOUND, given) : new ValidationResponse();
            }
        };
        private static readonly CommandArgument BALANCE_AMOUNT = new CommandArgument
        {
            Parameter = "amount",
            Validate = (given) =>
            {
                int amount = 0;
                return !int.TryParse(given, out amount) || amount < 0 ? new ValidationResponse(ValidationStatusCode.INVALID_VALUE, given) : new ValidationResponse();
            }
        };
        #endregion

        #region Commands

        public static readonly List<CommandInfo> Commands = new List<CommandInfo>()
        {
            new CommandInfo()
            {
                Command = "help",
                Method = "CmdHelp",
                Description = "Opens the plugin help menu.",
                Permission = PermissionAdmin,
                Rank = 1
            },
            new CommandInfo()
            {
                Command = "id",
                Method = "CmdTcId",
                Description = "Returns the id of the tool cupboard at your location or at the specified player.",
                Permission = PermissionAdmin,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_PLAYER
                }
            },
            new CommandInfo()
            {
                Command = "tp",
                Method = "CmdTcTp",
                Description = "Teleports you or the specified player to the tool cupboard.",
                Permission = PermissionAdmin,
                Arguments = new CommandArgument[]
                {
                    REQUIRED_TC_ID_ARGUMENT,
                    OPTIONAL_PLAYER
                }
            },
            new CommandInfo()
            {
                Command = "owners",
                Method = "CmdTcOwners",
                Description = "Returns a list of the owners of the tool cupboard.",
                Permission = PermissionAdmin,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT
                }
            },
            new CommandInfo()
            {
                Command = "owners add",
                Method = "CmdTcOwnersAdd",
                Description = "Adds the player as an owner of the tool cupboard.",
                Permission = PermissionAdmin,
                SkipOptional = true,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT,
                    REQUIRED_PLAYER
                }
            },
            new CommandInfo()
            {
                Command = "owners remove",
                Method = "CmdTcOwnersRemove",
                Description = "Removes an owner from the tool cupboard.",
                Permission = PermissionAdmin,
                SkipOptional = true,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT,
                    REQUIRED_PLAYER
                }
            },
            new CommandInfo()
            {
                Command = "founder",
                Method = "CmdTcFounder",
                Description = "Returns the name of the founder of the tool cupboard.",
                Permission = PermissionAdmin,
                SkipOptional = true,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT
                }
            },
            new CommandInfo()
            {
                Command = "founder remove",
                Method = "CmdTcFounderRemove",
                Description = "Removes the founder of the tool cupboard, it will have no founder and no protection level.",
                Permission = PermissionAdmin,
                SkipOptional = true,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT
                }
            },
            new CommandInfo()
            {
                Command = "founder set",
                Method = "CmdTcFounderSet",
                Description = "Sets the player as a founder of the tool cupboard, it will inherit their protection level.",
                Permission = PermissionAdmin,
                SkipOptional = true,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT,
                    REQUIRED_PLAYER
                }
            },
            new CommandInfo()
            {
                Command = "protection",
                Method = "CmdTcProtection",
                Description = "Returns the protection details of the tool cupboard.",
                Permission = PermissionAdmin,
                SkipOptional = true,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT
                }
            },
            new CommandInfo()
            {
                Command = "cost",
                Method = "CmdTcCost",
                Description = "Returns the cost details of the tool cupboard.",
                Permission = PermissionAdmin,
                SkipOptional = true,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT
                }
            },
            new CommandInfo()
            {
                Command = "balance",
                Method = "CmdTcBalance",
                Description = "Returns the current balance of the tool cupboard.",
                Permission = PermissionAdmin,
                SkipOptional = true,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT
                }
            },
            new CommandInfo()
            {
                Command = "balance set",
                Method = "CmdTcBalanceSet",
                Description = "Sets the tool cupboard protection balance to the amount.",
                Permission = PermissionAdmin,
                SkipOptional = true,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT,
                    BALANCE_AMOUNT
                }
            },
            new CommandInfo()
            {
                Command = "balance add",
                Method = "CmdTcBalanceAdd",
                Description = "Adds the amount to the tool cupboard protection balance.",
                Permission = PermissionAdmin,
                SkipOptional = true,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT,
                    BALANCE_AMOUNT
                }
            },
            new CommandInfo()
            {
                Command = "balance remove",
                Method = "CmdTcBalanceRemove",
                Description = "Removes the amount from the tool cupboard protection balance.",
                Permission = PermissionAdmin,
                SkipOptional = true,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT,
                    BALANCE_AMOUNT
                }
            },
            new CommandInfo()
            {
                Command = "status",
                Method = "CmdTcStatus",
                Description = "Returns the protection status and reason of the tool cupboard.",
                Permission = PermissionAdmin,
                SkipOptional = true,
                Arguments = new CommandArgument[]
                {
                    OPTIONAL_TC_ID_ARGUMENT
                }
            },
            new CommandInfo()
            {
                Command = "changelog",
                Method = "CmdTcChangelog",
                Description = "Displays a list of changes in the latest patch.",
                Permission = PermissionAdmin
            },
        };
        #endregion

        #region Helpers
        private BasePlayer GetPlayerOrTarget(IPlayer player, string command, string[] args, int index = 0, int length = 1)
        {
            BasePlayer caller = player.Object as BasePlayer;
            if (player.IsServer && args.Length < length)
            {
                Message(caller, "command player not found", "<not given>");
                return null;
            }
            BasePlayer basePlayer = caller;
            if (args.Length >= length)
            {
                basePlayer = BasePlayer.FindAwakeOrSleeping(args[index]);
            }
            return basePlayer;
        }

        private BuildingPrivlidge GetPrivFromId(string id)
        {
            return ProtectedCupboardManager.GetByID(uint.Parse(id)).BuildingPrivlidge;
        }

        private ProtectedCupboard GetTcFromId(string id)
        {
            return ProtectedCupboardManager.GetByID(uint.Parse(id));
        }

        private ProtectedCupboard GetTcFromIdOrPlayer(IPlayer player, string command, string[] args, int indexOfTcId = 0, int totalExpectedLengthOfArgs = 1)
        {
            BasePlayer caller = player.Object as BasePlayer;
            if (player.IsServer && args.Length < totalExpectedLengthOfArgs)
            {
                Message(caller, "command invalid value", args[0]);
                return null;
            }
            else if (args.Length < totalExpectedLengthOfArgs)
            {
                var priv = caller.GetBuildingPrivilege();
                if (priv == null) { return null; }
                return ProtectedCupboardManager.GetByID(priv.net.ID);
            }
            var id = uint.Parse(args[indexOfTcId]);
            return ProtectedCupboardManager.GetByID(id);
        }
        #endregion

        #region Controller
        private void CmdController(IPlayer player, string command, string[] args)
        {
            try
            {
                if (args.Length == 0)
                {
                    args = new string[] { "help" };
                }
                CommandInfo commandInfo = null;
                if (args.Length >= 2)
                {
                    commandInfo = Commands.OrderByDescending(x => x.CommandWordCount).FirstOrDefault(x => x.Command == string.Join(" ", new[] { args[0], args[1] }));
                }
                if (commandInfo == null)
                {
                    commandInfo = Commands.FirstOrDefault(x => x.Command == args[0]);
                }
                if (commandInfo == null)
                {
                    CmdHelp(player, command, args);
                    return;
                }
                // Permission
                if (!player.IsServer)
                {
                    foreach (var perm in commandInfo.Permissions)
                    {
                        if (!permission.UserHasPermission(player.Id, perm))
                        {
                            Message(player.Object as BasePlayer, "command no permission", perm);
                            return;
                        }
                    }
                }
                args = args.Skip(commandInfo.CommandWordCount).ToArray();
                // Validation
                var resp = commandInfo.Validate(player.IsServer, args);
                if (resp.IsValid)
                {
                    if (player.IsServer && args.Length < commandInfo.TotalArgCount)
                    {
                        Message(player.Object as BasePlayer, "command all args required");
                        Message(player.Object as BasePlayer, "command usage", config.Commands.Admin, commandInfo.Command, commandInfo.ArgString);
                        return;
                    }
                    commandInfo.Execute(player, command, args);
                }
                else
                {
                    var message = Lang("command usage", player.Object as BasePlayer, config.Commands.Admin, commandInfo.Command, commandInfo.ArgString);
                    switch (resp.StatusCode)
                    {
                        case ValidationStatusCode.PLAYER_NOT_FOUND:
                            message = Lang("command player not found", player.Id, resp.Data);
                            break;
                        case ValidationStatusCode.INVALID_VALUE:
                        case ValidationStatusCode.VALUE_NOT_ALLOWED:
                            message = Lang("command invalid value", player.Id, resp.Data);
                            break;
                    }
                    MessageNonLocalized(player.Object as BasePlayer, message);
                }
            }
            catch (Exception)
            {
                Message(player.Object as BasePlayer, "command error");
            }
        }
        #endregion

        #region Methods
        private void CmdHelp(IPlayer player, string command, string[] args)
        {
            if (player.IsServer)
            {
                Puts("\n" + string.Join("\n", Commands.Select(x => $"{config.Commands.Admin} {x.Command} {x.ArgString}".PadRight(60) + x.Description)));
            }
            else
            {
                ShowHelp(player.Object as BasePlayer);
            }
        }

        private void CmdTcId(IPlayer player, string command, string[] args)
        {
            var basePlayer = GetPlayerOrTarget(player, command, args);
            if (basePlayer == null) { return; }
            var priv = basePlayer.GetBuildingPrivilege();
            Message(player.Object as BasePlayer, "command id", priv.net.ID);
        }

        private void CmdTcTp(IPlayer player, string command, string[] args)
        {
            var priv = GetPrivFromId(args[0]);
            var basePlayer = GetPlayerOrTarget(player, command, args, 1, 2);
            basePlayer.MovePosition(priv.transform.position);
            Message(player.Object as BasePlayer, "command success", priv.net.ID);
        }

        private void CmdTcOwners(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args, 0, 1);
            Message(player.Object as BasePlayer, "command owners", tc.EntityID, string.Join("\n", tc.Owners.Select(x => x.ToString())));
        }

        private void CmdTcOwnersAdd(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args, 0, 2);
            var basePlayer = BasePlayer.FindAwakeOrSleeping(args.Last());
            if (!tc.OwnerHistory.ContainsKey(basePlayer.userID))
            {
                tc.OwnerHistory.Add(basePlayer.userID, new OwnerHistoryEntry(basePlayer));
                UpdateOwners(tc);
            }
            Message(player.Object as BasePlayer, "command success");
            Message(player.Object as BasePlayer, "command owners", tc.EntityID, tc.Owners.ToSentence());
        }

        private void CmdTcOwnersRemove(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args, 0, 2);
            var basePlayer = BasePlayer.FindAwakeOrSleeping(args.Last());
            if (tc.BuildingPrivlidge.IsAuthed(basePlayer))
            {
                Message(player.Object as BasePlayer, "command cannot remove auth");
                return;
            }
            if (tc.Owners.Contains(basePlayer))
            {
                tc.OwnerHistory.Remove(basePlayer.userID);
                tc.Owners.Remove(basePlayer);
                UpdateOwners(tc);
            }
            Message(player.Object as BasePlayer, "command success");
            Message(player.Object as BasePlayer, "command owners", tc.EntityID, tc.Owners.ToSentence());
        }

        private void CmdTcFounder(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args);
            Message(player.Object as BasePlayer, "command founder", tc.EntityID, tc.FounderDisplayName, tc.HighestProtectionLevel.Rank);
        }

        private void CmdTcFounderSet(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args, 0, 2);
            var basePlayer = BasePlayer.FindAwakeOrSleeping(args.Last());
            tc.Founder = basePlayer;
            UpdateOwners(tc);
            Message(player.Object as BasePlayer, "command success");
            Message(player.Object as BasePlayer, "command founder", tc.EntityID, tc.FounderDisplayName, tc.HighestProtectionLevel.Rank);
        }

        private void CmdTcFounderRemove(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args);
            tc.Founder = null;
            UpdateOwners(tc);
            Message(player.Object as BasePlayer, "command success");
            Message(player.Object as BasePlayer, "command founder", tc.EntityID, tc.FounderDisplayName, tc.HighestProtectionLevel.Rank);
        }

        private void CmdTcProtection(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args);
            Message(player.Object as BasePlayer, "command protection", tc.EntityID, tc.CurrentProtectionPercent, tc.OnlineProtectionPercent, tc.OfflineProtectionPercent, tc.HighestProtectionLevel.Rank);
        }

        private void CmdTcCost(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args);
            Message(player.Object as BasePlayer, "command cost", tc.EntityID, tc.BaseCostPerHour, tc.OwnerCostPerHour, tc.Owners.Count, tc.BuildingCostPerHour, tc.FoundationCount, FormatCurrency(tc.TotalProtectionCostPerHour));
        }

        private void CmdTcBalance(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args);
            Message(player.Object as BasePlayer, "command balance", tc.EntityID, FormatCurrency(tc.Balance));
        }

        private void CmdTcBalanceSet(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args, 0, 2);
            ClearProtectionBalance(tc);
            UpdateProtectionBalance(tc, float.Parse(args.Last()), BalanceLedgerReason.Other);
            Message(player.Object as BasePlayer, "command success");
            Message(player.Object as BasePlayer, "command balance", tc.EntityID, FormatCurrency(tc.Balance));
        }

        private void CmdTcBalanceAdd(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args, 0, 2);
            UpdateProtectionBalance(tc, float.Parse(args.Last()), BalanceLedgerReason.Other);
            Message(player.Object as BasePlayer, "command success");
            Message(player.Object as BasePlayer, "command balance", tc.EntityID, FormatCurrency(tc.Balance));
        }

        private void CmdTcBalanceRemove(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args, 0, 2);
            UpdateProtectionBalance(tc, -float.Parse(args.Last()), BalanceLedgerReason.Other);
            Message(player.Object as BasePlayer, "command success");
            Message(player.Object as BasePlayer, "command balance", tc.EntityID, FormatCurrency(tc.Balance));
        }

        private void CmdTcStatus(IPlayer player, string command, string[] args)
        {
            var tc = GetTcFromIdOrPlayer(player, command, args);
            Message(player.Object as BasePlayer, "command status", tc.EntityID, tc.Status, tc.Reason);
        }

        private void CmdTcChangelog(IPlayer player, string command, string[] args)
        {
            Message(player.Object as BasePlayer, "command changelog", PLUGIN.Version.ToString(), $"\n-{string.Join("\n-", CHANGELOG)}");
        }
        #endregion
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
		private Configuration config;

		private class Configuration
        {
			[JsonProperty(PropertyName = "Enable Logging")]
			public bool EnableLogging = false;

			[JsonProperty(PropertyName = "Enable Console Messages")]
			public bool EnableConsoleMessages = true;

			[JsonProperty(PropertyName = "Chat Message Icon ID")]
			public ulong ChatMessageIconId = 0;

			[JsonProperty(PropertyName = "Commands")]
			public CommandsConfig Commands = new CommandsConfig();

			[JsonProperty(PropertyName = "Protection Settings")]
			public ProtectionConfig Protection = new ProtectionConfig();

			[JsonProperty(PropertyName = "Image Settings")]
			public ImagesConfig Images = new ImagesConfig();

			[JsonProperty(PropertyName = "Indicator Settings")]
			public IndicatorConfig Indicator = new IndicatorConfig();

			[JsonProperty(PropertyName = "Plugin Integration")]
			public IntegrationConfig Integration = new IntegrationConfig();

			[JsonProperty(PropertyName = "Version")]
			public VersionNumber Version { get; set; } = new VersionNumber(0, 0, 0);

			public class ProtectionConfig
			{
				[JsonProperty(PropertyName = "Protected entities")]
				public ProtectedEntitiesConfig ProtectedEntities = new ProtectedEntitiesConfig();

				[JsonProperty(PropertyName = "Protected from")]
				public ProtectedFromConfig ProtectedFrom = new ProtectedFromConfig();

				[JsonProperty(PropertyName = "Protection levels")]
				public ProtectionLevel[] ProtectionLevels = new ProtectionLevel[]
				{
					new ProtectionLevel {
						Rank = 1,
						OnlineProtectionPercent = 100f,
						OfflineProtectionPercent = 100f,
						CostPerDamageProtected = 0f,
						HourlyBaseCost = 10,
					},
				};
				[JsonProperty(PropertyName = "Admin owners removed when deauthorized")]
				public bool RemoveAdminOwners { get; set; } = true;
				[JsonProperty(PropertyName = "Award remaining balance when cupboard destroyed")]
				public bool AwardRemainingBalance { get; set; } = true;

				[JsonProperty(PropertyName = "Allow max deposit")]
				public bool AllowMaxDeposit { get; set; } = true;
				[JsonProperty(PropertyName = "Allow balance withdraw")]
				public bool AllowBalanceWithdraw { get; set; } = true;

				[JsonProperty(PropertyName = "Allow protection pause")]
				public bool AllowProtectionPause { get; set; } = true;

				[JsonProperty(PropertyName = "Currency item (if not using ServerRewards or Economics)")]
				public string CurrencyItem { get; set; } = "scrap";
			}

			public class CommandsConfig
			{
				[JsonProperty(PropertyName = "Admin")]
				public string Admin { get; set; } = "tc";

				[JsonProperty(PropertyName = "Protection")]
				public string Protection { get; set; } = "pro";

				[JsonProperty(PropertyName = "Levels")]
				public string Levels { get; set; } = "lev";
			}

			public class ProtectedFromConfig
			{
				[JsonProperty(PropertyName = "Authorized Players")]
				public bool AuthorizedPlayers { get; set; } = false;

				[JsonProperty(PropertyName = "Unauthorized Players")]
				public bool UnauthorizedPlayers { get; set; } = true;

				[JsonProperty(PropertyName = "Attack Heli")]
				public bool AttackHeli { get; set; } = true;
			}

			public class ProtectedEntitiesConfig
            {
				[JsonProperty(PropertyName = "Buildings")]
				public bool Buildings { get; set; } = true;

				[JsonProperty(PropertyName = "Deployables")]
				public bool Containers { get; set; } = true;

				[JsonProperty(PropertyName = "Loot Nodes")]
				public bool LootNodes { get; set; } = false;

				[JsonProperty(PropertyName = "Authed Players")]
				public bool AuthedPlayers { get; set; } = false;

				[JsonProperty(PropertyName = "Unauthed Players")]
				public bool UnauthedPlayers { get; set; } = false;

				[JsonProperty(PropertyName = "NPCs")]
				public bool NPCs { get; set; } = false;

				[JsonProperty(PropertyName = "Animals")]
				public bool Animals { get; set; } = false;

				[JsonProperty(PropertyName = "Vehicles")]
				public bool Vehicles { get; set; } = false;

				[JsonProperty(PropertyName = "Horses")]
				public bool Horses { get; set; } = false;

				[JsonProperty(PropertyName = "Electrical")]
				public bool Electrical { get; set; } = true;
			}

			public class IntegrationConfig
			{
				[JsonProperty(PropertyName = "Economics")]
				public bool Economics { get; set; } = false;

				[JsonProperty(PropertyName = "Server Rewards")]
				public bool ServerRewards { get; set; } = false;
				[JsonProperty(PropertyName = "Custom Status Framework")]
				public bool CustomStatusFramework { get; set; } = false;
				[JsonProperty(PropertyName = "Notify")]
				public bool Notify { get; set; } = false;
				[JsonProperty(PropertyName = "Clans")]
				public bool Clans { get; set; } = false;
			}

			public class IndicatorConfig
            {
				public bool Enabled = true;
				public bool ShowBalanceDeducted = true;
				public int FontSize = 18;
				public string AnchorMin = "0.94 0.9";
				public string AnchorMax = "0.94 0.9";
				public string OffsetMin = "0 0";
				public string OffsetMax = "64 64";
			}

			public class ImagesConfig
            {
				[JsonProperty(PropertyName = "Status Protected")]
				public string StatusProtected { get; set; } = "https://i.imgur.com/yRpwabu.png";
				[JsonProperty(PropertyName = "Status Unprotected")]
				public string StatusUnprotected { get; set; } = "https://i.imgur.com/CVhQIOa.png";
				[JsonProperty(PropertyName = "Status Info")]
				public string StatusInfo { get; set; } = "https://i.imgur.com/WT3LEWe.png";
				[JsonProperty(PropertyName = "Status Toggle")]
				public string StatusToggle { get; set; } = "https://i.imgur.com/efiNWBE.png";
				[JsonProperty(PropertyName = "Status Refresh")]
				public string StatusRefresh { get; set; } = "https://i.imgur.com/ZHvH2L8.png";
				[JsonProperty(PropertyName = "Info Owners")]
				public string InfoOwners { get; set; } = "https://i.imgur.com/3tKTjEU.png";
				[JsonProperty(PropertyName = "Info Costs")]
				public string InfoCosts { get; set; } = "https://i.imgur.com/Z45c9qB.png";
				[JsonProperty(PropertyName = "Info Check")]
				public string InfoCheck { get; set; } = "https://imgur.com/d4TZqSJ.png";
				[JsonProperty(PropertyName = "Info Cross")]
				public string InfoCross { get; set; } = "https://i.imgur.com/vwdhgUb.png";
			}
		}

		protected override void LoadConfig()
		{
			base.LoadConfig();
			var recommended = $"It is recommended to backup your current oxide/config/{Name}.json and oxide/lang/en/{Name}.json files and remove them to generate fresh ones.";
			var usingDefault = "Overriding configuration with default values to avoid errors.";

			try
			{
				config = Config.ReadObject<Configuration>();
				if (config == null) { throw new Exception(); }
				else if (config.Version.Major != Version.Major || config.Version.Minor != Version.Minor) throw new NotSupportedException();
				SaveConfig();
			}
			catch (NotSupportedException)
			{
				if (ShowConfigWarnings)
                {
					PrintError($"Your configuration file is out of date. Your configuration file is for v{config.Version.Major}.{config.Version.Minor}.{config.Version.Patch} but the plugin is on v{Version.Major}.{Version.Minor}.{Version.Patch}. {recommended}");
					PrintWarning(usingDefault);
				}
				LoadDefaultConfig();
			}
			catch (Exception)
			{
				if (ShowConfigWarnings)
                {
					PrintError($"Your configuration file contains an error. {recommended}");
					PrintWarning(usingDefault);
				}
				LoadDefaultConfig();
			}
			ShowConfigWarnings = true;
		}

		protected override void SaveConfig() => Config.WriteObject(config);

		protected override void LoadDefaultConfig()
		{
			config = new Configuration();
			config.Version = new VersionNumber(Version.Major, Version.Minor, Version.Patch);
		}

	}
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        #region Protected Cupboard
        /// <summary>
        /// Initializes a new protected cupboard
        /// </summary>
        private bool AddProtectedCupboard(BuildingPrivlidge priv, ProtectedCupboard existing = null)
        {
            if (IsNull(priv)) return false;
            OnBeforeProtectedCupboardAdded(priv);
            var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
            var success = tc != null;
            if (success && existing != null)
            {
                LogAction(tc.EntityID, "Loaded existing tool cupboard");
                tc.OwnerHistory = existing.OwnerHistory;
                UpdateOwners(tc, false);
                tc.Balance = existing.Balance;
                tc.Enabled = existing.Enabled;
                tc.BalanceLedger.Add(new BalanceLedgerEntry(tc, BalanceLedgerReason.Initial));
            }
            else if (success && existing == null)
            {
                LogAction(tc.EntityID, "TC initially added");
            }
            OnAfterProtectedCupboardAdded(tc, success, existing != null);
            return success;
        }
        private void OnBeforeProtectedCupboardAdded(BuildingPrivlidge priv)
        {
        }
        private void OnAfterProtectedCupboardAdded(ProtectedCupboard tc, bool success, bool existing)
        {
            if (IsNull(tc)) return;
            if (success)
            {
                StartOrStopProtection(tc, () =>
                {
                    if (existing)
                    {
                        LogAction(tc.EntityID, $"Initial status is {tc.Status} with reason {tc.Reason} and balance {tc.Balance}");
                    }
                    tc.SkipNextProtectionStartDelay = false;
                });
            }
        }

        /// <summary>
        /// Removes an existing protected cupboard
        /// </summary>
        private void RemoveProtectedCupboard(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
            OnBeforeProtectedCupboardRemoved(tc);
            var success = ProtectedCupboardManager.RemoveProtectedCupboard(tc.EntityID);
            OnAfterProtectedCupboardRemoved(tc.EntityID, success);
        }
        private void OnBeforeProtectedCupboardRemoved(ProtectedCupboard tc)
        {
            StopProtection(tc);
            foreach(var bp in tc.CurrentlyViewing.ToList())
            {
                CloseProtectedCupboard(tc, bp);
            }
        }
        private void OnAfterProtectedCupboardRemoved(uint entityID, bool success)
        {

        }
        #endregion

        #region Protection
        private void StartOrStopProtection(ProtectedCupboard tc, Action callback = null)
        {
            if (IsNull(tc)) return;
            var valid = tc.HasFounder && tc.HasRemainingProtection && tc.Enabled && ((tc.HasOnlineProtection && tc.HasOwnersOnline) || (tc.HasOfflineProtection && !tc.HasOwnersOnline));
            if (!tc.IsProtected && valid)
            {
                StartProtection(tc, callback);
            }
            else if (tc.IsProtected && !valid)
            {
                StopProtection(tc, callback);
            }
            else
            {
                callback?.Invoke();
            }
        }
        /// <summary>
        /// Starts protection for a protected cupboard
        /// </summary>
        private void StartProtection(ProtectedCupboard tc, Action callback = null)
        {
            if (IsNull(tc)) return;
            OnBeforeProtectionStarted(tc);
            LogAction(tc.EntityID, $"Starting protection in {tc.ProtectionStartDelay}s {(tc.SkipNextProtectionStartDelay ? "(Skipped)" : string.Empty)}");
            timer.In(tc.ProtectionStartDelay, () =>
            {
                if (IsNull(tc)) return;
                var success = tc.HighestProtectionLevel != ProtectionLevel.NONE && tc.HasRemainingProtection;
                OnAfterProtectionStarted(tc, success, callback);
            });
        }
        private void OnBeforeProtectionStarted(ProtectedCupboard tc)
        {
        }
        private void OnAfterProtectionStarted(ProtectedCupboard tc, bool success, Action callback = null)
        {
            tc.SkipNextProtectionStartDelay = false;
            if (success)
            {
                UpdateProtectionStatus(tc, ProtectionStatus.Protected);
                StartCollectionLoop(tc);
            }
            callback?.Invoke();
        }

        /// <summary>
        /// Stops protection for a protected cupboard
        /// </summary>
        private void StopProtection(ProtectedCupboard tc, Action callback = null)
        {
            if (IsNull(tc)) return;
            OnBeforeProtectionStopped(tc);
            LogAction(tc.EntityID, "Stopping protection");
            StopCollectionLoop(tc);
            UpdateProtectionStatus(tc, ProtectionStatus.Unprotected);
            var success = true;
            OnAfterProtectionStopped(tc, success, callback);
        }
        private void OnBeforeProtectionStopped(ProtectedCupboard tc)
        {
        }
        private void OnAfterProtectionStopped(ProtectedCupboard tc, bool success, Action callback = null)
        {
            callback?.Invoke();
        }
        #endregion

        #region Collection Loop
        /// <summary>
        /// Called to collect the due amount from a protected cupboards balance
        /// </summary>
        protected void CollectProtectionCost(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
            OnBeforeProtectionCostCollected(tc);
            var cost = tc.TotalCostPerInterval(CollectionManager.COLLECTION_INTERVAL);
            bool success = tc.Balance >= cost;
            if (success)
            {
                UpdateProtectionBalance(tc, -tc.TotalCostPerInterval(CollectionManager.COLLECTION_INTERVAL), BalanceLedgerReason.Interval);
            }
            OnAfterProtectionCostCollected(tc, cost, success);
        }
        private void OnBeforeProtectionCostCollected(ProtectedCupboard tc)
        {
        }
        private void OnAfterProtectionCostCollected(ProtectedCupboard tc, float amountCollected, bool success)
        {
            if (success)
            {
                LogAction(tc.EntityID, $"Collected {amountCollected} Balance is {tc.Balance}");
                ContinueCollectionLoop(tc);
            }
            else
            {
                ClearProtectionBalance(tc);
            }
        }

        /// <summary>
        /// Starts the loop to continuously collect the due protection amount
        /// </summary>
        private void StartCollectionLoop(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
            OnBeforeCollectionLoopStarted(tc);
            CollectionManager.RunCollectionLoop(tc);
            OnAfterCollectionLoopStarted(tc);
        }
        private void OnBeforeCollectionLoopStarted(ProtectedCupboard tc)
        {
        }
        private void OnAfterCollectionLoopStarted(ProtectedCupboard tc)
        {
        }

        /// <summary>
        /// Continues an already started collection loop
        /// </summary>
        private void ContinueCollectionLoop(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
            OnBeforeCollectionLoopContinued(tc);
            CollectionManager.RunCollectionLoop(tc);
            OnAfterCollectionLoopContinued(tc);
        }
        private void OnBeforeCollectionLoopContinued(ProtectedCupboard tc)
        {
        }
        private void OnAfterCollectionLoopContinued(ProtectedCupboard tc)
        {
        }

        /// <summary>
        /// Stops an active collection loop
        /// </summary>
        private void StopCollectionLoop(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
            OnBeforeCollectionLoopStopped(tc);
            CollectionManager.StopCollectionLoop(tc);
            OnAfterCollectionLoopStopped(tc);
        }
        private void OnBeforeCollectionLoopStopped(ProtectedCupboard tc)
        {
        }
        private void OnAfterCollectionLoopStopped(ProtectedCupboard tc)
        {
        }
        #endregion

        #region Owners
        private void AdminOwnerDeauthorized(ProtectedCupboard tc, BasePlayer basePlayer)
        {
            if (IsNull(tc) || IsNull(basePlayer)) return;
            Message(basePlayer, "message deauthorize admin");
            tc.OwnerHistory.Remove(basePlayer.userID);
            tc.SkipNextProtectionStartDelay = true;
        }
        private void OwnerAuthorized(ProtectedCupboard tc, BasePlayer basePlayer)
        {
            if (IsNull(tc) || IsNull(basePlayer)) return;
            OnBeforeOwnerAuthorized(tc, basePlayer);
            var isNew = !tc.OwnerHistory.ContainsKey(basePlayer.userID);
            if (isNew)
            {
                tc.OwnerHistory[basePlayer.userID] = new OwnerHistoryEntry(basePlayer);
            }
            OnAfterOwnerAuthorized(tc, basePlayer, isNew);
        }

        private void OnBeforeOwnerAuthorized(ProtectedCupboard tc, BasePlayer basePlayer)
        {
            if (IsNull(tc)) return;
        }

        private void OnAfterOwnerAuthorized(ProtectedCupboard tc, BasePlayer basePlayer, bool isNew)
        {
            if (IsNull(tc)) return;
            if (IsAdmin(basePlayer) && config.Protection.RemoveAdminOwners)
            {
                Message(basePlayer, "message authorize admin");
            }
            LogAction(tc.EntityID, $"Player {basePlayer.displayName} has authorized on the TC");
        }
        private void UpdateOwners(ProtectedCupboard tc, bool autostart = true)
        {
            if (IsNull(tc)) return;
            OnBeforeOwnersUpdated(tc);
            tc.Owners = null;
            tc.OnlineOwners = null;
            tc.OwnerCostPerHour = null;
            tc.HighestProtectionLevel = null;
            tc.BuildingCostPerHour = null;
            tc.FoundationCount = null;
            tc.TotalProtectionCostPerHour = null;
            tc.Balance = tc.Balance;
            OnAfterOwnersUpdated(tc, autostart);
        }

        private void OnBeforeOwnersUpdated(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
        }

        private void OnAfterOwnersUpdated(ProtectedCupboard tc, bool autostart = true)
        {
            if (IsNull(tc)) return;
            LogAction(tc.EntityID, $"Owners have been updated to {tc.Owners.Select(x => x.displayName).ToSentence()} the Founder is {tc.FounderDisplayName}");
            if (autostart)
            {
                StartOrStopProtection(tc);
            }
        }

        private void UpdateOnlineOwners(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
            OnBeforeOnlineOwnersUpdated(tc);
            tc.OnlineOwners = null;
            OnAfterOnlineOwnersUpdated(tc);
        }

        private void OnBeforeOnlineOwnersUpdated(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
        }

        private void OnAfterOnlineOwnersUpdated(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
            // Start online protection
            if (tc.HasOwnersOnline && tc.HasOnlineProtection && tc.HasRemainingProtection && !tc.IsProtected)
            {
                StartOrStopProtection(tc);
            }
            // Start offline protection
            else if (!tc.HasOwnersOnline && tc.HasOfflineProtection && tc.HasRemainingProtection && !tc.IsProtected)
            {
                StartOrStopProtection(tc);
            }
            // Stop online protection
            else if (tc.HasOwnersOnline && !tc.HasOnlineProtection && tc.HasRemainingProtection && tc.IsProtected)
            {
                StartOrStopProtection(tc);
            }
            // Stop offline protection
            else if (!tc.HasOwnersOnline && !tc.HasOfflineProtection && tc.HasRemainingProtection && tc.IsProtected)
            {
                StartOrStopProtection(tc);
            }
        }
        #endregion

        #region Protection Balance
        private void ClearProtectionBalance(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
            OnBeforeProtectionBalanceCleared(tc);
            var cleared = tc.Balance > 0;
            tc.Balance = 0;
            OnAfterProtectionBalanceCleared(tc, cleared);
        }
        private void OnBeforeProtectionBalanceCleared(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
        }

        private void OnAfterProtectionBalanceCleared(ProtectedCupboard tc, bool cleared)
        {
            if (IsNull(tc)) return;
            if (cleared)
            {
                LogAction(tc.EntityID, $"Balance was cleared");
                tc.BalanceLedger.Add(new BalanceLedgerEntry(tc, BalanceLedgerReason.Withdraw));
            }
            StartOrStopProtection(tc);
        }

        private void UpdateProtectionBalance(ProtectedCupboard tc, float amount, BalanceLedgerReason reason)
        {
            if (IsNull(tc)) return;
            OnBeforeProtectionBalanceUpdated(tc);
            var beforeBalance = tc.Balance;
            if (amount != 0)
            {
                tc.Balance += amount;
            }
            OnAfterProtectionBalanceUpdated(tc, amount, beforeBalance, reason);
        }

        private void OnBeforeProtectionBalanceUpdated(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
        }

        private void OnAfterProtectionBalanceUpdated(ProtectedCupboard tc, float amount, float beforeBalance, BalanceLedgerReason reason)
        {
            if (IsNull(tc)) return;
            tc.BalanceLedger.Add(new BalanceLedgerEntry(tc, reason));
            if (reason == BalanceLedgerReason.Withdraw || reason == BalanceLedgerReason.Added || reason == BalanceLedgerReason.Other)
            {
                LogAction(tc.EntityID, $"Balance updated by {amount} from {beforeBalance} to {tc.Balance} with reason {reason}");
            }
            StartOrStopProtection(tc);
        }
        #endregion

        #region Protection Status
        private void UpdateProtectionStatus(ProtectedCupboard tc, ProtectionStatus status)
        {
            if (IsNull(tc)) return;
            OnBeforeProtectionStatusUpdated(tc);
            var previousStatus = tc.Status;
            if (status == ProtectionStatus.Protected)
            {
                if (!tc.HasFounder)
                {
                    // No founder
                    tc.Status = ProtectionStatus.Unprotected;
                }
                else if (!tc.HasOnlineProtection && !tc.HasOfflineProtection)
                {
                    // Has no protection at all
                    tc.Status = ProtectionStatus.Unprotected;
                }
                else if (tc.HasOnlineProtection && !tc.HasOfflineProtection && tc.HasOwnersOnline)
                {
                    // Has only Online protection
                    tc.Status = ProtectionStatus.Protected;
                }
                else if (!tc.HasOnlineProtection && tc.HasOfflineProtection && !tc.HasOwnersOnline)
                {
                    // Has only Offline protection
                    tc.Status = ProtectionStatus.Protected;
                }
                else if(tc.HasOnlineProtection && tc.HasOfflineProtection)
                {
                    // Has both Online/Offline protection
                    tc.Status = ProtectionStatus.Protected;
                }
                else
                {
                    tc.Status= ProtectionStatus.Unprotected;
                }
            }
            else
            {
                tc.Status = status;
            }
            OnAfterProtectionStatusUpdated(tc, previousStatus);
        }

        private void OnBeforeProtectionStatusUpdated(ProtectedCupboard tc)
        {
        }

        private void OnAfterProtectionStatusUpdated(ProtectedCupboard tc, ProtectionStatus previousStatus)
        {
            LogAction(tc.EntityID, $"Protection status updated from {previousStatus} to {tc.Status}");
            foreach (var viewer in tc.CurrentlyViewing)
            {
                if (PlayersViewingOverlay.Contains(viewer.userID))
                {
                    ShowProtectionStatusOverlay(viewer, tc);
                }
            }
        }
        #endregion

        #region Structure Damage
        private object DamageProtectedStructure(ProtectedCupboard tc, BaseCombatEntity entity, HitInfo info)
        {
            if (IsNull(tc) || IsNull(entity) || IsNull(info) || info.damageTypes.Has(Rust.DamageType.Decay) || info.damageTypes.Has(Rust.DamageType.Decay)) return null;
            OnBeforeProtectedStructureDamaged(tc);
            if (!ZoneManagerHelper.AllowDestruction(entity, tc)) { return null; }
            var attacker = info.Initiator;
            var attackerPlayer = info.InitiatorPlayer;
            if (!config.Protection.ProtectedFrom.AuthorizedPlayers && attackerPlayer != null && tc.AuthedPlayers.Contains(attackerPlayer))
            {
                return null;
            }
            else if (!config.Protection.ProtectedFrom.UnauthorizedPlayers && attackerPlayer != null)
            {
                return null;
            }
            else if (!config.Protection.ProtectedFrom.AttackHeli && attacker != null && attacker is BaseHelicopter)
            {
                return null;
            }
            var totalDamage = info.damageTypes.Total();
            var fireDamage = info.damageTypes.Get(Rust.DamageType.Heat);
            var shockDamage = info.damageTypes.Get(Rust.DamageType.ElectricShock);
            float damageTaken;
            float damageProtected;
            bool providedProtection = false;
            var protectionPercent = tc.CurrentProtectionPercent;
            var beforeBalance = tc.Balance;
            object returnVal;
            if (protectionPercent >= 100f)
            {
                returnVal = false; // completely protected
                damageTaken = 0f;
                damageProtected = totalDamage;
                providedProtection = true;
                //if (attackerPlayer != null)
                //{
                //    NotifyProtectionStatusChanged(attackerPlayer, tc);
                //}
            }
            else if (protectionPercent <= 0f)
            {
                returnVal = null; // completely unprotected
                damageTaken = totalDamage;
                damageProtected = 0f;
            }
            else
            {
                // some protection
                var factor = 1f - (protectionPercent / 100f);
                info.damageTypes.ScaleAll(factor);
                returnVal = null;
                damageTaken = totalDamage * factor;
                damageProtected = totalDamage - damageTaken;
                providedProtection = true;
                
            }
            // charge for damage taken
            if (providedProtection && tc.HasCostPerDamageProtected)
            {
                damageProtected -= fireDamage;
                damageProtected -= shockDamage; // dont charge for these
                var cost = tc.CostPerDamageProtected * damageProtected;
                UpdateProtectionBalance(tc, -cost, BalanceLedgerReason.DamageCost);
                if (attackerPlayer != null)
                {
                    NotifyProtectionStatusChanged(attackerPlayer, tc, false, cost);
                }
            }
            else if (providedProtection)
            {
                if (attackerPlayer != null)
                {
                    NotifyProtectionStatusChanged(attackerPlayer, tc, false);
                }
            }
            OnAfterProtectedStructureDamaged(tc, entity, attackerPlayer, beforeBalance, totalDamage, damageTaken, providedProtection);
            return returnVal;
        }

        private void OnBeforeProtectedStructureDamaged(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
        }

        private void OnAfterProtectedStructureDamaged(ProtectedCupboard tc, BaseCombatEntity entity, BaseEntity attacker, float beforeBalance, float totalDamage, float takenDamage, bool providedProtection)
        {
            if (IsNull(tc)) return;
            if (totalDamage > 0 || attacker != null)
            {
                LogAction(tc.EntityID, $"Attacked by {GetEntityDisplayName(attacker)} for {takenDamage}/{totalDamage} damage. Balance was {beforeBalance} and is now {tc.Balance}.");
            }
            if (!providedProtection)
            {
                tc.LastDamagedTime = DateTime.Now;
            }
            if (beforeBalance > 0 && tc.Balance <= 0)
            {
                StopProtection(tc, () =>
                {
                    timer.In(0.5f, () =>
                    {
                        if (attacker != null && tc != null && attacker is BasePlayer && !attacker.IsNpc)
                        {
                            NotifyProtectionStatusChanged((BasePlayer)attacker, tc, true);
                        }
                    });
                });
            }
        }

        private void DestroyProtectedCupboard(ProtectedCupboard tc, BaseEntity attacker)
        {
            if (IsNull(tc) || IsNull(attacker)) return;
            OnBeforeProtectedCupboardDestroyed(tc, attacker);
            var balance = tc.Balance;
            RemoveProtectedCupboard(tc);
            OnAfterProtectedCupboardDestroyed(tc, attacker, balance);
        }

        private void OnBeforeProtectedCupboardDestroyed(ProtectedCupboard tc, BaseEntity attacker)
        {
        }

        private void OnAfterProtectedCupboardDestroyed(ProtectedCupboard tc, BaseEntity attacker, double balance)
        {
            var awardBalance = config.Protection.AwardRemainingBalance && balance > 0;
            LogAction(tc.EntityID, $"Destroyed by {GetEntityDisplayName(attacker)}. Awarded {(awardBalance ? balance : 0)}");
            if (awardBalance && attacker is BasePlayer && !attacker.IsNpc)
            {
                GiveBalanceResource((BasePlayer)attacker, balance);
                Message((BasePlayer)attacker, "message awarded", FormatCurrency((float)balance, true));
            }
        }
        #endregion

        #region Structure Updates
        private void UpdateProtectedStructure(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
            OnBeforeProtectedStructureUpdated(tc);
            tc.FoundationCount = null;
            tc.BuildingCostPerHour = null;
            tc.TotalProtectionCostPerHour = null;
            OnAfterProtectedStructureUpdated(tc);
        }

        private void OnBeforeProtectedStructureUpdated(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
        }

        private void OnAfterProtectedStructureUpdated(ProtectedCupboard tc)
        {
            if (IsNull(tc)) return;
        }
        #endregion

        #region Viewing Cupboard
        private void UpdateProtectedCupboardInventory(ProtectedCupboard tc, BasePlayer basePlayer)
        {
            if (IsNull(tc)) return;
            if (PlayersViewingOverlay.Contains(basePlayer.userID))
            {
                ShowProtectionStatusOverlay(basePlayer, tc);
            }
        }
        private void OpenProtectedCupboard(ProtectedCupboard tc, BasePlayer basePlayer)
        {
            if (IsNull(tc)) return;
            OnBeforeProtectedCupboardOpened(tc, basePlayer);
            ShowProtectionStatusOverlayTabs(basePlayer, tc);
            if (!tc.CurrentlyViewing.Contains(basePlayer))
            {
                tc.CurrentlyViewing.Add(basePlayer);
                LogAction(tc.EntityID, $"Player {basePlayer.displayName} started viewing");
            }
            OnAfterProtectedCupboardOpened(tc, basePlayer);
        }

        private void OnBeforeProtectedCupboardOpened(ProtectedCupboard tc, BasePlayer basePlayer)
        {
        }

        private void OnAfterProtectedCupboardOpened(ProtectedCupboard tc, BasePlayer basePlayer)
        {
        }

        private void CloseProtectedCupboard(ProtectedCupboard tc, BasePlayer basePlayer)
        {
            if (IsNull(tc)) return;
            OnBeforeProtectedCupboardClosed(tc, basePlayer);
            CloseInfoPanel(basePlayer);
            CloseProtectionStatusOverlay(basePlayer);
            CloseProtectionStatusOverlayTabs(basePlayer);
            if (tc.CurrentlyViewing.Contains(basePlayer))
            {
                tc.CurrentlyViewing.Remove(basePlayer);
                LogAction(tc.EntityID, $"Player {basePlayer.displayName} stopped viewing");
            }
            OnAfterProtectedCupboardClosed(tc, basePlayer);
        }

        private void OnBeforeProtectedCupboardClosed(ProtectedCupboard tc, BasePlayer basePlayer)
        {
        }

        private void OnAfterProtectedCupboardClosed(ProtectedCupboard tc, BasePlayer basePlayer)
        {
        }
        #endregion

        #region Protected Area

        #endregion

        private bool IsNull(object obj)
        {
            if (obj == null)
            {
                return true;
            }
            return false;
        }

        private void NotifyProtectionStatusChanged(BasePlayer basePlayer, ProtectedCupboard tc, bool ignoreCooldown = false, double deducted = 0)
        {
            ShowIndicator(basePlayer, tc, ignoreCooldown, deducted);
            if (config.Integration.Notify)
            {
                NotifyHelper.ShowProtectionStatus(basePlayer, tc);
            }
            if (config.Integration.CustomStatusFramework)
            {
                CustomStatusFrameworkHelper.ShowProtectionStatus(basePlayer, tc);
                CustomStatusFrameworkHelper.ShowProtectionBalanceDeducted(basePlayer, tc);
            }
            SetIndicatorCooldownOrClose(basePlayer, tc);
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        private void LogAction(uint entityId, string message)
        {
            if (config.EnableLogging)
            {
                var now = DateTime.Now;
                var msg = $"{now,-30}{entityId, -20}{message}";
                var fileName = $"tc_{entityId}";
                LogToFile(fileName, msg, this, true);
            }
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        protected override void LoadDefaultMessages()
        {
            lang.RegisterMessages(new Dictionary<string, string>
            {
                ["title"] = "raid protection",
                ["tc id"] = "ID: {0}",
                ["message awarded"] = "You have been awarded the remaining protection balance of {0}.",
                ["command changelog"] = "<color=yellow>New changes in Raid Protecion v{0}:</color>{1}",
                ["command help title"] = "Raid Protection Admin Commands",
                ["command success"] = "The command was successful.",
                ["command error"] = "There was an error with the command.",
                ["command all args required"] = "All arguments are required when using a command from the server console.",
                ["command usage"] = "Usage: /{0} {1} {2}",
                ["command player not found"] = "The player {0} could not be found.",
                ["command invalid value"] = "Invalid value {0} was given.",
                ["command no permission"] = "The permission '{0}' is required to use this command.",
                ["command cannot remove auth"] = "Authorized owners cannot be removed. They must first deauthorize from the tool cupboard.",
                ["command id"] = "The tool cupboard id is {0}",
                ["command owners"] = "Owners of tool cupboard {0}\n{1}",
                ["command founder"] = "Founder of tool cupboard {0} is {1} and inherits their protection level rank {2}",
                ["command protection"] = "Protection for tool cupboard {0}\nCurrent {1}%\nOnline {2}%\nOffline {3}%\nLevel {4}",
                ["command cost"] = "The hourly costs for tool cupboard {0}\nBase {1}\nPer Owner {2} ({3})\nPer Floor {4} ({5})\nTotal {6}",
                ["command balance"] = "Balance for tool cupboard {0} is {1}",
                ["command status"] = "Protection status for tool cupboard {0}\nStatus {1}\nReason {2}",
                ["message authorize admin"] = "<color=yellow>IMPORTANT:</color> You have authorized on this tool cupboard as an admin. You will be counted as an owner and affect online/offline protection until you deauthorize yourself.",
                ["message deauthorize admin"] = "<color=yellow>IMPORTANT:</color> As an admin, you have been removed as an owner from this tool cupboard. The online/offline protection will no longer be affected.",
                ["balance"] = "balance",
                ["cost"] = "cost",
                ["max time"] = "max time",
                ["note maxed"] = "Max protection time reached",
                ["note paused"] = "Toggle to resume protection",
                ["note damaged"] = "Protection starts in {0} seconds",
                ["status protected"] = "Protected",
                ["status unprotected"] = "Unprotected",
                ["status pending"] = "Pending",
                ["reason paused"] = "protection paused, resume to continue protection",
                ["reason offline only"] = "Protected by {0}% for {1} when owners are offline",
                ["reason online only"] = "Protected by {0}% for {1} when owners are online",
                ["reason recently damaged"] = "took damage, protection delayed",
                ["reason insufficient item"] = "add {0} more {1} to tool cupboard",
                ["reason insufficient balance"] = "purchase at least 1 hour of protection time",
                ["reason no permission"] = "no protection permission",
                ["reason no founder"] = "no founder",
                ["reason pending offline only"] = "offline protection starts in {0} seconds",
                ["ui info text"] = "Your base can be protected from damage at the cost of resources. Purchase protection time to ensure your base cannot be raided. Clear to refund balance.",
                ["ui cost per hour"] = "Cost per hour",
                ["ui damage cost"] = "Protected damage will reduce your balance!",
                ["ui protected"] = "Protected by {0}% for {1}",
                ["ui button 1h"] = "+1 hour",
                ["ui button 24h"] = "+24 hour",
                ["ui button max"] = "+Max",
                ["ui button clear"] = "Clear",
                ["ui button info"] = "Info",
                ["ui button pause"] = "Pause",
                ["ui button resume"] = "Resume",
                ["ui max time reached"] = "Max Time Reached",
                ["ui tab upkeep"] = "Upkeep",
                ["ui tab protection"] = "Protection",
                ["ui your balance"] = "You have {0}",
                ["info need authorization"] = "You must be authorized on this tool cupboard or and admin to view it's protection info.",
                ["info owners count"] = "Owners {0}",
                ["info protection"] = "Protection {0}",
                ["info balance"] = "Balance {0}",
                ["info owners persistent"] = "Owners are permanent once authorized and are not removed when deauthorized.",
                ["info owners"] = "Owners",
                ["info online"] = "Online",
                ["info authorized"] = "Authorized",
                ["info others"] = "+{0} Others",
                ["info founder"] = "(founder)",
                ["info clan"] = "(clan)",
                ["info protection rank"] = "Protection Rank {0} (from founder)",
                ["info protection current"] = "Current",
                ["info protection online"] = "Online",
                ["info protection offline"] = "Offline",
                ["info max protection time"] = "Max Protection Time",
                ["info no limit"] = "No Limit",
                ["info hours"] = "{0} Hours",
                ["info cost when protected"] = "Cost When Protected",
                ["info protection time delay"] = "Protection Time Delay",
                ["info delay seconds"] = "{0} sec",
                ["info starts when owners offline"] = "Starts when all owners are offline",
                ["info protected entities"] = "Protected Entities",
                ["info protected animals"] = "Animals",
                ["info protected buildings"] = "Buildings",
                ["info protected deployables"] = "Deployables",
                ["info protected electrical"] = "Electrical",
                ["info protected horses"] = "Horses",
                ["info protected loot nodes"] = "Loot Nodes",
                ["info protected npcs"] = "NPCs",
                ["info protected unauthed players"] = "Unauthed Players",
                ["info protected authed players"] = "Authed Players",
                ["info protected vehicles"] = "Vehicles",
                ["info protected attack heli"] = "Attack Heli",
                ["info protected"] = "Protected",
                ["info from"] = "From",
                ["info total hourly cost"] = "Total Hourly Cost",
                ["info balance label"] = "Balance",
                ["info balance info"] = "Your protection balance must stay above zero (unless the cost is free) in order to maintain raid protection. If the balance falls below zero, you will be unprotected.",
                ["info hourly base cost"] = "Hourly Base Cost",
                ["info hourly base cost note"] = "From Rank {0} Protection",
                ["info hourly base cost info"] = "Fixed cost charged per hour, based on your protection level.",
                ["info hourly foundation cost"] = "Hourly Floor Cost",
                ["info hourly foundation cost note"] = "{0} Floors x {1}",
                ["info hourly foundation cost info"] = "Charged every hour for each floor in your base. Building larger structures will cost make protection cost more.",
                ["info hourly owner cost"] = "Hourly Owner Cost",
                ["info hourly owner cost note"] = "{0} Owners x {1}",
                ["info hourly owner cost info"] = "The amount charged for each owner of this structure. Having a larger team will make protection cost more.",
                ["indicator balance reduced amount"] = "-{0} Balance",
                ["levels protection"] = "Protection",
                ["levels costs"] = "Costs",
                ["levels online"] = "Online",
                ["levels offline"] = "Offline",
                ["levels max hours"] = "Max Hours",
                ["levels base cost"] = "Base",
                ["levels foundation cost"] = "Per Floor",
                ["levels owner cost"] = "Per Owner",
                ["levels damage cost"] = "Per Damage",
                ["levels delays"] = "Delays",
                ["levels after offline"] = "After Offline",
                ["levels after damaged"] = "After Damaged",
                ["levels protection level"] = "Your protection level is Rank {0}",
                ["levels description"] = "Tool cupboards that you place will inherit your protection level.",
                ["levels no limit"] = "No Limit",
                ["notify protected"] = "Protected {0}%",
                ["notify unprotected"] = "Unprotected",
                ["customstatusframework raid protected"] = "Raid Protected",
                ["customstatusframework protected"] = "Protected",
                ["customstatusframework balance"] = "Balance"
            }, this);
        }

        private string Lang(string key, params object[] args) => string.Format(lang.GetMessage(key, this), args);
        private string Lang(string key, string id, params object[] args) => string.Format(lang.GetMessage(key, this, id), args);
        private string Lang(string key, BasePlayer basePlayer, params object[] args) => string.Format(lang.GetMessage(key, this, basePlayer?.UserIDString), args);
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        public class ProtectedCupboard
        {
            public ProtectedCupboard()
            {

            }

            public ProtectedCupboard(BuildingPrivlidge priv)
            {
                this.EntityID = priv.net.ID;
                this.BuildingPrivlidge = priv;
            }

            private BuildingPrivlidge _buildingPrivlidge = null;
            private float? _ownerCost = null;
            private float? _buildingCost = null;
            private float? _totalProtectionCost = null;
            private int? _foundationCount = null;
            private float _balance = 0f;
            private float? _totalCostPerInterval = null;
            private BasePlayer _founder = null;
            private List<BasePlayer> _owners = null;
            private List<BasePlayer> _onlineOwners = null;
            private ProtectionLevel _highestProtectionLevel = null;

            public uint EntityID { get; set; }

            public bool Enabled { get; set; } = true;

            public Dictionary<ulong, OwnerHistoryEntry> OwnerHistory { get; set; } = new Dictionary<ulong, OwnerHistoryEntry>();

            public float Balance
            {
                get
                {
                    return _balance;
                }
                set
                {
                    _balance = Math.Min(MaxBalance ?? float.MaxValue, Math.Max(0, value));
                }
            }

            public ProtectionStatus Status { get; set; } = ProtectionStatus.Unprotected;

            public bool IsPending => !IsProtected && Reason == StatusReason.OfflineOnly || Reason == StatusReason.RecentlyTookDamage;

            public StatusReason Reason
            {
                get
                {
                    if (Founder == null)
                    {
                        return StatusReason.NoFounder;
                    }
                    else if (HighestProtectionLevel == ProtectionLevel.NONE)
                    {
                        return StatusReason.NoPermission;
                    }
                    else if (!HasRemainingProtection && !HasCurrencyItemAmount)
                    {
                        return StatusReason.InsufficientItem;
                    }
                    else if (!HasRemainingProtection)
                    {
                        return StatusReason.InsufficientBalance;
                    }
                    else if (!Enabled)
                    {
                        return StatusReason.Paused;
                    }
                    else if (RemainingDamageDelaySeconds > 0)
                    {
                        return StatusReason.RecentlyTookDamage;
                    }
                    else if (!HasOnlineProtection && HasOfflineProtection && HasOwnersOnline)
                    {
                        return StatusReason.OfflineOnly;
                    }
                    else if (HasOnlineProtection && !HasOfflineProtection && !HasOwnersOnline)
                    {
                        return StatusReason.OnlineOnly;
                    }
                    else if (RemainingOfflineProtectionDelaySeconds > 0)
                    {
                        return StatusReason.PendingOfflineOnly;
                    }
                    return StatusReason.NoReason;
                }
            }

            [JsonIgnore]
            public List<BalanceLedgerEntry> BalanceLedger { get; set; } = new List<BalanceLedgerEntry>();

            [JsonIgnore]
            public float? MaxBalance
            {
                get
                {
                    return !HasProtectionTimeLimit ? null : MaxProtectionTimeHours * TotalProtectionCostPerHour;
                }
            }

            [JsonIgnore]
            public bool HasFreeProtection
            {
                get
                {
                    return TotalProtectionCostPerHour <= 0;
                }
            }

            [JsonIgnore]
            public float? AllowedBalanceRemaining
            {
                get
                {
                    return !HasProtectionTimeLimit ? null : MaxBalance - Balance;
                }
            }

            [JsonIgnore]
            public bool HasOnlineProtection
            {
                get
                {
                    return HighestProtectionLevel.HasOnlineProtection;
                }
            }

            [JsonIgnore]
            public bool IsAtMaxBalance
            {
                get
                {
                    if (!HasProtectionTimeLimit) { return false; }
                    return Balance >= MaxBalance;
                }
            }

            [JsonIgnore]
            public bool HasOfflineProtection
            {
                get
                {
                    return HighestProtectionLevel.HasOfflineProtection;
                }
            }

            [JsonIgnore]
            public bool HasOwnersOnline
            {
                get
                {
                    return OnlineOwners.Count > 0;
                }
            }

            [JsonIgnore]
            public ProtectionLevel HighestProtectionLevel
            {
                get
                {
                    if (_highestProtectionLevel == null)
                    {
                        _highestProtectionLevel = HasFounder ? ProtectionLevel.GetProtectionLevelOfPlayer(Founder) : ProtectionLevel.NONE;
                    }
                    return _highestProtectionLevel;
                }
                set
                {
                    _highestProtectionLevel = value;
                }
            }

            [JsonIgnore]
            public bool HasProtectionTimeLimit
            {
                get
                {
                    return HighestProtectionLevel.HasProtectionTimeLimit;
                }
            }

            [JsonIgnore]
            public int? MaxProtectionTimeHours
            {
                get
                {
                    return HighestProtectionLevel.MaxProtectionTimeHours;
                }
            }

            [JsonIgnore]
            public float HoursOfProtection
            {
                get
                {
                    return TotalProtectionCostPerHour <= 0 ? float.PositiveInfinity : HasProtectionTimeLimit ? Math.Min(MaxProtectionTimeHours ?? 0, Balance / TotalProtectionCostPerHour ?? 0) : Balance / TotalProtectionCostPerHour ?? 0;
                }
            }

            [JsonIgnore]
            public float CostPerDamageProtected
            {
                get
                {
                    return HighestProtectionLevel.CostPerDamageProtected;
                }
            }

            [JsonIgnore]
            public bool HasCostPerDamageProtected
            {
                get
                {
                    return CostPerDamageProtected > 0f;
                }
            }

            [JsonIgnore]
            public float OfflineProtectionPercent
            {
                get
                {
                    return HighestProtectionLevel.OfflineProtectionPercent;
                }
            }

            [JsonIgnore]
            public float OnlineProtectionPercent
            {
                get
                {
                    return HighestProtectionLevel.OnlineProtectionPercent;
                }
            }

            [JsonIgnore]
            public float CurrentProtectionPercent
            {
                get
                {
                    return Status != ProtectionStatus.Protected ? 0f : (HasOwnersOnline ? OnlineProtectionPercent : OfflineProtectionPercent);
                }
            }

            [JsonIgnore]
            public BuildingPrivlidge BuildingPrivlidge
            {
                get
                {
                    if (_buildingPrivlidge == null)
                    {
                        _buildingPrivlidge = (BuildingPrivlidge)BaseNetworkable.serverEntities.Find(EntityID);
                    }
                    return _buildingPrivlidge;
                }
                set
                {
                    _buildingPrivlidge = value;
                }
            }

            [JsonIgnore]
            public float? TotalProtectionCostPerHour
            {
                get
                {
                    if (_totalProtectionCost == null)
                    {
                        _totalProtectionCost = BaseCostPerHour + BuildingCostPerHour + OwnerCostPerHour;
                    }
                    return _totalProtectionCost ?? 0;
                }
                set
                {
                    _totalProtectionCost = value;
                }
            }

            [JsonIgnore]
            public float? OwnerCostPerHour
            {
                get
                {
                    if (_ownerCost == null)
                    {
                        _ownerCost = Owners.Count * HighestProtectionLevel.HourlyCostPerOwner;
                    }
                    return _ownerCost ?? 0;
                }
                set
                {
                    _ownerCost = value;
                }
            }

            [JsonIgnore]
            public float? BuildingCostPerHour
            {
                get
                {
                    if (_buildingCost == null)
                    {
                        _buildingCost = FoundationCount * HighestProtectionLevel.HourlyCostPerFloor;
                    }
                    return _buildingCost ?? 0;
                }
                set
                {
                    _buildingCost = value;
                }
            }

            [JsonIgnore]
            public int? FoundationCount
            {
                get
                {
                    if (_foundationCount == null)
                    {
                        if (BuildingPrivlidge == null || BuildingPrivlidge.GetBuilding() == null)
                        {
                            return 0;
                        }
                        _foundationCount = BuildingPrivlidge.GetBuilding().buildingBlocks.Where(x => 
                        {
                            var shortPrefabName = x.ShortPrefabName;
                            return shortPrefabName.StartsWith("foundation") || shortPrefabName.StartsWith("floor");
                        }).Count();
                    }
                    return _foundationCount ?? 0;
                }
                set
                {
                    _foundationCount = value;
                }
            }

            [JsonIgnore]
            public float BaseCostPerHour
            {
                get
                {
                    return HighestProtectionLevel.HourlyBaseCost;
                }
            }

            [JsonIgnore]
            public List<BasePlayer> AuthedPlayers
            {
                get
                {
                    return BuildingPrivlidge == null ? new List<BasePlayer>() : BuildingPrivlidge.authorizedPlayers.Select(x => BasePlayer.FindAwakeOrSleeping(x.userid.ToString())).Where(x => x != null).ToList();
                }
            }

            [JsonIgnore]
            public bool HasFounder
            {
                get
                {
                    return Founder != null;
                }
            }

            [JsonIgnore]
            public ulong? FounderUserId
            {
                get
                {
                    return BuildingPrivlidge == null ? null : (ulong?)BuildingPrivlidge.OwnerID;
                }
            }

            [JsonIgnore]
            public string FounderDisplayName
            {
                get
                {
                    return HasFounder ? Founder.displayName : "None";
                }
            }


            [JsonIgnore]
            public BasePlayer Founder
            {
                get
                {
                    if (BuildingPrivlidge != null && FounderUserId != null && (_founder == null || FounderUserId != _founder.userID ))
                    {
                        _founder = BasePlayer.FindAwakeOrSleeping(FounderUserId.Value.ToString());
                    }
                    return _founder;
                }
                set
                {
                    BuildingPrivlidge.OwnerID = value == null ? 0 : value.userID;
                    _founder = null;
                }
            }

            [JsonIgnore]
            public bool IsFounderAdmin
            {
                get
                {
                    return PLUGIN.IsAdmin(Founder);
                }
            }

            [JsonIgnore]
            public List<BasePlayer> Owners
            {
                get
                {
                    if (_owners == null)
                    {
                        var temp = AuthedPlayers;
                        if (OwnerHistory != null)
                        {
                            if (HasFounder)
                            {
                                temp.AddRange(ClansHelper.GetClanMembers(Founder));
                            }
                            temp.AddRange(OwnerHistory.Where(x => x.Value.BasePlayer != null && (!PLUGIN.config.Protection.RemoveAdminOwners || FounderUserId == x.Value.BasePlayer.userID || !PLUGIN.IsAdmin(x.Value.BasePlayer))).Select(x => x.Value.BasePlayer));
                            // remove any duplicate players
                            temp = temp
                                .Where(x => x != null)
                                .GroupBy(x => x.userID)
                                .Select(group => group.First())
                                .ToList();
                        }
                        _owners = temp;
                    }
                    return _owners;
                }
                set
                {
                    _owners = value;
                }
            }

            [JsonIgnore]
            public List<BasePlayer> OnlineOwners
            {
                get
                {
                    if (_onlineOwners == null)
                    {
                        _onlineOwners = Owners.Where(x => x != null && x.IsConnected).ToList();
                    }
                    return _onlineOwners;
                }
                set
                {
                    _onlineOwners = value;
                }
            }

            [JsonIgnore]
            public List<BasePlayer> OnlineOwnersInBuildingZone
            {
                get
                {
                    return OnlineOwners.Where(x => x != null && x.GetBuildingPrivilege()?.net.ID == EntityID).ToList();
                }
            }

            [JsonIgnore]
            public HashSet<BasePlayer> CurrentlyViewing { get; } = new HashSet<BasePlayer>();

            [JsonIgnore]
            public int CurrencyItemCount
            {
                get
                {
                    var priv = BuildingPrivlidge;
                    if (priv == null) { return 0; }
                    var count = 0;
                    foreach (var item in priv.inventory.itemList)
                    {
                        if (item.info.shortname.ToLower() == PLUGIN.config.Protection.CurrencyItem.ToLower())
                        {
                            count += item.amount;
                        }
                    }
                    return count;
                }
            }

            [JsonIgnore]
            public int CurrencyItemRemainingAmountRequired
            {
                get
                {
                    return (int)Math.Ceiling(TotalProtectionCostPerHour.Value) - CurrencyItemCount;
                }
            }

            [JsonIgnore]
            public bool HasCurrencyItemAmount
            {
                get
                {
                    return (PLUGIN.config.Integration.Economics || PLUGIN.config.Integration.ServerRewards) ? true : PLUGIN.HasBalanceResourceAmount(this, null, TotalProtectionCostPerHour ?? 0);
                }
            }

            [JsonIgnore]
            public bool HasRemainingProtection
            {
                get
                {
                    return (HoursOfProtection > 0f && Balance > 0) || (TotalProtectionCostPerHour.HasValue && TotalProtectionCostPerHour <= 0);
                }
            }

            [JsonIgnore]
            public bool IsProtected
            {
                get
                {
                    return Status == ProtectionStatus.Protected;
                }
            }

            [JsonIgnore]
            public bool CanOfflineProtectionStart
            {
                get
                {
                    return HasRemainingProtection && HasOfflineProtection && !HasOwnersOnline && !IsProtected;
                }
            }

            [JsonIgnore]
            public bool SkipNextProtectionStartDelay { get; set; } = true;

            [JsonIgnore]
            public int ProtectionStartDelay
            {
                get
                {
                    if (SkipNextProtectionStartDelay)
                    {
                        return 0;
                    }
                    return Math.Max(RemainingDamageDelaySeconds, RemainingOfflineProtectionDelaySeconds);
                }
            }

            [JsonIgnore]
            public int RemainingOfflineProtectionDelaySeconds
            {
                get
                {
                    return !CanOfflineProtectionStart ? 0 : HighestProtectionLevel.OfflineProtectionDelay;
                }
            }

            [JsonIgnore]
            public DateTime? LastDamagedTime { get; set; } = null;

            [JsonIgnore]
            public int RemainingDamageDelaySeconds
            {
                get
                {
                    var remaining = (int)(LastDamagedTime == null ? 0 : (LastDamagedTime.Value.AddSeconds(HighestProtectionLevel.ProtectedDelayAfterTakingDamage) - DateTime.Now).TotalSeconds);
                    if (remaining <= 0)
                    {
                        LastDamagedTime = null;
                        remaining = 0;
                    }
                    return remaining;
                }
            }

            public float TotalCostPerInterval(int intervalTimeSeconds)
            {
                if (_totalCostPerInterval == null || _totalProtectionCost == null)
                {
                    _totalCostPerInterval = TotalProtectionCostPerHour / 60f * (intervalTimeSeconds / 60f);
                }
                return _totalCostPerInterval ?? 0;
            }

            public void ClearCache()
            {
                _buildingPrivlidge = null;
                _totalProtectionCost = null;
                _buildingCost = null;
                _foundationCount = null;
                _highestProtectionLevel = null;
                _onlineOwners = null;
            }
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        public static class ProtectedCupboardManager
        {
            public static Dictionary<uint, ProtectedCupboard> _protectedCupboards = new Dictionary<uint, ProtectedCupboard>();

            public static ProtectedCupboard InitProtectedCupboard(BuildingPrivlidge priv)
            {
                if (_protectedCupboards.ContainsKey(priv.net.ID))
                {
                    return _protectedCupboards[priv.net.ID];
                }
                var tc = new ProtectedCupboard(priv);
                _protectedCupboards.Add(priv.net.ID, tc);
                return tc;
            }
            public static bool RemoveProtectedCupboard(uint entityID)
            {
                return _protectedCupboards.Remove(entityID);
            }
            public static bool ProtectedCupboardExists(uint entityID)
            {
                return _protectedCupboards.ContainsKey(entityID);
            }
            public static ProtectedCupboard GetByID(uint entityID)
            {
                ProtectedCupboard tc = null;
                _protectedCupboards.TryGetValue(entityID, out tc);
                return tc;
            }
            public static List<ProtectedCupboard> GetCupboardsForOwner(BasePlayer basePlayer)
            {
                return _protectedCupboards.Values.Where(x => x.Owners.Contains(basePlayer)).ToList();
            }
            public static List<ProtectedCupboard> GetCupboardsForFounder(BasePlayer basePlayer)
            {
                return _protectedCupboards.Values.Where(x => x.HasFounder && x.Founder.userID == basePlayer.userID).ToList();
            }
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        public class ProtectionLevel
        {
            public static ProtectionLevel NONE { get; set; } = new ProtectionLevel() { MaxProtectionTimeHours = 0 };
            [JsonProperty(PropertyName = "Rank")]
            public int Rank { get; set; } = 0;

            [JsonProperty(PropertyName = "Online protection percentage (0-100)")]
            public float OnlineProtectionPercent { get; set; } = 0;

            [JsonProperty(PropertyName = "Offline protection percentage (0-100)")]
            public float OfflineProtectionPercent { get; set; } = 0;

            [JsonProperty(PropertyName = "Hourly cost per authorized player")]
            public float HourlyCostPerOwner { get; set; } = 0;

            [JsonProperty(PropertyName = "Hourly cost per floor")]
            public float HourlyCostPerFloor { get; set; } = 0;

            [JsonProperty(PropertyName = "Hourly base cost")]
            public float HourlyBaseCost { get; set; } = 0;

            [JsonProperty(PropertyName = "Cost per damage protected")]
            public float CostPerDamageProtected { get; set; } = 0f;

            [JsonProperty(PropertyName = "Max protection time (hours)")]
            public int? MaxProtectionTimeHours { get; set; } = null;

            [JsonProperty(PropertyName = "Delay for offline protection (seconds)")]
            public int OfflineProtectionDelay { get; set; } = 5;

            [JsonProperty(PropertyName = "Delay after taking damage (seconds)")]
            public int ProtectedDelayAfterTakingDamage { get; set; } = 10;

            [JsonIgnore]
            public bool HasOnlineProtection { get { return OnlineProtectionPercent > 0f; } }

            [JsonIgnore]
            public bool HasOfflineProtection { get { return OfflineProtectionPercent > 0f; } }

            [JsonIgnore]
            public bool HasProtectionTimeLimit { get { return MaxProtectionTimeHours != null; } }

            public static ProtectionLevel GetProtectionLevelOfPlayer(BasePlayer basePlayer)
            {
                try
                {
                    if (basePlayer != null)
                    {
                        var levels = PLUGIN.config.Protection.ProtectionLevels.OrderByDescending(x => x.Rank);
                        foreach (var pl in levels)
                        {
                            var i = pl.Rank;
                            if (PLUGIN.permission.UserHasPermission(basePlayer.UserIDString, PLUGIN.PermissionLevel + i) || PLUGIN.permission.GetUserGroups(basePlayer.UserIDString).Any(groupid => PLUGIN.permission.GroupHasPermission(groupid, PLUGIN.PermissionLevel + i)))
                            {
                                return pl;
                            }
                        }
                    }
                    return NONE;
                } catch (NullReferenceException) { return NONE; }
            }
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        public enum ProtectionStatus
        {
            Unprotected,
            Pending,
            Protected
        }

        public static string ProtectionStatusToString(ProtectedCupboard tc, BasePlayer basePlayer, ProtectionStatus status)
        {
            switch (status)
            {
                case ProtectionStatus.Protected:
                    return PLUGIN.Capitalize(PLUGIN.Lang("status protected", basePlayer));
                case ProtectionStatus.Pending:
                    return PLUGIN.Capitalize(PLUGIN.Lang("status pending", basePlayer));
                case ProtectionStatus.Unprotected:
                    return PLUGIN.Capitalize(PLUGIN.Lang("status unprotected", basePlayer));
                default:
                    return "default";
            }
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        void OnEntitySpawned(BuildingPrivlidge priv)
        {
            NextTick(() =>
            {
                if (priv != null)
                {
                    AddProtectedCupboard(priv);
                }
            });
        }

        private void OnEntityBuilt(Planner plan, GameObject go)
        {
            if (plan == null || go?.name == "assets/prefabs/deployable/tool cupboard/cupboard.tool.deployed.prefab") return;
            BuildingPrivlidge priv = plan.GetBuildingPrivilege();
            if (priv != null)
            {
                var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                if (tc != null)
                {
                    UpdateProtectedStructure(tc);
                }
            }
        }

        void OnEntityKill(BuildingPrivlidge priv)
        {
            if (priv != null)
            {
                if (ProtectedCupboardManager.ProtectedCupboardExists(priv.net.ID))
                {
                    var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                    RemoveProtectedCupboard(tc);
                }
            }
        }

        void OnEntityKill(BuildingBlock block)
        {
            BuildingPrivlidge priv = block.GetBuildingPrivilege();
            if (priv != null)
            {
                NextTick(() =>
                {
                    if (priv != null)
                    {
                        var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                        if (tc != null)
                        {
                            UpdateProtectedStructure(tc);
                        }
                    }
                });
            }
        }

        void OnEntityDeath(BuildingPrivlidge priv, HitInfo hitInfo)
        {
            if (priv != null && hitInfo != null)
            {
                if (ProtectedCupboardManager.ProtectedCupboardExists(priv.net.ID))
                {
                    var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                    DestroyProtectedCupboard(tc, hitInfo.Initiator);
                }
            }
        }

        void OnEntityDeath(BuildingBlock block, HitInfo hitInfo) => OnEntityKill(block);

        object OnCupboardAuthorize(BuildingPrivlidge priv, BasePlayer basePlayer)
        {
            NextTick(() =>
            {
                if (priv != null)
                {
                    var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                    OwnerAuthorized(tc, basePlayer);
                    UpdateOwners(tc);
                }
            });
            return null;
        }

        object OnCupboardClearList(BuildingPrivlidge priv, BasePlayer basePlayer)
        {
            NextTick(() =>
            {
                if (priv != null)
                {
                    var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                    if (config.Protection.RemoveAdminOwners)
                    {
                        foreach (var adminPlayer in tc.AuthedPlayers.Where(x => PLUGIN.IsAdmin(x)))
                        {
                            AdminOwnerDeauthorized(tc, basePlayer);
                        }
                    }
                    UpdateOwners(tc);
                }
            });
            return null;
        }

        object OnCupboardDeauthorize(BuildingPrivlidge priv, BasePlayer basePlayer)
        {
            NextTick(() =>
            {
                if (priv != null)
                {
                    var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                    if (IsAdmin(basePlayer) && config.Protection.RemoveAdminOwners)
                    {
                        AdminOwnerDeauthorized(tc, basePlayer);
                    }
                    UpdateOwners(tc);
                }
            });
            return null;
        }

        private object OnEntityTakeDamage(AnimatedBuildingBlock entity, HitInfo info)
        {
            return config.Protection.ProtectedEntities.Buildings ? OnEntityTakeDamageHelper(entity, info) : null;
        }

        private object OnEntityTakeDamage(SimpleBuildingBlock entity, HitInfo info)
        {
            return config.Protection.ProtectedEntities.Buildings ? OnEntityTakeDamageHelper(entity, info) : null;
        }

        private object OnEntityTakeDamage(BuildingBlock entity, HitInfo info)
        {
            return config.Protection.ProtectedEntities.Buildings ? OnEntityTakeDamageHelper(entity, info) : null;
        }

        private object OnEntityTakeDamage(DecayEntity entity, HitInfo info)
        {
            return config.Protection.ProtectedEntities.Containers ? OnEntityTakeDamageHelper(entity, info) : null;
        }

        private object OnEntityTakeDamage(StorageContainer entity, HitInfo info)
        {
            return config.Protection.ProtectedEntities.Containers ? OnEntityTakeDamageHelper(entity, info) : null;
        }

        private object OnEntityTakeDamage(LootContainer entity, HitInfo info)
        {
            return config.Protection.ProtectedEntities.LootNodes ? OnEntityTakeDamageHelper(entity, info) : null;
        }

        private object OnEntityTakeDamage(BaseVehicle entity, HitInfo info)
        {
            return config.Protection.ProtectedEntities.Vehicles ? OnEntityTakeDamageHelper(entity, info) : null;
        }

        private object OnEntityTakeDamage(Horse entity, HitInfo info)
        {
            return config.Protection.ProtectedEntities.Horses ? OnEntityTakeDamageHelper(entity, info) : null;
        }

        private object OnEntityTakeDamage(IOEntity entity, HitInfo info)
        {
            return config.Protection.ProtectedEntities.Electrical ? OnEntityTakeDamageHelper(entity, info) : null;
        }

        private object OnEntityTakeDamage(BasePlayer entity, HitInfo info)
        {
            return  (config.Protection.ProtectedEntities.AuthedPlayers || config.Protection.ProtectedEntities.UnauthedPlayers) ? OnEntityTakeDamagePlayerHelper(entity, info) : null;
        }

        private object OnEntityTakeDamage(NPCPlayer entity, HitInfo info)
        {
            return config.Protection.ProtectedEntities.NPCs ? OnEntityTakeDamageHelper(entity, info) : null;
        }

        private object OnEntityTakeDamage(BaseAnimalNPC entity, HitInfo info)
        {
            return config.Protection.ProtectedEntities.NPCs ? OnEntityTakeDamageHelper(entity, info) : null;
        }

        void OnLootEntity(BasePlayer basePlayer, BuildingPrivlidge priv)
        {
            if (basePlayer != null && priv != null)
            {
                var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                if (tc != null)
                {
                    OpenProtectedCupboard(tc, basePlayer);
                }
            }
        }

        void OnLootEntityEnd(BasePlayer basePlayer, BuildingPrivlidge priv)
        {
            if (basePlayer != null)
            {
                var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                if (tc != null)
                {
                    CloseProtectedCupboard(tc, basePlayer);
                }
            }
        }

        object OnLootNetworkUpdate(PlayerLoot loot)
        {
            if (loot.entitySource != null && loot.entitySource is BuildingPrivlidge)
            {
                ProtectedCupboard tc = ProtectedCupboardManager.InitProtectedCupboard((BuildingPrivlidge)loot.entitySource);
                if (tc != null)
                {
                    BasePlayer basePlayer = loot._baseEntity;
                    if (basePlayer != null)
                    {
                        UpdateProtectedCupboardInventory(tc, basePlayer);
                        //OpenProtectedCupboard(tc, basePlayer);
                    }
                }
            }
            return null;
        }

        void OnPlayerConnected(BasePlayer basePlayer)
        {
            NextTick(() =>
            {
                if (basePlayer != null)
                {
                    var cupboards = ProtectedCupboardManager.GetCupboardsForOwner(basePlayer);
                    foreach (var tc in cupboards)
                    {
                        UpdateOnlineOwners(tc);
                    }
                }
            });
        }

        void OnPlayerDisconnected(BasePlayer basePlayer, string reason)
        {
            NextTick(() =>
            {
                if (basePlayer != null)
                {
                    var cupboards = ProtectedCupboardManager.GetCupboardsForOwner(basePlayer);
                    foreach (var tc in cupboards)
                    {
                        UpdateOnlineOwners(tc);
                    }
                }
            });
        }

        void OnUserPermissionGranted(string id, string permName)
        {
            if (permName.StartsWith(PermissionLevel))
            {
                UpdatedPermission(id);
            }
        }

        void OnUserPermissionRevoked(string id, string permName)
        {
            if (permName.StartsWith(PermissionLevel))
            {
                UpdatedPermission(id);
            }
        }

        void OnGroupPermissionGranted(string name, string permName)
        {
            if (permName.StartsWith(PermissionLevel))
            {
                var users = players.All.Where(p => permission.UserHasGroup(p.Id, name)).Select(x => x.Id).ToList();
                foreach (var userIdString in users)
                {
                    UpdatedPermission(userIdString);
                }
            }
        }

        void OnGroupPermissionRevoked(string name, string permName)
        {
            if (permName.StartsWith(PermissionLevel))
            {
                permission.GetUsersInGroup(name);
                var users = players.All.Where(p => permission.UserHasGroup(p.Id, name)).Select(x => x.Id).ToList();
                foreach (var userIdString in users)
                {
                    UpdatedPermission(userIdString);
                }
            }
        }

        void OnPlayerKicked(BasePlayer basePlayer, string reason) => OnPlayerDisconnected(basePlayer, reason);

        #region Helper Methods
        private object OnEntityTakeDamagePlayerHelper(BasePlayer entity, HitInfo info)
        {
            if (entity == null || info == null) { return null; }
            var priv = entity.GetBuildingPrivilege();
            if (priv == null) { return null; }
            var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
            var authed = tc.AuthedPlayers.Contains(entity);
            if ((authed && config.Protection.ProtectedEntities.AuthedPlayers) || (!authed && config.Protection.ProtectedEntities.UnauthedPlayers))
            {
                return DamageProtectedStructure(tc, entity, info);
            }
            return null;
        }

        private object OnEntityTakeDamageHelper(BaseCombatEntity entity, HitInfo info)
        {
            if (entity != null && info != null)
            {
                var priv = entity.GetBuildingPrivilege();
                if (priv != null)
                {
                    var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                    return DamageProtectedStructure(tc, entity, info);
                }
            }
            return null;
        }

        private void UpdatedPermission(string id)
        {
            NextTick(() =>
            {
                var basePlayer = BasePlayer.Find(id);
                if (basePlayer != null)
                {
                    var cupboards = ProtectedCupboardManager.GetCupboardsForFounder(basePlayer);
                    foreach (var tc in cupboards)
                    {
                        UpdateOwners(tc);
                    }
                }
            });
        }

        private bool IsProtectedEntity(BaseEntity entity, ProtectedCupboard tc)
        {
            if (entity == null || tc == null)
            {
                return false;
            }
            else if (entity is AnimatedBuildingBlock || entity is BuildingBlock || entity is SimpleBuildingBlock)
            {
                return config.Protection.ProtectedEntities.Buildings;
            }
            else if (entity is LootContainer)
            {
                return config.Protection.ProtectedEntities.LootNodes;
            }
            else if (entity is DecayEntity || entity is StorageContainer)
            {
                return config.Protection.ProtectedEntities.Containers;
            }
            else if (entity is Horse)
            {
                return config.Protection.ProtectedEntities.Horses;
            }
            else if (entity is BaseVehicle)
            {
                return config.Protection.ProtectedEntities.Vehicles;
            }
            else if (entity is BaseNpc || entity is BaseAnimalNPC)
            {
                return config.Protection.ProtectedEntities.NPCs;
            }
            else if (entity is BasePlayer)
            {
                return tc.AuthedPlayers
                    .Where(x => x != null)
                    .Any(x => x.userID == ((BasePlayer)entity).userID)
                    ? config.Protection.ProtectedEntities.AuthedPlayers : config.Protection.ProtectedEntities.UnauthedPlayers;
            }
            return false;
        }
        #endregion

        #region Clans Hooks
        private void OnClanMemberJoined(string userID, List<string> memberUserIDs)
        {
            if (config.Integration.Clans)
            {
                ClanUpdated(memberUserIDs);
            }
        }

        private void OnClanGone(string userID, List<string> memberUserIDs)
        {
            if (config.Integration.Clans)
            {
                ClanUpdated(memberUserIDs);
            }
        }

        private void OnClanDisbanded(List<string> memberUserIDs)
        {
            if (config.Integration.Clans)
            {
                ClanUpdated(memberUserIDs);
            }
        }

        private void ClanUpdated(List<string> memberUserIDs)
        {
            var basePlayers = memberUserIDs
                .Select(x => BasePlayer.FindAwakeOrSleeping(x))
                .Where(x => x != null)
                .ToList();
            foreach(var basePlayer in basePlayers)
            {
                var tcs = ProtectedCupboardManager.GetCupboardsForFounder(basePlayer);
                foreach(var tc in tcs)
                {
                    UpdateOwners(tc);
                }
            }
        }
        #endregion
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        public enum StatusReason
        {
            NoReason,
            Paused,
            OfflineOnly,
            OnlineOnly,
            RecentlyTookDamage,
            InsufficientItem,
            InsufficientBalance,
            NoPermission,
            NoFounder,
            PendingOfflineOnly
        }

        public static string StatusReasonToString(ProtectedCupboard tc, BasePlayer basePlayer, StatusReason reason)
        {
            switch (reason)
            {
                case StatusReason.Paused:
                    return PLUGIN.Capitalize(PLUGIN.Lang("reason paused", basePlayer));
                case StatusReason.OfflineOnly:
                    return PLUGIN.Capitalize(PLUGIN.Lang("reason offline only", basePlayer, tc.OfflineProtectionPercent, PLUGIN.FormatTimeShort(tc.HoursOfProtection, false)));
                case StatusReason.OnlineOnly:
                    return PLUGIN.Capitalize(PLUGIN.Lang("reason online only", basePlayer, tc.OnlineProtectionPercent, PLUGIN.FormatTimeShort(tc.HoursOfProtection, false)));
                case StatusReason.RecentlyTookDamage:
                    return PLUGIN.Capitalize(PLUGIN.Lang("reason recently damaged", basePlayer));
                case StatusReason.InsufficientItem:
                   return PLUGIN.Capitalize(PLUGIN.Lang("reason insufficient item", basePlayer, tc.CurrencyItemRemainingAmountRequired, PLUGIN.config.Protection.CurrencyItem));
                case StatusReason.InsufficientBalance:
                    return PLUGIN.Capitalize(PLUGIN.Lang("reason insufficient balance", basePlayer));
                case StatusReason.NoPermission:
                    return PLUGIN.Capitalize(PLUGIN.Lang("reason no permission", basePlayer));
                case StatusReason.NoFounder:
                    return PLUGIN.Capitalize(PLUGIN.Lang("reason no founder", basePlayer));
                case StatusReason.PendingOfflineOnly:
                    return PLUGIN.Capitalize(PLUGIN.Lang("reason pending offline only", basePlayer, tc.RemainingOfflineProtectionDelaySeconds));
                default:
                    return "default";
            }
        }
    }
}

namespace Oxide.Plugins
{
	partial class HomeProtection
	{
		private static string ColorToHex(string color)
		{
			var split = color.Split(' ');
			var r = (int)Math.Round(float.Parse(split[0]) * 255f);
			var g = (int)Math.Round(float.Parse(split[1]) * 255f);
			var b = (int)Math.Round(float.Parse(split[2]) * 255f);
			return "#" + r.ToString("X2") + g.ToString("X2") + b.ToString("X2");
		}
		private string Opacity(string color, float opacity)
        {
			var split = color.Split(' ');
			return $"{split[0]} {split[1]} {split[2]} {opacity}";
        }

		private bool GiveBalanceResource(BasePlayer basePlayer, double amount)
		{
			if (IsNull(basePlayer)) { return false; }
			var userId = basePlayer.userID;
			if (config.Integration.Economics)
			{
				Economics?.Call("Deposit", userId, amount);
				return true;
			}
			else if (config.Integration.ServerRewards)
			{
				ServerRewards?.Call("AddPoints", userId, (int)Math.Ceiling(amount));
				return true;
			}
			else
			{
				var item = ItemManager.Create(CurrencyItemDef, (int)Math.Floor(amount));
				if (IsNull(item)) return false;
				basePlayer.GiveItem(item);
				return true;
			}
		}

		private bool ConsumeBalanceResource(ProtectedCupboard tc, BasePlayer basePlayer, double amount)
		{
			var priv = tc.BuildingPrivlidge;
			if (IsNull(basePlayer)) { return false; }
			if (priv != null)
            {
				var userId = basePlayer.userID;
				if (config.Integration.Economics)
                {
					Economics?.Call("Withdraw", userId, amount);
					return true;
				}
				else if (config.Integration.ServerRewards)
                {
					ServerRewards?.Call("TakePoints", userId, (int)Math.Ceiling(amount));
					return true;
				}
				else
                {
					var due = (int)Math.Ceiling(amount);
					foreach (var item in priv.inventory.itemList.ToArray())
					{
						if (item.info.shortname.ToLower() == config.Protection.CurrencyItem.ToLower())
						{
							if (item.amount <= due)
							{
								due -= item.amount;
								item.RemoveFromContainer();
							}
							else
							{
								item.UseItem(due);
								due -= item.amount;
							}
						}
						if (due <= 0)
                        {
							return true;
						}
					}
					return true;
				}
			}
			return false;
		}

		private ItemDefinition _currencyItemDef = null;
		private ItemDefinition CurrencyItemDef
        {
			get
            {
				if (_currencyItemDef == null)
                {
					_currencyItemDef = ItemManager.FindItemDefinition(config.Protection.CurrencyItem);
                }
				return _currencyItemDef;
            }
        }

		private string FormatTimeShort(float hours, bool seconds = true)
		{
			if (hours >= float.MaxValue)
            {
				return $"forever";
            }
			var time = TimeSpan.FromHours(hours);
			return $"{time.Days}d {time.Hours}h {time.Minutes}m{(seconds ? ($" {time.Seconds}s") : String.Empty)}";
		}

		private string FormatCurrency(float? amount, bool round = false)
		{
			if (amount == null)
            {
				amount = 0;
            }
			if (config.Integration.Economics)
			{
				if (round)
				{
					amount = (float?)Math.Round(amount.Value, 2);
				}
				return $"{amount:C}";
			}
			if (config.Integration.ServerRewards)
			{
				if (round)
                {
					amount = (float?) Math.Floor(amount.Value);
                }
				return $"{amount} RP";
			}
			if (round)
			{
				amount = (float?)Math.Floor(amount.Value);
			}
			return $"{amount} {CurrencyItemDef.displayName.translated}";

		}

		private double GetBalanceResourceAmount(ProtectedCupboard tc, BasePlayer basePlayer)
		{
			var priv = tc.BuildingPrivlidge;
			if (priv != null)
			{
				if (config.Integration.Economics)
				{
					var userId = basePlayer.userID;
					var bal = (double)Economics?.Call("Balance", userId);
					return bal;
				}
				else if (config.Integration.ServerRewards)
				{
					var userId = basePlayer.userID;
					int? points = (int?)ServerRewards?.Call("CheckPoints", userId);
					return points ?? 0;
				}
				else
				{
					var count = 0;
					foreach (var item in priv.inventory.itemList)
					{
						if (item.info.shortname.ToLower() == config.Protection.CurrencyItem.ToLower())
						{
							count += item.amount;
						}
					}
					return count;
				}
			}
			return 0;
		}

		private bool HasBalanceResourceAmount(ProtectedCupboard tc, BasePlayer basePlayer, double amount)
        {
			var priv = tc.BuildingPrivlidge;
			if (priv != null)
            {
				if (config.Integration.Economics)
				{
					if (IsNull(basePlayer)) { return false; }
					var userId = basePlayer.userID;
					var bal = (double)Economics?.Call("Balance", userId, amount);
					var due = amount;
					return bal >= due;
				}
				else if (config.Integration.ServerRewards)
				{
					if (IsNull(basePlayer)) { return false; }
					var userId = basePlayer.userID;
					int? points = (int?)ServerRewards?.Call("CheckPoints", userId);
					var due = (int)Math.Ceiling(amount);
					return points != null && points >= due;
				}
				else
				{
					var due = (int)Math.Ceiling(amount);
					var count = 0;
					foreach (var item in priv.inventory.itemList)
					{
						if (item.info.shortname.ToLower() == config.Protection.CurrencyItem.ToLower())
						{
							count += item.amount;
							if (count >= due)
                            {
								return true;
                            }
						}
					}
				}
			}
			return false;
		}

		private string GetEntityDisplayName(BaseEntity baseEntity)
        {
			if (baseEntity == null)
            {
				return "None";
            }
			if (baseEntity is BasePlayer && !baseEntity.IsNpc)
            {
				return ((BasePlayer)baseEntity).displayName;
            }
			return baseEntity.name;
        }

		private string ID(string parent, string id, bool guid = false)
        {
			return $"{parent}.{id}{(guid ? Guid.NewGuid().ToString() : string.Empty)}";
        }

		private string Capitalize(string str)
        {
			if (str == null)
				return null;
			if (str.Length > 1)
				return char.ToUpper(str[0]) + str.Substring(1);
			return str.ToUpper();
		}

		private void MessageNonLocalized(BasePlayer basePlayer, string message)
		{
			var icon = config.ChatMessageIconId;
			if (basePlayer == null)
			{
				Puts(message);
			}
			else
            {
				ConsoleNetwork.SendClientCommand(basePlayer.Connection, "chat.add", 2, icon, message);
			}
		}

		private void Message(BasePlayer basePlayer, string lang, params object[] args)
		{
			if (basePlayer == null)
            {
				Puts(Lang(lang, args));
            }
			else
            {
				MessageNonLocalized(basePlayer, Lang(lang, basePlayer, args));
			}
		}

		private T LoadDataFile<T>(string fileName)
		{
			try
			{
				return Interface.Oxide.DataFileSystem.ReadObject<T>($"{Name}/{fileName}");
			}
			catch (Exception ex)
			{
				return default(T);
			}
		}

        private void SaveDataFile<T>(string fileName, T data)
        {
            Interface.Oxide.DataFileSystem.WriteObject($"{Name}/{fileName}", data);
        }

		private bool IsAdmin(BasePlayer basePlayer)
        {
			if (basePlayer == null) { return false; }
			return permission.UserHasPermission(basePlayer.UserIDString, PermissionAdmin);
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        public static class CustomStatusFrameworkHelper
        {
            public static void CreateProtectionStatus()
            {
                PLUGIN.CustomStatusFramework?.Call("DeleteStatus", "protectionstatus");
                PLUGIN.CustomStatusFramework?.Call("DeleteStatus", "protectionstatus.offline");
                PLUGIN.timer.In(1f, () =>
                {
                    // Online Protection
                    Func<BasePlayer, bool> conditionOnline = (basePlayer) =>
                    {
                        if (basePlayer == null) { return false; }
                        var priv = basePlayer.GetBuildingPrivilege();
                        if (priv == null) { return false; }
                        var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                        if (tc == null) { return false; }
                        return tc.IsProtected && tc.Owners.Contains(basePlayer);
                    };
                    Func<BasePlayer, string> value = (basePlayer) =>
                    {
                        if (basePlayer == null) { return ""; }
                        var priv = basePlayer.GetBuildingPrivilege();
                        if (priv == null) { return ""; }
                        var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                        if (tc == null) { return ""; }
                        return tc.HasFreeProtection ? String.Empty : $"{(int)Math.Ceiling(tc.HoursOfProtection)}h";
                    };
                        PLUGIN.CustomStatusFramework?.Call("CreateDynamicStatus",
                            "protectionstatus",
                            RustColor.Green,
                            "Raid Protection",
                            RustColor.LightWhite,
                            RustColor.LightLime,
                            "status.protected",
                            RustColor.Lime,
                            conditionOnline, value);
                    // Offline Protection
                    Func<BasePlayer, bool> conditionOffline = (basePlayer) =>
                        {
                            if (basePlayer == null) { return false; }
                            var priv = basePlayer.GetBuildingPrivilege();
                            if (priv == null) { return false; }
                            var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                            if (tc == null) { return false; }
                            return tc.IsPending && tc.Owners.Contains(basePlayer);
                        };
                    PLUGIN.CustomStatusFramework?.Call("CreateStatus",
                        "protectionstatus.offline",
                        RustColor.Red,
                        "Raid Protection",
                        RustColor.LightOrange,
                        "Pending",
                        RustColor.LightOrange,
                        "status.protected",
                        RustColor.LightOrange,
                        conditionOffline);
                });
            }

            public static void ShowProtectionStatus(BasePlayer basePlayer, ProtectedCupboard tc)
            {
                if (basePlayer == null) return;
                if (PLUGIN.IsOffIndicatorCooldown(basePlayer))
                {
                    PLUGIN.CustomStatusFramework?.Call("UpdateStatus",
                        basePlayer,
                        "protectionstatuspopup",
                        RustColor.Maroon,
                        PLUGIN.Lang("customstatusframework protected", basePlayer),
                        RustColor.LightMaroon,
                        $"{tc.CurrentProtectionPercent}%",
                        RustColor.LightMaroon,
                        "status.protected",
                        RustColor.LightMaroon);
                }
            }

            public static void ShowProtectionBalanceDeducted(BasePlayer basePlayer, ProtectedCupboard tc)
            {
                if (basePlayer == null) return;
                if (PLUGIN.config.Indicator.ShowBalanceDeducted)
                {
                    double amount = 0;
                    if (!PLUGIN.AccumulatedCost.TryGetValue(basePlayer.UserIDString, out amount)) { return; }
                    var statusid = "protectionstatusdeductedpopup";
                    var amountRounded = String.Format("{0:0.00}", amount);
                    PLUGIN.CustomStatusFramework?.Call("UpdateStatus",
                        basePlayer,
                        statusid,
                        RustColor.Maroon,
                        PLUGIN.Lang("customstatusframework balance", basePlayer),
                        RustColor.LightMaroon,
                        $"-{amountRounded}",
                        RustColor.LightMaroon,
                        "status.protected",
                        RustColor.LightMaroon);
                }
            }

            public static void ClearProtectionStatus(BasePlayer basePlayer)
            {
                if (basePlayer == null) return;
                PLUGIN.CustomStatusFramework?.Call("ClearStatus", basePlayer, "protectionstatuspopup");
                PLUGIN.CustomStatusFramework?.Call("ClearStatus", basePlayer, "protectionstatusdeductedpopup");
            }

            public static void ClearAllProtectionStatuses()
            {
                foreach (var basePlayer in BasePlayer.activePlayerList.Where(x => x != null))
                {
                    ClearProtectionStatus(basePlayer);
                }
            }

            public static void DeleteProtectionStatus()
            {
                ClearAllProtectionStatuses();
                PLUGIN.CustomStatusFramework?.Call("DeleteStatus", "protectionstatus");
                PLUGIN.CustomStatusFramework?.Call("DeleteStatus", "protectionstatus.offline");
            }
        }

        public static class NotifyHelper
        {
            public static void ShowProtectionStatus(BasePlayer basePlayer, ProtectedCupboard tc)
            {
                if (basePlayer == null || !PLUGIN.IsOffIndicatorCooldown(basePlayer)) return;
                PLUGIN.NextTick(() =>
                {
                    if (basePlayer == null || tc == null) return;
                    if (tc.Status == ProtectionStatus.Protected)
                    {
                        PLUGIN.Notify?.Call("SendNotify", basePlayer, 0, PLUGIN.Lang("notify protected", basePlayer, tc.CurrentProtectionPercent));
                    }
                    else
                    {
                        PLUGIN.Notify?.Call("SendNotify", basePlayer, 0, PLUGIN.Lang("notify unprotected", basePlayer));
                    }
                });
            }
        }

        public static class ZoneManagerHelper
        {
            public static bool AllowDestruction(BaseCombatEntity entityAttacked, ProtectedCupboard tc)
            {
                if ((entityAttacked != null && tc != null) && (PLUGIN.ZoneManager?.IsLoaded ?? false))
                {
                    var zones = PLUGIN.ZoneManager.Call<string[]>("GetEntityZoneIDs", entityAttacked);
                    foreach(var zone in zones)
                    {
                        if (PLUGIN.ZoneManager.Call<bool>("HasFlag", zone, "undestr"))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        public static class ClansHelper
        {
            public static List<BasePlayer> GetClanMembers(BasePlayer basePlayer)
            {
                try
                {
                    if (basePlayer == null || !PLUGIN.config.Integration.Clans || (!PLUGIN.Clans?.IsLoaded ?? true)) { return new List<BasePlayer>(); }
                    var memberIds = PLUGIN.Clans?.Call<List<string>>("GetClanMembers", basePlayer.UserIDString);
                    if (memberIds == null) { return new List<BasePlayer>(); }
                    var playerList = new List<BasePlayer>();
                    foreach (var memberId in memberIds)
                    {
                        try
                        {
                            var found = BasePlayer.FindAwakeOrSleeping(memberId);
                            if (found == null) { continue; }
                            playerList.Add(found);
                        } catch (NullReferenceException)
                        {
                            continue;
                        }
                    }
                    return playerList;
                } catch (Exception)
                {
                    return new List<BasePlayer>();
                }
            }
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        public enum BalanceLedgerReason
        {
            Initial, Interval, DamageCost, Withdraw, Added, Other
        }
        public class BalanceLedgerEntry
        {
            public BalanceLedgerEntry(ProtectedCupboard tc, BalanceLedgerReason reason = BalanceLedgerReason.Other)
            {
                this.Timestamp = DateTime.Now;
                this.Balance = tc.Balance;
                this.Reason = reason;
            }
            public DateTime Timestamp;
            public float Balance;
            public BalanceLedgerReason Reason;
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        public class OwnerHistoryEntry
        {
            public OwnerHistoryEntry() {}
            public OwnerHistoryEntry(BasePlayer basePlayer)
            {
                UserID = basePlayer.userID;
                AuthTime = DateTime.Now;
            }
            public OwnerHistoryEntry(ulong userId)
            {
                UserID = userId;
                AuthTime = DateTime.Now;
            }
            public ulong UserID { get; set; }
            public DateTime AuthTime { get; set; }

            [JsonIgnore]
            private BasePlayer _basePlayer = null;

            [JsonIgnore]
            public BasePlayer BasePlayer
            {
                get
                {
                    if (_basePlayer == null)
                    {
                        _basePlayer = BasePlayer.FindAwakeOrSleeping(UserID.ToString());
                    }
                    return _basePlayer;
                }
            }
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        public static class RustColor
        {
            public static string Blue = "0.08627 0.25490 0.38431 1";
            public static string LightBlue = "0.25490 0.61176 0.86275 1";
            //public static string Red = "0.68627 0.21569 0.14118 1";
            public static string Red = "0.77255 0.23922 0.15686 1";
            public static string Maroon = "0.46667 0.22745 0.18431 1";
            public static string LightMaroon = "1.00000 0.32549 0.21961 1";
            public static string LightRed = "0.91373 0.77647 0.75686 1";
            //public static string Green = "0.25490 0.30980 0.14510 1";
            public static string Green = "0.35490 0.40980 0.24510 1";
            public static string LightGreen = "0.76078 0.94510 0.41176 1";
            public static string Gray = "0.45490 0.43529 0.40784 1";
            public static string LightGray = "0.69804 0.66667 0.63529 1";
            public static string Orange = "1.00000 0.53333 0.18039 1";
            public static string LightOrange = "1.00000 0.82353 0.44706 1";
            public static string White = "0.87451 0.83529 0.80000 1";
            public static string LightWhite = "0.97647 0.97647 0.97647 1";
            public static string Lime = "0.64706 1.00000 0.00000 1";
            public static string LightLime = "0.69804 0.83137 0.46667 1";
            public static string DarkGray = "0.08627 0.08627 0.08627 1";
            public static string DarkBrown = "0.15686 0.15686 0.12549 1";
            public static string LightBrown = "0.54509 0.51372 0.4705 1";
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        private static readonly string TRANSPARENT = "0 0 0 0";

        private static readonly string BOTTOM_LEFT = "0 0";

        private static readonly string BOTTOM_CENTER = "0.5 0";

        private static readonly string BOTTOM_RIGHT = "1 0";

        private static readonly string TOP_LEFT = "0 1";

        private static readonly string TOP_CENTER = "0.5 1";

        private static readonly string TOP_RIGHT = "1 1";

        private static readonly string MIDDLE_LEFT = "0 0.5";

        private static readonly string MIDDLE_CENTER = "0.5 0.5";

        private static readonly string MIDDLE_RIGHT = "1 0.5";

        public string Offset(float x, float y)
        {
            return $"{x} {y}";
        }
        public string Offset(int x, int y)
        {
            return $"{x} {y}";
        }

        public string Anchor(float x, float y)
        {
            return $"{x} {y}";
        }

        public string GetImage(string imgId)
        {
            return ImageLibrary?.Call<string>("GetImage", imgId);
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        [Command("tc.help")]
        private void CmdHelpShow(IPlayer player, string command, string[] args)
        {
            var basePlayer = player.Object as BasePlayer;
            if (basePlayer != null)
            {
                ShowLevels(basePlayer);
            }
        }

        [Command("tc.help.close")]
        private void CmdHelpClose(IPlayer player, string command, string[] args)
        {
            var basePlayer = player.Object as BasePlayer;
            if (basePlayer != null)
            {
                CloseHelp(basePlayer);
            }
        }

        private readonly static string HELP_ID = "rp.help";

        private void ShowHelp(BasePlayer basePlayer)
        {
            if (basePlayer == null) { return; }
            var styles = new InfoPanelStyles();
            var x = 150;
            var y = 50;
            var w = 700;
            var h = 500;
            var fontSize = 12;
            var container = new CuiElementContainer();
            container.Add(new CuiElement
            {
                Name = HELP_ID,
                Parent = "Overlay",
                Components =
                {
                    new CuiImageComponent
                    {
                        Color = styles.BackgroundColor
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = TOP_CENTER,
                        AnchorMax = TOP_CENTER,
                        OffsetMin = Offset(-w/2+x, -y-h),
                        OffsetMax = Offset(w/2+x, -y)
                    }
                }
            });
            var padded = ID(HELP_ID, "padded");
            container.Add(new CuiElement
            {
                Name = padded,
                Parent = HELP_ID,
                Components =
                {
                    new CuiImageComponent
                    {
                        Color = TRANSPARENT
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = Offset(styles.ContentPad, styles.ContentPad),
                        OffsetMax = Offset(-styles.ContentPad, -styles.ContentPad)
                    }
                }
            });
            /* Title */
            container.Add(new CuiElement
            {
                Parent = padded,
                Components =
                {
                    new CuiTextComponent
                    {
                        Text = Lang("command help title", basePlayer),
                        Align = UnityEngine.TextAnchor.UpperCenter,
                        Color = styles.TextColor,
                        FontSize = 16
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = TOP_LEFT,
                        AnchorMax = TOP_RIGHT,
                        OffsetMin = Offset(100, -styles.HeaderHeight),
                        OffsetMax = Offset(-100, 0)
                    }
                }
            });
            /* Close Button */
            var closebtn = ID(padded, "close");
            container.Add(new CuiElement
            {
                Parent = padded,
                Name = closebtn,
                Components =
                {
                    new CuiImageComponent
                    {
                        Png = ImageLibrary?.Call<string>("GetImage", "rp.cross"),
                        Color = styles.TextColor
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = TOP_RIGHT,
                        AnchorMax = TOP_RIGHT,
                        OffsetMin = Offset(-styles.HeaderHeight+styles.HeaderImgPad*2, -styles.HeaderHeight+styles.HeaderImgPad*2),
                        OffsetMax = Offset(0, 0)
                    }
                }
            });
            container.Add(new CuiElement
            {
                Parent = closebtn,
                Components =
                {
                    new CuiButtonComponent
                    {
                        Color = TRANSPARENT,
                        Command = "tc.help.close"
                    }
                }
            });
            var cmdh = 20;
            var gap = 220;
            var top = styles.HeaderHeight + styles.ContentPad;
            foreach(var command in Commands)
            {
                /* Command */
                container.Add(new CuiElement
                {
                    Parent = padded,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            Text = $"/{config.Commands.Admin} {command.Command} <color={ColorToHex(styles.SubtextColor)}>{command.ArgString}</color>",
                            Align = UnityEngine.TextAnchor.MiddleLeft,
                            Color = styles.TextColor,
                            FontSize = fontSize
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = TOP_LEFT,
                            AnchorMax = TOP_LEFT,
                            OffsetMin = Offset(0, -top-cmdh),
                            OffsetMax = Offset(gap, -top)
                        }
                    }
                });
                /* Description */
                container.Add(new CuiElement
                {
                    Parent = padded,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            Text = $"{command.Description}",
                            Align = UnityEngine.TextAnchor.MiddleLeft,
                            Color = styles.TextColor,
                            FontSize = fontSize
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = TOP_LEFT,
                            AnchorMax = TOP_RIGHT,
                            OffsetMin = Offset(gap, -top-cmdh),
                            OffsetMax = Offset(0, -top)
                        }
                    }
                });
                top += cmdh;
            }
            var hints = new string[]
            {
                "Use /tc id to get the <tc id> parameter value.",
                "A '?' represents an optional parameter.",
                "You can interact with this menu when the chat or inventory is open."
            };
            var bottom = 0;
            foreach(var hint in hints)
            {
                /* Hint */
                container.Add(new CuiElement
                {
                    Parent = padded,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            Text = $"HINT - {hint}",
                            Align = UnityEngine.TextAnchor.MiddleLeft,
                            Color = styles.SubtextColor,
                            FontSize = fontSize
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = BOTTOM_LEFT,
                            AnchorMax = BOTTOM_RIGHT,
                            OffsetMin = Offset(0, bottom),
                            OffsetMax = Offset(0, bottom + cmdh)
                        }
                    }
                });
                bottom += cmdh;
            }
            CloseHelp(basePlayer);
            CuiHelper.AddUi(basePlayer, container);
        }
        private void CloseHelp(BasePlayer basePlayer)
        {
            CuiHelper.DestroyUi(basePlayer, HELP_ID);
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        private static readonly float INDICATOR_LIFETIME_SECONDS = 2f;

        [Command("rp.indicator")]
        private void UIIndicatorShow(IPlayer player, string command, string[] args)
        {
            var result = Security.ValidateTokenArgs(args);
            if (!result.Success) { return; } else { args = result.Args; }
            var basePlayer = player.Object as BasePlayer;
            if (basePlayer != null)
            {
                var priv = basePlayer.GetBuildingPrivilege();
                if (priv != null)
                {
                    var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                    if (tc != null)
                    {
                        ShowIndicator(basePlayer, tc);
                    }
                }
            }
        }

        [Command("rp.indicator.close")]
        private void UIIndicatorClose(IPlayer player, string command, string[] args)
        {
            var basePlayer = player.Object as BasePlayer;
            if (basePlayer != null)
            {
                var priv = basePlayer.GetBuildingPrivilege();
                if (priv != null)
                {
                    var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
                    if (tc != null)
                    {
                        CloseIndicator(basePlayer);
                    }
                }
            }
        }

        private Dictionary<string, DateTime> IndicatorCooldowns = new Dictionary<string, DateTime>();
        public Dictionary<string, double> AccumulatedCost = new Dictionary<string, double>();

        private readonly static string INDICATOR_ID = "rp.indicator";

        private void SetIndicatorCooldown(BasePlayer basePlayer)
        {
            var until = DateTime.Now.AddSeconds(INDICATOR_LIFETIME_SECONDS);
            IndicatorCooldowns[basePlayer.UserIDString] = until;
        }

        private double UpdateIndicatorCost(BasePlayer basePlayer, double amount)
        {
            if (!AccumulatedCost.ContainsKey(basePlayer.UserIDString))
            {
                AccumulatedCost[basePlayer.UserIDString] = 0;
            }
            AccumulatedCost[basePlayer.UserIDString] += amount;
            return AccumulatedCost[basePlayer.UserIDString];
        }

        private bool IsOffIndicatorCooldown(BasePlayer basePlayer)
        {
            if (!IndicatorCooldowns.ContainsKey(basePlayer.UserIDString))
            {
                return true;
            }
            var now = DateTime.Now.Ticks;
            var target = IndicatorCooldowns[basePlayer.UserIDString].Ticks;
            if (now >= target)
            {
                AccumulatedCost.Remove(basePlayer.UserIDString);
                return true;
            }
            return false;
        }

        private void ShowIndicatorCost(BasePlayer basePlayer, ProtectedCupboard tc, double amount)
        {
            if (tc.IsProtected && config.Indicator.Enabled && config.Indicator.ShowBalanceDeducted && tc.HasCostPerDamageProtected && amount > 0)
            {
                var container = new CuiElementContainer();
                var offset = 4;
                var panelw = 70;
                var panelh = 20;
                var costpanel = ID(INDICATOR_ID, "cost");
                var bgcolor = RustColor.Red;
                container.Add(new CuiElement
                {
                    Name = costpanel,
                    Parent = INDICATOR_ID,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            Color = bgcolor
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = $"{0.5} {0}",
                            AnchorMax = $"{0.5} {0}",
                            OffsetMin = $"{-panelw/2} {-offset-panelh}",
                            OffsetMax = $"{panelw/2} {-offset}"
                        }
                    }
                });
                var txtcolor = RustColor.LightOrange;
                var deductedRounded = String.Format("{0:0.00}", amount);
                container.Add(new CuiElement
                {
                    Parent = costpanel,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            Text = Lang("indicator balance reduced amount", basePlayer, deductedRounded),
                            Color = txtcolor,
                            FontSize = 8,
                            Align = UnityEngine.TextAnchor.MiddleCenter
                        }
                    }
                });
                CuiHelper.DestroyUi(basePlayer, costpanel);
                CuiHelper.AddUi(basePlayer, container);
            }

        }

        private void ShowIndicator(BasePlayer basePlayer, ProtectedCupboard tc, bool ignoreCooldown = false, double amountDeducted = 0)
        {
            if (IsNull(basePlayer)) { return; }
            if (IsNull(tc))
            {
                CloseIndicator(basePlayer);
                return;
            }
            // Show Indicator if off cooldown
            if (config.Indicator.Enabled && (IsOffIndicatorCooldown(basePlayer) || ignoreCooldown))
            {
                var fontSize = config.Indicator.FontSize;
                var container = new CuiElementContainer();
                container.Add(new CuiElement
                {
                    Name = INDICATOR_ID,
                    Parent = "Hud",
                    Components =
                    {
                        new CuiImageComponent
                        {
                            Color = TRANSPARENT
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = config.Indicator.AnchorMin,
                            AnchorMax = config.Indicator.AnchorMax,
                            OffsetMin = config.Indicator.OffsetMin,
                            OffsetMax = config.Indicator.OffsetMax
                        }
                    }
                });
                var img = $"{INDICATOR_ID}.img";
                var green = RustColor.LightGreen;
                var red = RustColor.Red;
                var active = tc.IsProtected;
                container.Add(new CuiElement
                {
                    Name = img,
                    Parent = INDICATOR_ID,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            Png = active ? ImageLibrary?.Call<string>("GetImage", "status.protected") : ImageLibrary?.Call<string>("GetImage", "status.unprotected"),
                            Color = active ? green : red
                        }
                    }
                });
                if (active)
                {
                    container.Add(new CuiElement
                    {
                        Parent = img,
                        Components =
                    {
                        new CuiTextComponent
                        {
                            Text = $"{tc.CurrentProtectionPercent}%",
                            FontSize = fontSize,
                            Align = UnityEngine.TextAnchor.MiddleCenter,
                            Color = RustColor.Green
                        }
                    }
                    });
                }
                CloseIndicator(basePlayer);
                CuiHelper.AddUi(basePlayer, container);
                CloseIndicatorIfOffCooldown(basePlayer);
            }
            else if (!config.Indicator.Enabled && config.Integration.CustomStatusFramework && (IsOffIndicatorCooldown(basePlayer) || ignoreCooldown))
            {
                CloseIndicatorIfOffCooldown(basePlayer);
            }
            // Update damage balance if enabled
            if (tc.IsProtected && (config.Indicator.Enabled || config.Integration.CustomStatusFramework) && config.Indicator.ShowBalanceDeducted && amountDeducted > 0)
            { 
                var amount = UpdateIndicatorCost(basePlayer, amountDeducted);
                ShowIndicatorCost(basePlayer, tc, amount);
            }
        }

        private void SetIndicatorCooldownOrClose(BasePlayer basePlayer, ProtectedCupboard tc)
        {
            if (basePlayer == null || tc == null) { return; }
            if (tc.IsProtected)
            {
                SetIndicatorCooldown(basePlayer);
            }
            else
            {
                timer.In(INDICATOR_LIFETIME_SECONDS * 2, () =>
                {
                    CloseIndicator(basePlayer);
                });
            }
        }

        private void CloseIndicatorIfOffCooldown(BasePlayer basePlayer, int depth = 0)
        {
            timer.In(INDICATOR_LIFETIME_SECONDS+0.1f, () =>
            {
                if (basePlayer == null || !IndicatorCooldowns.ContainsKey(basePlayer.UserIDString)) { return; }
                if (depth >= 10)
                {
                    CloseIndicator(basePlayer);
                    IndicatorCooldowns.Remove(basePlayer.UserIDString);
                }
                if (IsOffIndicatorCooldown(basePlayer))
                {
                    CloseIndicator(basePlayer);
                    IndicatorCooldowns.Remove(basePlayer.UserIDString);
                }
                else
                {
                    var newDepth = depth += 1;
                    CloseIndicatorIfOffCooldown(basePlayer, newDepth);
                }
            });
        }

        private void CloseIndicator(BasePlayer basePlayer)
        {
            CustomStatusFrameworkHelper.ClearProtectionStatus(basePlayer);
            CuiHelper.DestroyUi(basePlayer, INDICATOR_ID);
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        private readonly string INFO_PANEL_ID = "rp.infopanel";
        private readonly string INFO_PANEL_CONTENT_ID = "rp.infopanel.content";
        private readonly string INFO_PANEL_SHADOW_ID = "rp.infopanel.shadow";

        private void TcInfoShowPlayer(IPlayer player, string command, string[] args)
        {
            var basePlayer = player.Object as BasePlayer;
            if (IsNull(basePlayer)) return;
            var priv = basePlayer.GetBuildingPrivilege();
            if (IsNull(priv)) return;
            var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
            if (IsNull(tc)) return;
            if (priv.IsAuthed(basePlayer) || IsAdmin(basePlayer))
            {
                ShowInfoPanel(basePlayer, tc);
            }
            else
            {
                Message(basePlayer, "info need authorization");
            }
        }

        [Command("tc.info.open")]
        private void TcInfoOpen(IPlayer player, string command, string[] args)
        {
            var result = Security.ValidateTokenArgs(args);
            if (!result.Success) { return; } else { args = result.Args; }
            var basePlayer = player.Object as BasePlayer;
            if (IsNull(basePlayer)) return;
            uint privid = 0;
            uint.TryParse(args[0], out privid);
            var tc = ProtectedCupboardManager.GetByID(privid);
            if (IsNull(tc)) return;
            var page = 0;
            int.TryParse(args[1], out page);
            ShowInfoPanel(basePlayer, tc, page);
        }

        [Command("tc.info.show")]
        private void TcInfoShow(IPlayer player, string command, string[] args)
        {
            var result = Security.ValidateTokenArgs(args);
            if (!result.Success) { return; } else { args = result.Args; }
            var basePlayer = player.Object as BasePlayer;
            if (IsNull(basePlayer)) return;
            var priv = basePlayer.GetBuildingPrivilege();
            if (IsNull(priv)) return;
            var tc = ProtectedCupboardManager.InitProtectedCupboard(priv);
            if (IsNull(tc)) return;
            ShowInfoPanel(basePlayer, tc, 0);
        }

        [Command("tc.info.close")]
        private void TcInfoClose(IPlayer player, string command, string[] args)
        {
            var basePlayer = player.Object as BasePlayer;
            if (IsNull(basePlayer)) return;
            CloseInfoPanel(basePlayer);
        }

        private class InfoPanelStyles
        {
            public string ShadowColor = "0 0 0 0.9";
            public string BackgroundColor = RustColor.DarkGray;
            public string HeaderColor = RustColor.DarkBrown ;
            public string BoxColor = RustColor.DarkBrown;
            public string TextColor = "0.54509 0.51372 0.4705 1";
            public string SubtextColor = "0.54509 0.51372 0.5705 1";
            public float PanelSize = 0.5f;
            public int HeaderHeight = 30;
            public int HeaderImgPad = 8;
            public int HeaderImgGap = 2;
            public int ContentPad = 8;
            public string HeaderImgColor = "1 1 1 1";
            public float OVTitleH = 0.2f;
            public float OVTitleW = 0.6f;
            public float OVPanelP = 0.1f;
            public float OVPanelG = 0.05f;
            public int OVPanelContentP = 20;
            public int OVPanelImgS = 80;
            public float OVPanelFadeIn = 0.5f;
            public string TabSelectedColor = "0.54509 0.51372 0.4705 1";
            public string TabUnselectedColor = "0.54509 0.51372 0.4705 0.5";
            public float UPanelTitleW = 0.3f;
            public float UPanelTitleH = 0.1f;
        }

        private void ShowInfoPanel(BasePlayer basePlayer, ProtectedCupboard tc, int page = 0)
        {
            var container = new CuiElementContainer();

            #region Styles
            var styles = new InfoPanelStyles();
            #endregion

            /* Shadow */
            container.Add(new CuiElement
            {
                Parent = "Overlay",
                Name = INFO_PANEL_SHADOW_ID,
                Components =
                {
                    new CuiButtonComponent
                    {
                        Color = styles.ShadowColor,
                        Material = "assets/content/ui/uibackgroundblur-ingamemenu.mat",
                        Command = "tc.info.close"
                    }
                }
            });

            /* Panel */
            container.Add(new CuiElement
            {
                Parent = "Overlay",
                Name = INFO_PANEL_ID,
                Components =
                {
                    new CuiNeedsCursorComponent(),
                    new CuiImageComponent
                    {
                        Color = styles.BackgroundColor
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0.5-(styles.PanelSize/2)} {0.5-(styles.PanelSize/2)}",
                        AnchorMax = $"{0.5+(styles.PanelSize/2)} {0.5+(styles.PanelSize/2)}"
                    }
                }
            });

            /* Header */
            var headerId = $"{INFO_PANEL_ID}.header";
            container.Add(new CuiElement
            {
                Parent = INFO_PANEL_ID,
                Name = headerId,
                Components =
                {
                    new CuiImageComponent
                    {
                        Color = styles.HeaderColor
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = TOP_LEFT,
                        AnchorMax = TOP_RIGHT,
                        OffsetMin = $"{0} {-styles.HeaderHeight}",
                        OffsetMax = $"{0} {0}"
                    }
                }
            });
            container.Add(new CuiElement
            {
                Parent = headerId,
                Components =
                {
                    new CuiTextComponent
                    {
                        Text = Lang("title", basePlayer).TitleCase(),
                        Color = styles.TextColor,
                        Align = UnityEngine.TextAnchor.MiddleCenter
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0.3} {0}",
                        AnchorMax = $"{0.7} {1}"
                    }
                }
            });

            /* Header Tabs */
            #region Header Tabs
            var tabs = new[]
            {
                new 
                {
                    Icon = "status.info",
                    Command = $"tc.info.open {Security.Token} {tc.EntityID} 0",
                    Selected = page == 0
                },
                new
                {
                    Icon = "rp.owners",
                    Command = $"tc.info.open {Security.Token} {tc.EntityID} 1",
                    Selected = page == 1
                },
                new
                {
                    Icon = "status.protected",
                    Command = $"tc.info.open {Security.Token} {tc.EntityID} 2",
                    Selected = page == 2
                },
                new
                {
                    Icon = "rp.costs",
                    Command = $"tc.info.open {Security.Token} {tc.EntityID} 3",
                    Selected = page == 3
                }
            };

            var s = 0;
            foreach(var tab in tabs)
            {
                var tid = ID(headerId, "btn", true);
                container.Add(new CuiElement
                {
                    Parent = headerId,
                    Name = tid,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            Png = ImageLibrary?.Call<string>("GetImage", tab.Icon),
                            Color = tab.Selected ? styles.TabSelectedColor : styles.TabUnselectedColor
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = BOTTOM_LEFT,
                            AnchorMax = TOP_LEFT,
                            OffsetMin = $"{s+styles.HeaderImgPad} {styles.HeaderImgPad}",
                            OffsetMax = $"{s-styles.HeaderImgPad+styles.HeaderHeight} {-styles.HeaderImgPad}"
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = tid,
                    Components =
                    {
                        new CuiButtonComponent
                        {
                            Color = TRANSPARENT,
                            Command = tab.Selected ? string.Empty : tab.Command
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = BOTTOM_LEFT,
                            AnchorMax = TOP_RIGHT
                        }
                    }
                });
                s += styles.HeaderHeight + styles.HeaderImgGap;
            }
            #endregion

            #region Option Tabs
            var options = new[]
            {
                new
                {
                    Icon = "rp.cross",
                    Command = "tc.info.close"
                }
            };
            s = 0;
            foreach (var option in options)
            {
                var tid = ID(headerId, "btn", true);
                container.Add(new CuiElement
                {
                    Parent = headerId,
                    Name = tid,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            Png = ImageLibrary?.Call<string>("GetImage", option.Icon),
                            Color = styles.TextColor
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = BOTTOM_RIGHT,
                            AnchorMax = TOP_RIGHT,
                            OffsetMin = $"{-s-styles.HeaderHeight+styles.HeaderImgPad} {styles.HeaderImgPad}",
                            OffsetMax = $"{-s-styles.HeaderImgPad} {-styles.HeaderImgPad}"
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = tid,
                    Components =
                    {
                        new CuiButtonComponent
                        {
                            Color = TRANSPARENT,
                            Command = option.Command
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = BOTTOM_LEFT,
                            AnchorMax = TOP_RIGHT
                        }
                    }
                });
                s += styles.HeaderHeight + styles.HeaderImgGap;
            }
            #endregion

            /* Panel Content */
            container.Add(new CuiElement
            {
                Parent = INFO_PANEL_ID,
                Name = INFO_PANEL_CONTENT_ID,
                Components =
                {
                    new CuiImageComponent
                    {
                        Color = TRANSPARENT
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = BOTTOM_LEFT,
                        AnchorMax = TOP_RIGHT,
                        OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                        OffsetMax = $"{-styles.ContentPad} {-styles.HeaderHeight-styles.ContentPad}"
                    }
                }
            });
            if (page == 0)
            {
                container = CreateOverviewPage(container, styles, basePlayer, tc);
            }
            if (page == 1)
            {
                container = CreateOwnersPage(container, styles, basePlayer, tc);
            }
            if (page == 2)
            {
                container = CreateProtectionPage(container, styles, basePlayer, tc);
            }
            if (page == 3)
            {
                container = CreateCostsPage(container, styles, basePlayer, tc);
            }
            CloseInfoPanel(basePlayer);
            CuiHelper.AddUi(basePlayer, container);
        }

        private CuiElementContainer CreateOverviewPage(CuiElementContainer container, InfoPanelStyles styles, BasePlayer basePlayer, ProtectedCupboard tc)
        {
            /* Status Text */
            var pid = ID(INFO_PANEL_CONTENT_ID, "title");
            var isProtected = tc.Status == ProtectionStatus.Protected;
            var ptextcolor = isProtected ? RustColor.LightGreen : RustColor.LightRed;
            var pbgcolor = isProtected ? RustColor.Green : RustColor.Red;
            container.Add(new CuiElement
            {
                Parent = INFO_PANEL_CONTENT_ID,
                Name = pid,
                Components =
                {
                    new CuiImageComponent
                    {
                        Color = pbgcolor
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0.5f-styles.OVTitleW/2} {1-styles.OVTitleH}",
                        AnchorMax = $"{0.5f+styles.OVTitleW/2} {1}"
                    }
                }
            });
            container.Add(new CuiElement
            {
                Parent = pid,
                Components =
                {
                    new CuiTextComponent
                    {
                        Text = ProtectionStatusToString(tc, basePlayer, tc.Status),
                        Color = ptextcolor,
                        Align = UnityEngine.TextAnchor.UpperCenter,
                        FontSize = 18
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = Offset(0, styles.ContentPad),
                        OffsetMax = Offset(0, -styles.ContentPad)
                    }
                }
            });
            var rcolor = isProtected ? RustColor.LightGreen : RustColor.LightOrange;
            var reason = isProtected ? Lang("ui protected", basePlayer, tc.CurrentProtectionPercent, FormatTimeShort(tc.HoursOfProtection)) : StatusReasonToString(tc, basePlayer, tc.Reason);
            container.Add(new CuiElement
            {
                Parent = pid,
                Components =
                {
                    new CuiTextComponent
                    {
                        Text = reason,
                        Color = rcolor,
                        Align = UnityEngine.TextAnchor.LowerCenter
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = Offset(0, styles.ContentPad),
                        OffsetMax = Offset(0, -styles.ContentPad)
                    }
                }
            });
            var panels = new[]
            {
                new
                {
                    Text = Lang("info owners count", basePlayer, tc.Owners.Count),
                    Icon = "rp.owners",
                    Command = $"tc.info.open {Security.Token} {tc.EntityID} 1",
                    Color = "0 0 0 1"
                },
                new
                {
                    Text = Lang("info protection", basePlayer, tc.CurrentProtectionPercent),
                    Icon = isProtected ? "status.protected" : "status.unprotected",
                    Command = $"tc.info.open {Security.Token} {tc.EntityID} 2",
                    Color = "0 1 0 1"
                },
                new
                {
                    Text = Lang("info balance", basePlayer, FormatCurrency((float)Math.Round(tc.Balance, 1))),
                    Icon = "rp.costs",
                    Command = $"tc.info.open {Security.Token} {tc.EntityID} 3",
                    Color = "0 0 1 1"
                }
            };
            #region Panels
            var panelsId = ID(INFO_PANEL_CONTENT_ID, "panels");
            container.Add(new CuiElement
            {
                Parent = INFO_PANEL_CONTENT_ID,
                Name = panelsId,
                Components =
                {
                    new CuiImageComponent
                    {
                        Color = TRANSPARENT
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{styles.OVPanelP} {styles.OVPanelP}",
                        AnchorMax = $"{1f-styles.OVPanelP} {1f-styles.OVTitleH-styles.OVPanelP}",
                        OffsetMin = $"{0} {styles.ContentPad*2}",
                        OffsetMax = $"{0} {0}"
                    }
                }
            });
            var panelW = (1f - ((panels.Length-1)*styles.OVPanelG)) / panels.Length;
            var s = 0f;
            foreach(var panel in panels)
            {
                var ppid = ID(panelsId, "panel", true);
                container.Add(new CuiElement
                {
                    Parent = panelsId,
                    Name = ppid,
                    Components =
                    {
                        new CuiButtonComponent
                        {
                            Command = panel.Command,
                            FadeIn = styles.OVPanelFadeIn,
                            Color = styles.BoxColor
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = $"{s} {0}",
                            AnchorMax = $"{s+panelW} {1}"
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = ppid,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = styles.TextColor,
                            Png = ImageLibrary?.Call<string>("GetImage", panel.Icon),
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = TOP_CENTER,
                            AnchorMax = TOP_CENTER,
                            OffsetMin = $"{-styles.OVPanelImgS/2} {-styles.OVPanelContentP-styles.OVPanelImgS}",
                            OffsetMax = $"{styles.OVPanelImgS/2} {-styles.OVPanelContentP}"
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = ppid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Text = panel.Text,
                            Align = UnityEngine.TextAnchor.MiddleCenter,
                            Color = styles.TextColor
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = BOTTOM_LEFT,
                            AnchorMax = TOP_RIGHT,
                            OffsetMin = $"{styles.OVPanelContentP} {styles.OVPanelContentP}",
                            OffsetMax = $"{-styles.OVPanelContentP} {-styles.OVPanelContentP-styles.OVPanelImgS}"
                        }
                    }
                });
                s += panelW + styles.OVPanelG;
            }
            #endregion
            return container;
        }

        private CuiElementContainer CreateOwnersPage(CuiElementContainer container, InfoPanelStyles styles, BasePlayer basePlayer, ProtectedCupboard tc)
        {
            /* Bottom Bar */
            var bbarid = ID(INFO_PANEL_CONTENT_ID, "bar");
            container.Add(new CuiElement
            {
                Parent = INFO_PANEL_CONTENT_ID,
                Name = bbarid,
                Components =
                    {
                        new CuiImageComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = RustColor.Red
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = $"{0.1} {0}",
                            AnchorMax = $"{0.9} {styles.UPanelTitleH}"
                        }
                    }
            });
            container.Add(new CuiElement
            {
                Parent = bbarid,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Text = Lang("info owners persistent", basePlayer),
                        FontSize = 12,
                        Color = RustColor.LightRed,
                        Align = UnityEngine.TextAnchor.MiddleCenter
                    }
                }
            });
            var panels = new[]
            {
                new
                {
                    Title = Lang("info owners", basePlayer),
                    Values = tc.Owners
                    .Where(x => x != null)
                    .OrderBy(x => tc.FounderUserId == x.userID)
                    .OrderBy(x => x.IsConnected)
                    .Select(x => new {
                        Text = x.displayName,
                        Founder = tc.FounderUserId == x.userID
                    }).ToList()
                },
                new
                {
                    Title = Lang("info online", basePlayer),
                    Values = tc.OnlineOwners
                    .Where(x => x != null)
                    .OrderBy(x => tc.FounderUserId == x.userID)
                    .Select(x => new {
                        Text = x.displayName,
                        Founder = tc.FounderUserId == x.userID
                    }).ToList()
                },
                new
                {
                    Title = Lang("info authorized", basePlayer),
                    Values = tc.AuthedPlayers
                    .Where(x => x != null)
                    .OrderBy(x => tc.FounderUserId == x.userID)
                    .OrderBy(x => x.IsConnected)
                    .Select(x => new {
                        Text = x.displayName,
                        Founder = tc.FounderUserId == x.userID
                    }).ToList()
                }
            };
            var panelW = 1f / panels.Length;
            var s = 0f;
            var idx = 0;
            foreach(var panel in panels)
            {
                bool isLast = idx == panels.Length - 1;
                var p = isLast ? 2 * styles.ContentPad : styles.ContentPad;
                /* Owner Title */
                var pid = ID(INFO_PANEL_CONTENT_ID, "title", true);
                container.Add(new CuiElement
                {
                    Parent = INFO_PANEL_CONTENT_ID,
                    Name = pid,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = styles.BoxColor
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = $"{s} {1-styles.UPanelTitleH}",
                            AnchorMax = $"{s+panelW} {1}",
                            OffsetMin = $"{0} {0}",
                            OffsetMax = $"{-p} {0}"
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = pid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Text = panel.Title,
                            Color = styles.TextColor,
                            Align = UnityEngine.TextAnchor.MiddleCenter
                        }
                    }
                });
                /* Owner Box */
                pid = ID(pid, "owners", true);
                container.Add(new CuiElement
                {
                    Parent = INFO_PANEL_CONTENT_ID,
                    Name = pid,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = styles.BoxColor
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = $"{s} {styles.UPanelTitleH}",
                            AnchorMax = $"{s+panelW} {1-styles.UPanelTitleH}",
                            OffsetMin = $"{0} {styles.ContentPad}",
                            OffsetMax = $"{-p} {-styles.ContentPad}"
                        }
                    }
                });
                p = styles.ContentPad;
                s += panelW;
                idx++;
                /* Owner Entries */
                var t = 0f;
                var h = 0.1f;
                var values = panel.Values;
                var moreThan10 = values.Count > 10;
                var j = 0;
                foreach(var owner in values)
                {
                    var lastEntry = j == values.Count - 1;
                    var entryId = ID(pid, "entry", true);
                    var ownerText = (moreThan10 && j >= 9) ? Lang("info others", basePlayer, values.Count - 9) : $"{owner.Text}";
                    if (owner.Founder)
                    {
                        ownerText += $" {Lang("info founder", basePlayer)}";
                    }
                    container.Add(new CuiElement
                    {
                        Parent = pid,
                        Name = entryId,
                        Components =
                        {
                            new CuiTextComponent
                            {
                                FadeIn = styles.OVPanelFadeIn,
                                Text =  ownerText,
                                Color = styles.SubtextColor,
                                Align = UnityEngine.TextAnchor.MiddleLeft
                            },
                            new CuiRectTransformComponent
                            {
                                AnchorMin = $"{0} {1-t-h}",
                                AnchorMax = $"{1} {1-t}",
                                OffsetMin = $"{styles.ContentPad} {0}",
                                OffsetMax = $"{-styles.ContentPad} {0}"
                            }
                        }
                    });
                    if (moreThan10 && j >= 9)
                    {
                        break;
                    }
                    t += h;
                    j++;
                }
            }
            return container;
        }

        private CuiElementContainer CreateProtectionPage(CuiElementContainer container, InfoPanelStyles styles, BasePlayer basePlayer, ProtectedCupboard tc)
        {
            #region Left
            var leftId = ID(INFO_PANEL_CONTENT_ID, "left");
            container.Add(new CuiElement
            {
                Parent = INFO_PANEL_CONTENT_ID,
                Name = leftId,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Color = TRANSPARENT
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0} {0}",
                        AnchorMax = $"{0.5} {1}",
                        OffsetMin = $"{0} {0}",
                        OffsetMax = $"{-styles.ContentPad} {0}"
                    }
                }
            });
            /* Title */
            var titleLId = ID(leftId, "title");
            container.Add(new CuiElement
            {
                Parent = leftId,
                Name = titleLId,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Color = styles.BoxColor
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0} {1-styles.UPanelTitleH}",
                        AnchorMax = $"{1} {1}"
                    }
                }
            });
            container.Add(new CuiElement
            {
                Parent = titleLId,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Text = Lang("info protection rank", basePlayer, tc.HighestProtectionLevel.Rank),
                        Align = UnityEngine.TextAnchor.MiddleCenter,
                        Color = styles.TextColor
                    }
                }
            });
            var boxes = new[]
            {
                new
                {
                    Title = "",
                    Text = "",
                    Note = "",
                    FontSize = 24,
                    UseSubbox = true,
                    Subboxes = new []
                    {
                        new
                        {
                            Title = Lang("info protection current", basePlayer),
                            Text = $"{tc.CurrentProtectionPercent}%"
                        },
                        new
                        {
                            Title = Lang("info protection online", basePlayer),
                            Text = $"{tc.OnlineProtectionPercent}%"
                        },
                        new
                        {
                            Title = Lang("info protection offline", basePlayer),
                            Text = $"{tc.OfflineProtectionPercent}%"
                        }
                    }
                },
                new
                {
                    Title = "",
                    Text = "",
                    Note = "",
                    FontSize = 18,
                    UseSubbox = true,
                    Subboxes = new []
                    {
                        new
                        {
                            Title = Lang("info max protection time", basePlayer),
                            Text = (!tc.HasProtectionTimeLimit || tc.TotalProtectionCostPerHour <= 0) ? Lang("info no limit", basePlayer) : Lang("info hours", basePlayer, tc.MaxProtectionTimeHours),
                        },
                        new
                        {
                            Title = Lang("info cost when protected", basePlayer),
                            Text = $"{FormatCurrency(tc.CostPerDamageProtected)}"
                        }
                    }
                },
                new
                {
                    Title = Lang("info protection time delay", basePlayer),
                    Text = Lang("info delay seconds", basePlayer, tc.HighestProtectionLevel.OfflineProtectionDelay),
                    Note = Lang("info starts when owners offline", basePlayer),
                    FontSize = 18,
                    UseSubbox = false,
                    Subboxes = new[] { new { Title = "", Text = "" } }
                }
            };
            var t = 1f-styles.UPanelTitleH;
            var boxH = 0.3f;
            foreach(var box in boxes)
            {
                var boxId = ID(leftId, "box", true);
                container.Add(new CuiElement
                {
                    Parent = leftId,
                    Name = boxId,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = styles.BoxColor
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = $"{0} {t-boxH}",
                            AnchorMax = $"{1} {t}",
                            OffsetMin = $"{styles.ContentPad} {0}",
                            OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                        }
                    }
                });
                if (box.UseSubbox)
                {
                    var sbs = 0f;
                    var sbw = 1f / (box.Subboxes.Length);
                    foreach(var sb in box.Subboxes)
                    {
                        var sbid = ID(boxId, "sb", true);
                        container.Add(new CuiElement
                        {
                            Parent = boxId,
                            Name = sbid,
                            Components =
                            {
                                new CuiImageComponent
                                {
                                    FadeIn = styles.OVPanelFadeIn,
                                    Color = TRANSPARENT
                                },
                                new CuiRectTransformComponent
                                {
                                    AnchorMin = $"{sbs} {0}",
                                    AnchorMax = $"{sbs+sbw} {1}",
                                    OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                                    OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                                }
                            }
                        });
                        container.Add(new CuiElement
                        {
                            Parent = sbid,
                            Components =
                            {
                                new CuiTextComponent
                                {
                                    FadeIn = styles.OVPanelFadeIn,
                                    Text = sb.Title,
                                    Color = styles.TextColor,
                                    Align = UnityEngine.TextAnchor.UpperCenter
                                }
                            }
                        });
                        container.Add(new CuiElement
                        {
                            Parent = sbid,
                            Components =
                            {
                                new CuiTextComponent
                                {
                                    FadeIn = styles.OVPanelFadeIn,
                                    Text = sb.Text,
                                    FontSize = box.FontSize,
                                    Color = styles.TextColor,
                                    Align = UnityEngine.TextAnchor.MiddleCenter
                                },
                                new CuiRectTransformComponent
                                {
                                    AnchorMin = $"{0} {0}",
                                    AnchorMax = $"{1} {0.8}",
                                }
                            }
                        });
                        sbs += sbw;
                    }
                }
                else
                {
                    container.Add(new CuiElement
                    {
                        Parent = boxId,
                        Components =
                        {
                            new CuiTextComponent
                            {
                                FadeIn = styles.OVPanelFadeIn,
                                Text = box.Title,
                                Color = styles.TextColor,
                                Align = UnityEngine.TextAnchor.UpperCenter
                            },
                            new CuiRectTransformComponent
                            {
                                AnchorMin = "0 0",
                                AnchorMax = "1 1",
                                OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                                OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                            }
                        }
                    });
                    container.Add(new CuiElement
                    {
                        Parent = boxId,
                        Components =
                        {
                            new CuiTextComponent
                            {
                                FadeIn = styles.OVPanelFadeIn,
                                Text = box.Text,
                                FontSize = box.FontSize,
                                Color = styles.TextColor,
                                Align = UnityEngine.TextAnchor.MiddleCenter
                            },
                            new CuiRectTransformComponent
                            {
                                AnchorMin = $"{0} {0}",
                                AnchorMax = $"{1} {0.8}",
                                OffsetMin = $"{0} {(box.Note != "" ? styles.ContentPad : 0)}",
                                OffsetMax = $"{0} {0}"
                            }
                        }
                    });
                    container.Add(new CuiElement
                    {
                        Parent = boxId,
                        Components =
                        {
                            new CuiTextComponent
                            {
                                FadeIn = styles.OVPanelFadeIn,
                                Text = box.Note,
                                FontSize = 10,
                                Color = styles.SubtextColor,
                                Align = UnityEngine.TextAnchor.LowerCenter
                            },
                            new CuiRectTransformComponent
                            {
                                AnchorMin = $"{0} {0}",
                                AnchorMax = $"{1} {0.8}",
                                OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                                OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                            }
                        }
                    });
                }
                t -= boxH;
            }
            #endregion
            #region Right
            var rightId = ID(INFO_PANEL_CONTENT_ID, "right");
            container.Add(new CuiElement
            {
                Parent = INFO_PANEL_CONTENT_ID,
                Name = rightId,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Color = TRANSPARENT
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0.5} {0}",
                        AnchorMax = $"{1} {1}",
                        OffsetMin = $"{styles.ContentPad} {0}",
                        OffsetMax = $"{0} {0}"
                    }
                }
            });
            var titleRId = ID(rightId, "title");
            container.Add(new CuiElement
            {
                Parent = rightId,
                Name = titleRId,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Color = styles.BoxColor
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0} {1-styles.UPanelTitleH}",
                        AnchorMax = $"{1} {1}"
                    }
                }
            });
            container.Add(new CuiElement
            {
                Parent = titleRId,
                Components =
                {
                    new CuiTextComponent
                    {
                        Text = Lang("info protected entities", basePlayer),
                        FadeIn = styles.OVPanelFadeIn,
                        Align = UnityEngine.TextAnchor.MiddleCenter,
                        Color = styles.TextColor
                    }
                }
            });
            var protectedEntities = new[]
            {
                new
                {
                    Text = Lang("info protected animals", basePlayer),
                    IsProtected = config.Protection.ProtectedEntities.Animals
                },
                new
                {
                    Text = Lang("info protected buildings", basePlayer),
                    IsProtected = config.Protection.ProtectedEntities.Buildings
                },
                new
                {
                    Text = Lang("info protected deployables", basePlayer),
                    IsProtected = config.Protection.ProtectedEntities.Containers
                },
                new
                {
                    Text = Lang("info protected electrical", basePlayer),
                    IsProtected = config.Protection.ProtectedEntities.Electrical
                },
                new
                {
                    Text = Lang("info protected horses", basePlayer),
                    IsProtected = config.Protection.ProtectedEntities.Horses
                },
                new
                {
                    Text = Lang("info protected loot nodes", basePlayer),
                    IsProtected = config.Protection.ProtectedEntities.LootNodes
                },
                new
                {
                    Text = Lang("info protected npcs", basePlayer),
                    IsProtected = config.Protection.ProtectedEntities.NPCs
                },
                new
                {
                    Text = Lang("info protected authed players", basePlayer),
                    IsProtected = config.Protection.ProtectedEntities.AuthedPlayers
                },
                new
                {
                    Text = Lang("info protected unauthed players", basePlayer),
                    IsProtected = config.Protection.ProtectedEntities.UnauthedPlayers
                },
                new
                {
                    Text = Lang("info protected vehicles", basePlayer),
                    IsProtected = config.Protection.ProtectedEntities.Vehicles
                },
            };
            var protectedFrom = new[]
{
                new
                {
                    Text = Lang("info protected authed players", basePlayer),
                    IsProtected = config.Protection.ProtectedFrom.AuthorizedPlayers
                },
                new
                {
                    Text = Lang("info protected unauthed players", basePlayer),
                    IsProtected = config.Protection.ProtectedFrom.UnauthorizedPlayers
                },
                new
                {
                    Text = Lang("info protected attack heli", basePlayer),
                    IsProtected = config.Protection.ProtectedFrom.AttackHeli
                }
            };
            var columns = new[]
            {
                new
                {
                    Title = Lang("info protected", basePlayer),
                    Data = protectedEntities
                },
                new
                {
                    Title = Lang("info from", basePlayer),
                    Data = protectedFrom
                }
            };
            var left = 0f;
            var colw = 1f / columns.Length;
            var subtitleH = 20;
            foreach(var col in columns)
            {
                var ttlId = ID(rightId, "title", true);
                var entId = ID(rightId, "entities", true);
                container.Add(new CuiElement
                {
                    Parent = rightId,
                    Name = ttlId,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = styles.BoxColor
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = $"{left} {1-styles.UPanelTitleH}",
                            AnchorMax = $"{left+colw} {1-styles.UPanelTitleH}",
                            OffsetMin = $"{styles.ContentPad} {-styles.ContentPad-subtitleH}",
                            OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = ttlId,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = styles.TextColor,
                            Align = UnityEngine.TextAnchor.MiddleCenter,
                            Text = col.Title
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = rightId,
                    Name = entId,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = styles.BoxColor
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = $"{left} {0}",
                            AnchorMax = $"{left+colw} {1-styles.UPanelTitleH}",
                            OffsetMin = $"{styles.ContentPad} {0}",
                            OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad-subtitleH-styles.ContentPad}"
                        }
                    }
                });
                var entryH = 0.09f;
                var entryT = 1f;
                foreach (var entry in col.Data)
                {
                    var entryId = ID(entId, "entry", true);
                    var checkColor = entry.IsProtected ? "0 0.6 0 1" : "1 0 0 1";
                    var checkIcon = entry.IsProtected ? "rp.check" : "rp.cross";
                    var iconSz = 12;
                    container.Add(new CuiElement
                    {
                        Parent = entId,
                        Name = entryId,
                        Components =
                        {
                            new CuiImageComponent
                            {
                                FadeIn = styles.OVPanelFadeIn,
                                Color = styles.BoxColor
                            },
                            new CuiRectTransformComponent
                            {
                                AnchorMin = $"{0} {entryT-entryH}",
                                AnchorMax = $"{1} {entryT}",
                                OffsetMin = $"{styles.ContentPad} {0}",
                                OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                            }
                        }
                    });
                    container.Add(new CuiElement
                    {
                        Parent = entryId,
                        Components =
                        {
                            new CuiImageComponent
                            {
                                FadeIn = styles.OVPanelFadeIn,
                                Png = ImageLibrary?.Call<string>("GetImage", checkIcon),
                                Color = checkColor
                            },
                            new CuiRectTransformComponent
                            {
                                AnchorMin = MIDDLE_LEFT,
                                AnchorMax = MIDDLE_LEFT,
                                OffsetMin = $"{styles.ContentPad + -iconSz/2} {-iconSz/2}",
                                OffsetMax = $"{styles.ContentPad + iconSz/2} {iconSz/2}"
                            }
                        }
                    });
                    container.Add(new CuiElement
                    {
                        Parent = entryId,
                        Components =
                        {
                            new CuiTextComponent
                            {
                                FadeIn = styles.OVPanelFadeIn,
                                Text = entry.Text,
                                FontSize = 12,
                                Color = styles.TextColor,
                                Align = UnityEngine.TextAnchor.MiddleRight
                            },
                            new CuiRectTransformComponent
                            {
                                OffsetMin = $"{styles.ContentPad} {0}",
                                OffsetMax = $"{-styles.ContentPad} {0}"
                            }
                        }
                    });
                    entryT -= entryH;
                }

                left += colw;
            }
            #endregion
            return container;
        }

        private CuiElementContainer CreateCostsPage(CuiElementContainer container, InfoPanelStyles styles, BasePlayer basePlayer, ProtectedCupboard tc)
        {
            // Balance
            var balanceid = ID(INFO_PANEL_CONTENT_ID, "balance");
            var balanceH = styles.UPanelTitleH * 2;
            container.Add(new CuiElement
            {
                Parent = INFO_PANEL_CONTENT_ID,
                Name = balanceid,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Color = styles.BoxColor
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0} {1f-balanceH}",
                        AnchorMax = $"{0.3} {1f}",
                        OffsetMin = $"{0} {0}",
                        OffsetMax = $"{-styles.ContentPad} {0}"
                    }
                }
            });
            container.Add(new CuiElement
            {
                Parent = balanceid,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Text = Lang("info balance label", basePlayer),
                        FontSize = 12,
                        Align = UnityEngine.TextAnchor.UpperCenter,
                        Color = styles.TextColor
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                        OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                    }
                }
            });
            container.Add(new CuiElement
            {
                Parent = balanceid,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Text = FormatCurrency((float)Math.Round(tc.Balance, 1)),
                        Align = UnityEngine.TextAnchor.LowerCenter,
                        Color = styles.TextColor,
                        FontSize = 22
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                        OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                    }
                }
            });
            // Balance Info
            var balanceinfoid = ID(INFO_PANEL_CONTENT_ID, "balanceinfo");
            container.Add(new CuiElement
            {
                Parent = INFO_PANEL_CONTENT_ID,
                Name = balanceinfoid,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Color = styles.BoxColor
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0} {0}",
                        AnchorMax = $"{0.3} {1f-balanceH}",
                        OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                        OffsetMax = $"{-styles.ContentPad*2} {-styles.ContentPad}"
                    }
                }
            });
            container.Add(new CuiElement
            {
                Parent = balanceinfoid,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Text = Lang("info balance info", basePlayer),
                        Align = UnityEngine.TextAnchor.UpperLeft,
                        Color = styles.SubtextColor,
                        FontSize = 10
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                        OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                    }
                }
            });
            // Title
            var titleId = ID(INFO_PANEL_CONTENT_ID, "title");
            var titleH = styles.UPanelTitleH * 2;
            container.Add(new CuiElement
            {
                Parent = INFO_PANEL_CONTENT_ID,
                Name = titleId,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Color = styles.BoxColor
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0.3} {1f-titleH}",
                        AnchorMax = $"{1f} {1f}",
                        OffsetMin = $"{styles.ContentPad} {0}",
                        OffsetMax = $"{0} {0}"
                    }
                }
            });
            container.Add(new CuiElement
            {
                Parent = titleId,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Text = Lang("info total hourly cost", basePlayer),
                        FontSize = 12,
                        Align = UnityEngine.TextAnchor.UpperCenter,
                        Color = styles.TextColor
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                        OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                    }
                }
            });
            container.Add(new CuiElement
            {
                Parent = titleId,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Text = $"{FormatCurrency(tc.TotalProtectionCostPerHour)}",
                        Align = UnityEngine.TextAnchor.LowerCenter,
                        Color = styles.TextColor,
                        FontSize = 22
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                        OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                    }
                }
            });
            var costs = new[]
            {
                new
                {
                    Text = Lang("info hourly base cost", basePlayer),
                    Note = Lang("info hourly base cost note", basePlayer, tc.HighestProtectionLevel.Rank),
                    Info = Lang("info hourly base cost info", basePlayer),
                    Value = $"{FormatCurrency(tc.BaseCostPerHour)}"
                },
                new
                {
                    Text = Lang("info hourly foundation cost", basePlayer),
                    Note = Lang("info hourly foundation cost note", basePlayer, tc.FoundationCount, FormatCurrency(tc.HighestProtectionLevel.HourlyCostPerFloor)),
                    Info = Lang("info hourly foundation cost info", basePlayer),
                    Value = $"{FormatCurrency(tc.BuildingCostPerHour)}"
                },
                new
                {
                    Text = Lang("info hourly owner cost", basePlayer),
                    Note = Lang("info hourly owner cost note", basePlayer, tc.Owners.Count, FormatCurrency(tc.HighestProtectionLevel.HourlyCostPerOwner)),
                    Info = Lang("info hourly owner cost info", basePlayer),
                    Value = $"{FormatCurrency(tc.OwnerCostPerHour)}"
                }
            };
            var panelH = (1f - titleH) / costs.Length;
            var t = 1f-titleH;
            var left = 0.3f;
            var panelW = (1f - left) / 2;
            foreach (var cost in costs)
            {
                var eid = ID(INFO_PANEL_CONTENT_ID, "entry", true);
                var eidright = ID(INFO_PANEL_CONTENT_ID, "entryright", true);
                // left
                container.Add(new CuiElement
                {
                    Parent = INFO_PANEL_CONTENT_ID,
                    Name = eid,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = styles.BoxColor
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = $"{left} {t-panelH}",
                            AnchorMax = $"{left+panelW} {t}",
                            OffsetMin = $"{styles.ContentPad*2} {styles.ContentPad}",
                            OffsetMax = $"{-styles.ContentPad/2} {-styles.ContentPad}"
                        }
                    }
                });
                // right
                container.Add(new CuiElement
                {
                    Parent = INFO_PANEL_CONTENT_ID,
                    Name = eidright,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = styles.BoxColor
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = $"{1f-panelW} {t-panelH}",
                            AnchorMax = $"{1f} {t}",
                            OffsetMin = $"{styles.ContentPad/2} {styles.ContentPad}",
                            OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                        }
                    }
                });
                // left stuff
                container.Add(new CuiElement
                {
                    Parent = eid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Text = cost.Text,
                            Align = UnityEngine.TextAnchor.UpperCenter,
                            Color = styles.TextColor,
                            FontSize = 10
                        },
                        new CuiRectTransformComponent
                        {
                            OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                            OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = eid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Text = cost.Note,
                            Align = UnityEngine.TextAnchor.MiddleCenter,
                            Color = styles.SubtextColor,
                            FontSize = 10
                        },
                        new CuiRectTransformComponent
                        {
                            OffsetMin = $"{styles.ContentPad} {styles.ContentPad*2}",
                            OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = eid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Text = cost.Value,
                            Align = UnityEngine.TextAnchor.LowerCenter,
                            Color = styles.TextColor,
                            FontSize = 18
                        },
                        new CuiRectTransformComponent
                        {
                            OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                            OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                        }
                    }
                });
                // right stuff
                container.Add(new CuiElement
                {
                    Parent = eidright,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Text = cost.Info,
                            Align = UnityEngine.TextAnchor.UpperCenter,
                            Color = styles.SubtextColor,
                            FontSize = 10
                        },
                        new CuiRectTransformComponent
                        {
                            OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                            OffsetMax = $"{-styles.ContentPad} {-styles.ContentPad}"
                        }
                    }
                });
                t -= panelH;
            }
            return container;
        }

        private void CloseInfoPanel(BasePlayer basePlayer)
        {
            CuiHelper.DestroyUi(basePlayer, INFO_PANEL_SHADOW_ID);
            CuiHelper.DestroyUi(basePlayer, INFO_PANEL_ID);
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        private void UILevelsShow(IPlayer player, string command, string[] args)
        {
            var basePlayer = player.Object as BasePlayer;
            if (basePlayer != null)
            {
                ShowLevels(basePlayer);
            }
        }

        [Command("lev.close")]
        private void UILevelsClose(IPlayer player, string command, string[] args)
        {
            var basePlayer = player.Object as BasePlayer;
            if (basePlayer != null)
            {
                CloseLevels(basePlayer);
            }
        }

        private readonly static string LEVELS_ID = "rp.levels";
        private readonly static string LEVELS_SHADOW_ID = "rp.levels.shadow";

        private void ShowLevels(BasePlayer basePlayer)
        {
            if (basePlayer == null) { return; }
            var styles = new InfoPanelStyles();
            var pl = ProtectionLevel.GetProtectionLevelOfPlayer(basePlayer);
            var w = 600;
            var h = 400;
            var container = new CuiElementContainer();
            container.Add(new CuiElement
            {
                Name = LEVELS_SHADOW_ID,
                Parent = "Overlay",
                Components =
                {
                    new CuiButtonComponent
                    {
                        Color = styles.ShadowColor,
                        Material = "assets/content/ui/uibackgroundblur-ingamemenu.mat",
                        Command = "lev.close"
                    }
                }
            });
            container.Add(new CuiElement
            {
                Name = LEVELS_ID,
                Parent = "Overlay",
                Components =
                {
                    new CuiNeedsCursorComponent{},
                    new CuiImageComponent
                    {
                        Color = RustColor.DarkGray
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = "0.5 0.5",
                        AnchorMax = "0.5 0.5",
                        OffsetMin = $"{-w/2} {-h/2}",
                        OffsetMax = $"{w/2} {h/2}"
                    }
                }
            });
            var titleh = 30;
            var title = ID(LEVELS_ID, "title");
            container.Add(new CuiElement
            {
                Name = title,
                Parent = LEVELS_ID,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Color = RustColor.DarkBrown
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = "0 1",
                        AnchorMax = "1 1",
                        OffsetMin = $"{0} {-titleh}",
                        OffsetMax = $"{0} {0}"
                    }
                }
            });
            var content = ID(LEVELS_ID, "content");
            container.Add(new CuiElement
            {
                Name = content,
                Parent = LEVELS_ID,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Color = TRANSPARENT
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = "0 0",
                        AnchorMax = "1 1",
                        OffsetMin = $"{styles.ContentPad} {styles.ContentPad}",
                        OffsetMax = $"{-styles.ContentPad} {-titleh-styles.ContentPad}"
                    }
                }
            });
            /* Title */
            container.Add(new CuiElement
            {
                Parent = title,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Text = Lang("title", basePlayer).TitleCase(),
                        Color = RustColor.LightBrown,
                        Align = UnityEngine.TextAnchor.MiddleCenter
                    }
                }
            });
            /* Close Button */
            var closebtn = ID(title, "close");
            container.Add(new CuiElement
            {
                Parent = title,
                Name = closebtn,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Png = ImageLibrary?.Call<string>("GetImage", "rp.cross"),
                        Color = styles.TextColor
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = BOTTOM_RIGHT,
                        AnchorMax = TOP_RIGHT,
                        OffsetMin = $"{-styles.HeaderHeight+styles.HeaderImgPad} {styles.HeaderImgPad}",
                        OffsetMax = $"{-styles.HeaderImgPad} {-styles.HeaderImgPad}"
                    }
                }
            });
            container.Add(new CuiElement
            {
                Parent = closebtn,
                Components =
                {
                    new CuiButtonComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Color = TRANSPARENT,
                        Command = "lev.close"
                    }
                }
            });
            /* Rank Text */
            container.Add(new CuiElement
            {
                Parent = content,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Text = Lang("levels protection level", basePlayer, pl.Rank),
                        Color = RustColor.LightBrown,
                        FontSize = 18,
                        Align = UnityEngine.TextAnchor.UpperCenter
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = $"{0} {0}",
                        OffsetMax = $"{0} {-10}"
                    }
                }
            });
            /* Description */
            container.Add(new CuiElement
            {
                Parent = content,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Text = Lang("levels description", basePlayer),
                        Color = RustColor.LightBrown,
                        FontSize = 12,
                        Align = UnityEngine.TextAnchor.UpperCenter
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = $"{0} {0}",
                        OffsetMax = $"{0} {-40}"
                    }
                }
            });
            /* Grid */
            var gridtop = 60;
            var grid = ID(LEVELS_ID, "grid");
            container.Add(new CuiElement
            {
                Name = grid,
                Parent = content,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.OVPanelFadeIn,
                        Color = TRANSPARENT
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = "0 0",
                        AnchorMax = "1 1",
                        OffsetMin = $"{0} {0}",
                        OffsetMax = $"{0} {-gridtop}"
                    }
                }
            });

            var panels = new[]
            {
                new
                {
                    Title = Lang("levels protection", basePlayer),
                    Values = new []
                    {
                        new
                        {
                            Label = Lang("levels online", basePlayer),
                            Value = $"{pl.OnlineProtectionPercent}%"
                        },
                        new
                        {
                            Label = Lang("levels offline", basePlayer),
                            Value = $"{pl.OfflineProtectionPercent}%"
                        },
                        new
                        {
                            Label = Lang("levels max hours", basePlayer),
                            Value = pl.MaxProtectionTimeHours?.ToString() ?? Lang("levels no limit", basePlayer)
                        }
                    }
                },
                new
                {
                    Title = Lang("levels delays", basePlayer),
                    Values = new []
                    {
                        new
                        {
                            Label = Lang("levels after offline", basePlayer),
                            Value = $"{pl.OfflineProtectionDelay}s"
                        },
                        new
                        {
                            Label = Lang("levels after damaged", basePlayer),
                            Value = $"{pl.ProtectedDelayAfterTakingDamage}s"
                        }
                    }
                },
                new
                {
                    Title = Lang("levels costs", basePlayer),
                    Values = new []
                    {
                        new
                        {
                            Label = Lang("levels base cost", basePlayer),
                            Value = FormatCurrency(pl.HourlyBaseCost)
                        },
                        new
                        {
                            Label = Lang("levels foundation cost", basePlayer),
                            Value = FormatCurrency(pl.HourlyCostPerFloor)
                        },
                        new
                        {
                            Label = Lang("levels owner cost", basePlayer),
                            Value = FormatCurrency(pl.HourlyCostPerOwner)
                        },
                        new
                        {
                            Label = Lang("levels damage cost", basePlayer),
                            Value = FormatCurrency(pl.CostPerDamageProtected)
                        }
                    }
                }
            };
            var panelw = 1f / panels.Length;
            var panelleft = 0f;
            foreach (var panel in panels)
            {
                var panelid = ID(grid, "panel", true);
                /* Panel Base */
                container.Add(new CuiElement
                {
                    Name = panelid,
                    Parent = grid,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = TRANSPARENT
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = Anchor(panelleft, 0),
                            AnchorMax = Anchor(panelleft+panelw, 1),
                            OffsetMin = Offset(styles.ContentPad, styles.ContentPad),
                            OffsetMax = Offset(-styles.ContentPad, -styles.ContentPad)
                        }
                    }
                });
                /* Panel Title Base */
                var panelbase = ID(panelid, "base");
                container.Add(new CuiElement
                {
                    Name = panelbase,
                    Parent = panelid,
                    Components =
                    {
                        new CuiImageComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = RustColor.DarkBrown
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = Anchor(0, 0.8f),
                            AnchorMax = Anchor(1, 1f),
                            OffsetMin = Offset(styles.ContentPad, styles.ContentPad),
                            OffsetMax = Offset(-styles.ContentPad, -styles.ContentPad)
                        }
                    }
                });
                /* Panel Title */
                container.Add(new CuiElement
                {
                    Parent = panelbase,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.OVPanelFadeIn,
                            Color = RustColor.LightBrown,
                            Text = panel.Title,
                            FontSize = 18,
                            Align = UnityEngine.TextAnchor.MiddleCenter
                        }
                    }
                });
                /* Subpanels */
                var subpanelt = 0.8f;
                //var subpanelh = subpanelt / panel.Values.Length;
                var subpanelh = 0.15f;
                var subpanelpad = styles.ContentPad / 2;
                foreach (var value in panel.Values)
                {
                    var subpanel = ID(panelid, "subpanel", true);
                    /* Box */
                    container.Add(new CuiElement
                    {
                        Name = subpanel,
                        Parent = panelid,
                        Components =
                        {
                            new CuiImageComponent
                            {
                                FadeIn = styles.OVPanelFadeIn,
                                Color = RustColor.DarkBrown
                            },
                            new CuiRectTransformComponent
                            {
                                AnchorMin = Anchor(0, subpanelt-subpanelh),
                                AnchorMax = Anchor(1, subpanelt),
                                OffsetMin = Offset(styles.ContentPad, subpanelpad),
                                OffsetMax = Offset(-styles.ContentPad, -subpanelpad)
                            }
                        }
                    });
                    /* Label */
                    container.Add(new CuiElement
                    {
                        Parent = subpanel,
                        Components =
                        {
                            new CuiTextComponent
                            {
                                FadeIn = styles.OVPanelFadeIn,
                                Color = RustColor.LightBrown,
                                Text = value.Label,
                                FontSize = 12,
                                Align = UnityEngine.TextAnchor.MiddleLeft
                            },
                            new CuiRectTransformComponent
                            {
                                OffsetMin = Offset(styles.ContentPad, 0),
                                OffsetMax = Offset(-styles.ContentPad, 0)
                            }
                        }
                    });
                    /* Value */
                    container.Add(new CuiElement
                    {
                        Parent = subpanel,
                        Components =
                        {
                            new CuiTextComponent
                            {
                                FadeIn = styles.OVPanelFadeIn,
                                Color = RustColor.LightBrown,
                                Text = value.Value,
                                FontSize = 12,
                                Align = UnityEngine.TextAnchor.MiddleRight
                            },
                            new CuiRectTransformComponent
                            {
                                OffsetMin = Offset(styles.ContentPad, 0),
                                OffsetMax = Offset(-styles.ContentPad, 0)
                            }
                        }
                    });
                    subpanelt -= subpanelh;
                }
                panelleft += panelw;
            }
            CloseLevels(basePlayer);
            CuiHelper.AddUi(basePlayer, container);
        }
        private void CloseLevels(BasePlayer basePlayer)
        {
            CuiHelper.DestroyUi(basePlayer, LEVELS_SHADOW_ID);
            CuiHelper.DestroyUi(basePlayer, LEVELS_ID);
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        private static class Security
        {
            public static Guid Token { get; private set; }

            public static void GenerateToken()
            {
                Token = Guid.NewGuid();
            }

            public static TokenResponse ValidateTokenArgs(string[] args)
            {
                var token = args[0];
                bool success = token == Token.ToString();
                if (!success)
                {
                    PLUGIN.PrintWarning("Attempted to call a secure command without a valid token");
                }
                return new TokenResponse
                {
                    Success = success,
                    Args = args.Skip(1).ToArray()
                };
            }

            public class TokenResponse
            {
                public bool Success;
                public string[] Args;
            }
        }
    }
}

namespace Oxide.Plugins
{
    partial class HomeProtection
    {
        [Command("tc.ui.withdraw")]
        private void TcWithdraw(IPlayer player, string command, string[] args)
        {
            var result = Security.ValidateTokenArgs(args);
            if (!result.Success) { return; } else { args = result.Args; }
            var basePlayer = player.Object as BasePlayer;
            if (IsNull(basePlayer)) return;
            var tc = ProtectedCupboardManager.GetByID(uint.Parse(args[0]));
            if (IsNull(tc)) return;
            var bal = tc.Balance;
            if (bal <= 0) return;
            GiveBalanceResource(basePlayer, bal);
            ClearProtectionBalance(tc);
            ShowProtectionStatusOverlay(basePlayer, tc);
        }

        [Command("tc.ui.balance")]
        private void TcBalance(IPlayer player, string command, string[] args)
        {
            var result = Security.ValidateTokenArgs(args);
            if (!result.Success) { return; } else { args = result.Args; }
            var basePlayer = player.Object as BasePlayer;
            float amt = float.Parse(args[0]);
            var tc = ProtectedCupboardManager.GetByID(uint.Parse(args[1]));
            if (tc == null) { return; }
            if (tc.HasProtectionTimeLimit)
            {
                amt = Math.Min(amt, tc.AllowedBalanceRemaining.Value);
            }
            if (ConsumeBalanceResource(tc, basePlayer, amt))
            {
                UpdateProtectionBalance(tc, amt, BalanceLedgerReason.Added);
                StartOrStopProtection(tc, () =>
                {
                    ShowProtectionStatusOverlay(basePlayer, tc);
                });
            }
        }

        [Command("tc.tab")]
        private void TcTab(IPlayer player, string command, string[] args)
        {
            var result = Security.ValidateTokenArgs(args);
            if (!result.Success) { return; } else { args = result.Args; }
            var basePlayer = player.Object as BasePlayer;
            if (IsNull(basePlayer)) return;
            var tc = ProtectedCupboardManager.GetByID(uint.Parse(args[0]));
            if (IsNull(tc)) return;
            var tab = int.Parse(args[1]);
            if (tab == 0)
            {
                CloseProtectionStatusOverlay(basePlayer);
                ShowProtectionStatusOverlayTabs(basePlayer, tc, 0);
            }
            if (tab == 1)
            {
                ShowProtectionStatusOverlay(basePlayer, tc);
                ShowProtectionStatusOverlayTabs(basePlayer, tc, 1);
            }
        }

        [Command("tc.toggle")]
        private void TcToggle(IPlayer player, string command, string[] args)
        {
            var result = Security.ValidateTokenArgs(args);
            if (!result.Success) { return; } else { args = result.Args; }
            var basePlayer = player.Object as BasePlayer;
            var priv = basePlayer.GetBuildingPrivilege();
            var tc = ProtectedCupboardManager.GetByID(uint.Parse(args[0]));
            if (tc == null) { return; }
            if (tc.Enabled)
            {
                tc.Enabled = false;
                StopProtection(tc);
                ShowProtectionStatusOverlay(basePlayer, tc);
            }
            else
            {
                Action callback = () =>
                {
                    if (basePlayer != null && tc != null && tc.CurrentlyViewing.Contains(basePlayer))
                    {
                        NextTick(() =>
                        {
                            ShowProtectionStatusOverlay(basePlayer, tc);
                        });
                    }
                };
                tc.Enabled = true;
                StartOrStopProtection(tc, callback);
            }
        }

        [Command("rp.info.show")]
        private void UIInfoShow(IPlayer player, string command, string[] args)
        {
            var result = Security.ValidateTokenArgs(args);
            if (!result.Success) { return; } else { args = result.Args; }
            var basePlayer = player.Object as BasePlayer;
            var netId = uint.Parse(args[0]);
            var tc = ProtectedCupboardManager.GetByID(netId);
            if (tc != null)
            {
                //ShowProtectionStatusInfo(basePlayer, tc);
            }
        }

        [Command("rp.info.close")]
        private void UIInfoClose(IPlayer player, string command, string[] args)
        {
            var result = Security.ValidateTokenArgs(args);
            if (!result.Success) { return; } else { args = result.Args; }
            var basePlayer = player.Object as BasePlayer;
            CloseProtectionStatusOverlay(basePlayer);
            CloseProtectionStatusOverlayTabs(basePlayer);
        }

        [Command("rp.ui.refresh")]
        private void UIRefresh(IPlayer player, string command, string[] args)
        {
            var result = Security.ValidateTokenArgs(args);
            if (!result.Success) { return; } else { args = result.Args; }
            var basePlayer = player.Object as BasePlayer;
            var netId = uint.Parse(args[0]);
            var tc = ProtectedCupboardManager.GetByID(netId);
            if (tc != null)
            {
                if (PlayersViewingOverlay.Contains(basePlayer.userID))
                {
                    ShowProtectionStatusOverlay(basePlayer, tc);
                }
            }
        }

        private static readonly string PROTECTION_STATUS_ID = "rp.status";
        private static readonly string PROTECTION_INFO_SHADOW_ID = "rp.info.shadow";
        private static readonly string PROTECTION_INFO_ID = "rp.info";
        private static readonly string PROTECTION_OVERLAY_ID = "rp.overlay";
        private static readonly string PROTECTION_OVERLAY_TABS_ID = "rp.overlay.tabs";
        private static readonly string PROTECTION_OVERLAY_BACKGROUND_ID = "rp.overlay.background";

        public class Styles
        {
            public string BackgroundColor = "0.31765 0.30588 0.27451 1";
            public string BackgroundColorShaded = "0.29412 0.27843 0.25490 1";
            public int TitleHeight = 20;
            public int MiddleHeight = 65;
            public int BottomHeight = 127;
            public int GapHeight = 4;
            public int TitleFontSize = 13;
            public int TitlePad = 8;
            public int InfoFontSize = 13;
            public int InfoPad = 7;
            public string InfoFontColor = "0.9 0.9 0.9 1";
            public int InfoIconSize = 60;
            public int BottomFontSize = 14;
            public int BottomPad = 7;
            public string WhiteColor = "0.87451 0.83529 0.80000 1";
            public string BlueColor = "0.08627 0.25490 0.38431 1";
            public string LightBlueColor = "0.25490 0.61176 0.86275 1";
            public string GrayColor = "0.45490 0.43529 0.40784 1";
            public string LightGrayColor = "0.69804 0.66667 0.63529 1";
            public string OrangeColor = "1.00000 0.53333 0.18039 1";
            public string RedColor = "0.52549 0.19608 0.14118 1";
            public string GreenColor = "0.25490 0.30980 0.14510 1";
            public string LightGreenColor = "0.76078 0.94510 0.41176 1";
            public string LightRedColor = "0.91373 0.77647 0.75686 1";
            public float FadeIn = 0f;
        }

        private CuiElementContainer CreateProtectionStatusOverlayBackground(CuiElementContainer container, BasePlayer basePlayer)
        {
            var styles = new Styles();
            var x = 192;
            var w = 381;
            var opacity = 1f;
            var offset = -3;
            var fadein = 0f;
            /* Base */
            container.Add(new CuiElement
            {
                Parent = "Hud",
                Name = PROTECTION_OVERLAY_BACKGROUND_ID,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = fadein,
                        Color = TRANSPARENT
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0.5} {0.50}",
                        AnchorMax = $"{0.5} {0.8055}",
                        OffsetMin = Offset(x, 1),
                        OffsetMax = Offset(x+w, 1)
                    }
                }
            });
            /* Top */
            var topid = ID(PROTECTION_OVERLAY_ID, "top");
            container.Add(new CuiElement
            {
                Parent = PROTECTION_OVERLAY_BACKGROUND_ID,
                Name = topid,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = fadein,
                        Color = Opacity(styles.BackgroundColor, opacity),
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = TOP_LEFT,
                        AnchorMax = TOP_RIGHT,
                        OffsetMin = Offset(offset, -styles.TitleHeight-1+offset),
                        OffsetMax = Offset(-offset, -offset),
                    }
                }
            });
            /* Middle */
            var middleid = ID(PROTECTION_OVERLAY_BACKGROUND_ID, "middle");
            container.Add(new CuiElement
            {
                Parent = PROTECTION_OVERLAY_BACKGROUND_ID,
                Name = middleid,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = fadein,
                        Color = Opacity(styles.BackgroundColor, opacity),
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = TOP_LEFT,
                        AnchorMax = TOP_RIGHT,
                        OffsetMin = Offset(0+offset, -styles.TitleHeight-styles.GapHeight-styles.MiddleHeight+offset),
                        OffsetMax = Offset(0-offset, -styles.TitleHeight-styles.GapHeight-offset),
                    }
                }
            });
            /* Bottom */
            var bottomid = ID(PROTECTION_OVERLAY_BACKGROUND_ID, "bottom");
            container.Add(new CuiElement
            {
                Parent = PROTECTION_OVERLAY_BACKGROUND_ID,
                Name = bottomid,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = fadein,
                        Color = Opacity(styles.BackgroundColor, opacity),
                        Material = "assets/icons/iconmaterial.mat",
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = BOTTOM_LEFT,
                        AnchorMax = BOTTOM_RIGHT,
                        OffsetMin = Offset(0+offset, -1+offset-2),
                        OffsetMax = Offset(0-offset, styles.BottomHeight-offset),
                    }
                }
            });
            return container;
        }

        private void ShowProtectionStatusOverlayTabs(BasePlayer basePlayer, ProtectedCupboard tc, int page = 0)
        {
            CuiElementContainer container = new CuiElementContainer();
            container = CreateProtectionStatusOverlayBackground(container, basePlayer);
            /* Base */
            var styles = new Styles();
            var x = 285;
            var w = 200;
            container.Add(new CuiElement
            {
                Parent = "Overlay",
                Name = PROTECTION_OVERLAY_TABS_ID,
                Components =
                {
                    new CuiImageComponent
                    {
                        Color = TRANSPARENT  
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0.5} {0.8055}",
                        AnchorMax = $"{0.5} {0.8055}",
                        OffsetMin = Offset(x, styles.GapHeight),
                        OffsetMax = Offset(x+w, styles.GapHeight+30)
                    }
                }
            });
            /* Tabs */
            var tabs = new[]
            {
                new
                {
                    Selected = page == 0,
                    Command = $"tc.tab {Security.Token} {tc.EntityID} {0}",
                    Text = Lang("ui tab upkeep", basePlayer)
                },
                new
                {
                    Selected = page == 1,
                    Command = $"tc.tab {Security.Token} {tc.EntityID} {1}",
                    Text = Lang("ui tab protection", basePlayer)
                }
            };
            var left = 0f;
            var tabw = 1f / tabs.Length;
            var gap = 4;
            foreach(var tab in tabs)
            {
                var tabid = ID(PROTECTION_OVERLAY_TABS_ID, "tab", true);
                container.Add(new CuiElement
                {
                    Parent = PROTECTION_OVERLAY_TABS_ID,
                    Name = tabid,
                    Components =
                    {
                        new CuiButtonComponent
                        {
                            Color = tab.Selected ? styles.BlueColor : styles.BackgroundColor,
                            Command = tab.Command
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = $"{left} {0}",
                            AnchorMax = $"{left+tabw} {1}",
                            OffsetMin = Offset(gap, 0),
                            OffsetMax = Offset(-gap, 0),
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = tabid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            Text = tab.Text,
                            Color = tab.Selected ? styles.LightBlueColor : styles.LightGrayColor,
                            Align = UnityEngine.TextAnchor.MiddleCenter
                        }
                    }
                });
                left += tabw;
            }

            CloseProtectionStatusOverlayTabs(basePlayer);
            CuiHelper.AddUi(basePlayer, container);
        }

        private void CloseProtectionStatusOverlayTabs(BasePlayer basePlayer)
        {
            CuiHelper.DestroyUi(basePlayer, PROTECTION_OVERLAY_TABS_ID);
            CuiHelper.DestroyUi(basePlayer, PROTECTION_OVERLAY_BACKGROUND_ID);
        }

        public static HashSet<ulong> PlayersViewingOverlay = new HashSet<ulong>();

        private void ShowProtectionStatusOverlay(BasePlayer basePlayer, ProtectedCupboard tc)
        {
            CuiElementContainer container = new CuiElementContainer();

            var styles = new Styles();
            var x = 192;
            var w = 381;
            var lsoffset = 0.4f;
            var rsoffset = -0.7f;
            /* Base */
            container.Add(new CuiElement
            {
                Parent = "Overlay",
                Name = PROTECTION_OVERLAY_ID,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.FadeIn,
                        Color = TRANSPARENT
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = $"{0.5} {0.50}",
                        AnchorMax = $"{0.5} {0.8055}",
                        OffsetMin = Offset(x, 1),
                        OffsetMax = Offset(x+w, 1)
                    }
                }
            });
            /* Top */
            var topid = ID(PROTECTION_OVERLAY_ID, "top");
            container.Add(new CuiElement
            {
                Parent = PROTECTION_OVERLAY_ID,
                Name = topid,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.FadeIn,
                        Color = styles.BackgroundColorShaded,
                        Material = "assets/icons/greyout.mat"
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = TOP_LEFT,
                        AnchorMax = TOP_RIGHT,
                        OffsetMin = Offset(lsoffset, -styles.TitleHeight-1),
                        OffsetMax = Offset(rsoffset, 0),
                    }
                }
            });
            /* Top - Title */
            container.Add(new CuiElement
            {
                Parent = topid,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.FadeIn,
                        Text = Lang("title", basePlayer).ToUpper(),
                        FontSize = styles.TitleFontSize,
                        Color = styles.WhiteColor,
                        Align = UnityEngine.TextAnchor.MiddleLeft
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = Offset(styles.TitlePad, 0),
                        OffsetMax = Offset(-styles.TitlePad, 0),
                    }
                }
            });
            if (IsAdmin(basePlayer))
            {
                /* Top - Info */
                container.Add(new CuiElement
                {
                    Parent = topid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.FadeIn,
                            Text = Lang("tc id", basePlayer, tc.EntityID),
                            FontSize = 12,
                            Color = "1 1 1 0.2",
                            Align = UnityEngine.TextAnchor.MiddleRight
                        },
                        new CuiRectTransformComponent
                        {
                            OffsetMin = Offset(styles.TitlePad, 0),
                            OffsetMax = Offset(-styles.TitlePad, 0),
                        }
                    }
                });
            }
            /* Middle */
            var middleid = ID(PROTECTION_OVERLAY_ID, "middle");
            container.Add(new CuiElement
            {
                Parent = PROTECTION_OVERLAY_ID,
                Name = middleid,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.FadeIn,
                        Color = styles.BackgroundColorShaded,
                        Material = "assets/icons/greyout.mat"
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = TOP_LEFT,
                        AnchorMax = TOP_RIGHT,
                        OffsetMin = Offset(lsoffset, -styles.TitleHeight-styles.GapHeight-styles.MiddleHeight),
                        OffsetMax = Offset(rsoffset, -styles.TitleHeight-styles.GapHeight-1),
                    }
                }
            });
            /* Middle - Icon */
            var imgp = 12;
            var xoff = 6;
            container.Add(new CuiElement
            {
                Parent = middleid,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.FadeIn,
                        Png = GetImage("status.protected"),
                        Color = styles.GrayColor
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = MIDDLE_LEFT,
                        AnchorMax = MIDDLE_LEFT,
                        OffsetMin = Offset(styles.InfoPad+xoff, -styles.InfoIconSize/2+imgp),
                        OffsetMax = Offset(styles.InfoPad+styles.InfoIconSize-(2*imgp)+xoff, styles.InfoIconSize/2-imgp),
                    }
                }
            });
            /* Middle - Text */
            container.Add(new CuiElement
            {
                Parent = middleid,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.FadeIn,
                        Text = Lang("ui info text", basePlayer),
                        Font = "RobotoCondensed-Regular.ttf",
                        FontSize = styles.InfoFontSize,
                        Color = styles.LightGrayColor,
                        Align = UnityEngine.TextAnchor.UpperLeft
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = Offset(styles.InfoPad+styles.InfoIconSize, styles.InfoPad),
                        OffsetMax = Offset(-styles.InfoPad-4, -styles.InfoPad),
                    }
                }
            });
            /* Bottom */
            var bottomid = ID(PROTECTION_OVERLAY_ID, "bottom");
            container.Add(new CuiElement
            {
                Parent = PROTECTION_OVERLAY_ID,
                Name = bottomid,
                Components =
                {
                    new CuiImageComponent
                    {
                        FadeIn = styles.FadeIn,
                        Color = Opacity(styles.BackgroundColorShaded, 1f),
                        Material = "assets/icons/greyout.mat"
                    },
                    new CuiRectTransformComponent
                    {
                        AnchorMin = BOTTOM_LEFT,
                        AnchorMax = BOTTOM_RIGHT,
                        OffsetMin = Offset(lsoffset, -1),
                        OffsetMax = Offset(rsoffset, styles.BottomHeight),
                    }
                }
            });
            /* Bottom - Cost Title */
            container.Add(new CuiElement
            {
                Parent = bottomid,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.FadeIn,
                        Text = Lang("ui cost per hour", basePlayer),
                        FontSize = styles.BottomFontSize,
                        Color = styles.OrangeColor,
                        Align = UnityEngine.TextAnchor.UpperCenter
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = Offset(styles.BottomPad-1, styles.BottomPad),
                        OffsetMax = Offset(-styles.BottomPad, -styles.BottomPad+1),
                    }
                }
            });
            /* Bottom - Max Time */
            if (tc.IsAtMaxBalance)
            {
                container.Add(new CuiElement
                {
                    Parent = bottomid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.FadeIn,
                            Text = Lang("ui max time reached", basePlayer),
                            FontSize = 10,
                            Color = RustColor.LightGreen,
                            Align = UnityEngine.TextAnchor.MiddleCenter
                        },
                        new CuiRectTransformComponent
                        {
                            OffsetMin = Offset(-20, -60),
                            OffsetMax = Offset(20, 0),
                        }
                    }
                });
            }
            /* Bottom - Damage Cost Text */
            if (tc.IsProtected && tc.CostPerDamageProtected > 0)
            {
                var msgoffset = -20;
                container.Add(new CuiElement
                {
                    Parent = bottomid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.FadeIn,
                            Text = Lang("ui damage cost", basePlayer),
                            FontSize = 9,
                            Color = RustColor.LightOrange,
                            Align = UnityEngine.TextAnchor.UpperCenter
                        },
                        new CuiRectTransformComponent
                        {
                            OffsetMin = Offset(styles.BottomPad-2, styles.BottomPad+3+msgoffset),
                            OffsetMax = Offset(-styles.BottomPad, -styles.BottomPad+msgoffset),
                        }
                    }
                });
            }            
            /* Bottom - Balance Text */
            if (config.Integration.Economics || config.Integration.ServerRewards)
            {
                var msgoffset = -10;
                container.Add(new CuiElement
                {
                    Parent = bottomid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.FadeIn,
                            Text = Lang("ui your balance", basePlayer, FormatCurrency((float)GetBalanceResourceAmount(tc, basePlayer))),
                            FontSize = 10,
                            Color = RustColor.LightGray,
                            Align = UnityEngine.TextAnchor.MiddleCenter
                        },
                        new CuiRectTransformComponent
                        {
                            OffsetMin = Offset(styles.BottomPad-2, styles.BottomPad+3+msgoffset),
                            OffsetMax = Offset(-styles.BottomPad, -styles.BottomPad+msgoffset),
                        }
                    }
                });
            }
            /* Bottom - Protected Title */
            var text = tc.IsProtected ? Lang("ui protected", basePlayer, tc.CurrentProtectionPercent, FormatTimeShort(tc.HoursOfProtection)) : StatusReasonToString(tc, basePlayer, tc.Reason);
            container.Add(new CuiElement
            {
                Parent = bottomid,
                Components =
                {
                    new CuiTextComponent
                    {
                        FadeIn = styles.FadeIn,
                        Text = text,
                        FontSize = styles.BottomFontSize,
                        Color = tc.IsProtected ? styles.LightGreenColor : styles.OrangeColor,
                        Align = UnityEngine.TextAnchor.LowerCenter
                    },
                    new CuiRectTransformComponent
                    {
                        OffsetMin = Offset(styles.BottomPad-2, styles.BottomPad+3),
                        OffsetMax = Offset(-styles.BottomPad, -styles.BottomPad),
                    }
                }
            });
            /* Bottom - Cost Text */
            if (config.Integration.ServerRewards || config.Integration.Economics)
            {
                container.Add(new CuiElement
                {
                    Parent = bottomid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.FadeIn,
                            Text = $"{FormatCurrency(tc.TotalProtectionCostPerHour)}",
                            FontSize = 16,
                            Align = UnityEngine.TextAnchor.LowerCenter
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = MIDDLE_CENTER,
                            AnchorMax = MIDDLE_CENTER,
                            OffsetMin = Offset(-40, 0),
                            OffsetMax = Offset(40, 28),
                        }
                    }
                });
            }
            else
            {
                container.Add(new CuiElement
                {
                    Parent = bottomid,
                    Components =
                    {
                        new CuiRawImageComponent
                        {
                            FadeIn = styles.FadeIn,
                            Png = ImageLibrary?.Call<string>("GetImage", config.Protection.CurrencyItem.ToLower())
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = MIDDLE_CENTER,
                            AnchorMax = MIDDLE_CENTER,
                            OffsetMin = Offset(-14-1, -14+3),
                            OffsetMax = Offset(14-1, 14+3),
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = bottomid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.FadeIn,
                            Text = $"x{tc.TotalProtectionCostPerHour}",
                            FontSize = 12,
                            Color = styles.LightGrayColor,
                            Align = UnityEngine.TextAnchor.LowerCenter
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = MIDDLE_CENTER,
                            AnchorMax = MIDDLE_CENTER,
                            OffsetMin = Offset(-84, -22),
                            OffsetMax = Offset(100, -20+40),
                        }
                    }
                });
            }
            /* Bottom - Buttons */
            bool allowMaxDeposit = config.Protection.AllowMaxDeposit;
            var maxHours = 0;
            if (allowMaxDeposit)
            {
                var amountInPossesion = GetBalanceResourceAmount(tc, basePlayer);
                maxHours = tc.TotalProtectionCostPerHour <= 0 ? 0 : (int)Math.Floor(amountInPossesion / tc.TotalProtectionCostPerHour.Value);
            }
            var rightButtons = new[]
            {
                new
                {
                    Text = Lang("ui button 1h", basePlayer),
                    Command = $"tc.ui.balance {Security.Token} {tc.TotalProtectionCostPerHour} {tc.EntityID}",
                    Visible = !tc.HasFreeProtection,
                    Enabled = !tc.HasFreeProtection && HasBalanceResourceAmount(tc, basePlayer, tc.TotalProtectionCostPerHour.Value) && !tc.IsAtMaxBalance
                },
                new
                {
                    Text = Lang("ui button 24h", basePlayer),
                    Command = $"tc.ui.balance {Security.Token} {tc.TotalProtectionCostPerHour*24} {tc.EntityID}",
                    Visible = !tc.HasFreeProtection,
                    Enabled = !tc.HasFreeProtection && HasBalanceResourceAmount(tc, basePlayer, tc.TotalProtectionCostPerHour.Value*24) && !tc.IsAtMaxBalance
                },
                new
                {
                    Text = Lang("ui button max", basePlayer),
                    Command = $"tc.ui.balance {Security.Token} {tc.TotalProtectionCostPerHour*maxHours} {tc.EntityID}",
                    Visible = !tc.HasFreeProtection && allowMaxDeposit,
                    Enabled = !tc.HasFreeProtection && HasBalanceResourceAmount(tc, basePlayer, tc.TotalProtectionCostPerHour.Value) && !tc.IsAtMaxBalance
                }
            };
            var leftButtons = new[]
            {
                new
                {
                    Text = Lang("ui button info", basePlayer),
                    Command = $"tc.info.open {Security.Token} {tc.EntityID} 0",
                    Color = styles.GrayColor,
                    TextColor = styles.WhiteColor,
                    Visible = true,
                    Enabled = true
                },
                new
                {
                    Text = tc.Enabled ? Lang("ui button pause", basePlayer) : Lang("ui button resume", basePlayer),
                    Command = $"tc.toggle {Security.Token} {tc.EntityID}",
                    Color = tc.Enabled ? styles.GrayColor : styles.RedColor,
                    TextColor = tc.Enabled ? styles.WhiteColor : styles.LightRedColor,
                    Visible = config.Protection.AllowProtectionPause,
                    Enabled = true
                },
                new
                {
                    Text = Lang("ui button clear", basePlayer),
                    Command = $"tc.ui.withdraw {Security.Token} {tc.EntityID}",
                    Color = styles.GrayColor,
                    TextColor = styles.WhiteColor,
                    Visible = config.Protection.AllowBalanceWithdraw,
                    Enabled = tc.Balance > 0
                }
            };

            var topoffset = 0;
            var btnh = 20;
            var btnw = 60;
            var btngap = 6;
            foreach(var button in rightButtons)
            {
                if (!button.Visible) { continue; }
                var bid = ID(bottomid, "btn", true);
                container.Add(new CuiElement
                {
                    Parent = bottomid,
                    Name = bid,
                    Components =
                    {
                        new CuiButtonComponent
                        {
                            FadeIn = styles.FadeIn,
                            Color = button.Enabled ? styles.GrayColor : Opacity(styles.GrayColor, 0.6f),
                            Command = button.Enabled ? button.Command : String.Empty
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = TOP_RIGHT,
                            AnchorMax = TOP_RIGHT,
                            OffsetMin = Offset(-btnw-styles.BottomPad, topoffset-btnh-styles.BottomPad),
                            OffsetMax = Offset(-styles.BottomPad, topoffset-styles.BottomPad),
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = bid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.FadeIn,
                            Text = button.Text,
                            FontSize = 12,
                            Color = button.Enabled ? styles.WhiteColor : Opacity(styles.WhiteColor, 0.6f),
                            Align = UnityEngine.TextAnchor.MiddleCenter
                        }
                    }
                });
                topoffset -= btnh + btngap;
            }
            topoffset = 0;
            foreach (var button in leftButtons)
            {
                if (!button.Visible) { continue; }
                var bid = ID(bottomid, "btn", true);
                container.Add(new CuiElement
                {
                    Parent = bottomid,
                    Name = bid,
                    Components =
                    {
                        new CuiButtonComponent
                        {
                            FadeIn = styles.FadeIn,
                            Color = button.Enabled ? button.Color : Opacity(button.Color, 0.6f),
                            Command = button.Enabled ? button.Command : String.Empty
                        },
                        new CuiRectTransformComponent
                        {
                            AnchorMin = TOP_LEFT,
                            AnchorMax = TOP_LEFT,
                            OffsetMin = Offset(styles.BottomPad, topoffset-btnh-styles.BottomPad),
                            OffsetMax = Offset(styles.BottomPad+btnw, topoffset-styles.BottomPad),
                        }
                    }
                });
                container.Add(new CuiElement
                {
                    Parent = bid,
                    Components =
                    {
                        new CuiTextComponent
                        {
                            FadeIn = styles.FadeIn,
                            Text = button.Text,
                            FontSize = 12,
                            Color = button.Enabled ? button.TextColor : Opacity(button.TextColor, 0.6f),
                            Align = UnityEngine.TextAnchor.MiddleCenter
                        }
                    }
                });
                topoffset -= btnh + btngap;
            }

            CloseProtectionStatusOverlay(basePlayer);
            CuiHelper.AddUi(basePlayer, container);
            PlayersViewingOverlay.Add(basePlayer.userID);
        }

        private void CloseProtectionStatusOverlay(BasePlayer basePlayer)
        {
            CuiHelper.DestroyUi(basePlayer, PROTECTION_OVERLAY_ID);
            PlayersViewingOverlay.Remove(basePlayer.userID);
        }

        private class ProtectedCupboardStatusInfo
        {
            public string Status;
            public string Reason;
            public string Protection;
            public string MaxProtectionTime;
            public string Balance;
            public string ProtectionTime;
            public string Costs;
            public string CostPerDamageTaken;
            public string Owners;
            public string ProtectionLevel;
        }
    }
}
