import discord
from discord.ext import commands
import json
import os
from discord import ui
from discord.utils import get

### Logging Types ###
Enable_Accept_Logging = True
Enable_Deny_Logging = True
Enable_New_Suggestion_Logging = True

config = json.load(open(os.path.join(os.path.dirname(__file__), "config.json")))
currentmessageid = 0
intents = discord.Intents.all()


class Bot(commands.Bot):
    def __init__(self):
        super().__init__(command_prefix="!", intents=intents)

    async def setup_hook(self):
        bot.tree.copy_global_to(guild=discord.Object(id=int(config["Guild ID"])))
        await bot.tree.sync(guild=discord.Object(id=int(config["Guild ID"])))


bot = Bot()
bot.remove_command("help")


@bot.event
async def on_ready():
    print(f"Bot is logged in as {bot.user}")
    print("Made by Middel#9816")
    print("--------------------------------")