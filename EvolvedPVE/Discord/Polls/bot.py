import discord
from discord.ext import commands
import json
import os
from discord import ui
from discord.utils import get

Poll_Embed_Color = 0x0000ff

config = json.load(open(os.path.join(os.path.dirname(__file__), "config.json")))
currentmessageid = 0
intents = discord.Intents.all()


class Bot(commands.Bot):

    __version__ = "1.0.2"

    def __init__(self):
        super().__init__(command_prefix="!", intents=intents)

    async def setup_hook(self):
        bot.tree.copy_global_to(guild=discord.Object(id=int(config["Guild ID"])))
        await bot.tree.sync(guild=discord.Object(id=int(config["Guild ID"])))

    bot = Bot()
    bot.remove_command("help")

    @bot.event
    async def on_ready():
        print(f"Bot is logged in as {bot.user}")
        print("Made by: Skillz")
        print("--------------------------------")

    @commands.has_role(int(config["Polls Access Role ID"]))
    @bot.hybrid_command(name="poll", description="Create a poll! Made by: Skillz", with_app_command=True)
    async def poll(ctx, poll_message: str, option_1: str, option_2: str, option_3: str = None, option_4: str = None, option_5: str = None, option_6: str = None, option_7: str = None, option_8: str = None, option_9: str = None):
        options = [option_1, option_2, option_3, option_4, option_5, option_6, option_7, option_8, option_9]
        options = [x for x in options if x is not None]
        embed=discord.Embed(title=poll_message, description="\n".join([f"{i+1}. {x}" for i, x in enumerate(options)]), color=0x0000FF)
        embed.set_footer(text=f"Poll created by: {ctx.author.display_name}{ctx.author.discriminator}", icon_url=ctx.author.avatar.url)
        embed.timestamp=discord.utils.utcnow()
        message = await ctx.channel.send(embed=embed)
        for i in range(len(options)):
            await message.add_reaction(f"{i+1}\N{combining enclosing keycap}")

        await ctx.reply(embed=discord.Embed(description="Poll created!", color=0x00FF00), ephemeral=True)

    @bot.event
    async def on_command_error(ctx, error):
        if isinstance(error, commands.CommandOnCooldown):
            print(error)
            await ctx.reply(embed=discord.Embed(description=f"Please wait {error.retry_after:.2f} seconds before using this command again. Cooldowns are in place to prevent spamming in case unauthorised users get access to run this command on accident or through other means.", color=0xff0000), ephemeral=True)
            return
        if isinstance(error, commands.MissingRole):
            print(error)
            await ctx.reply(embed=discord.Embed(description=f"You do not have permission to use this command.", color=0xff0000), ephemeral=True)
            return
        if isinstance(error, commands.MissingRequiredArgument):
            print(error)
            await ctx.reply(embed=discord.Embed(description=f"Please provide all required arguments.", color=0xff0000), ephemeral=True)
            return
        if isinstance(error, commands.BadArgument):
            print(error)
            await ctx.reply(embed=discord.Embed(description=f"Please provide a valid argument.", color=0xff0000), ephemeral=True)
            return
        if isinstance(error, commands.CommandNotFound):
            print(error)
            await ctx.reply(embed=discord.Embed(description=f"Command not found.", color=0xff0000), ephemeral=True)
            return
        if isinstance(error, commands.BotMissingPermissions):
            print(error)
            await ctx.reply(embed=discord.Embed(description=f"Bot is missing permissions.", color=0xff0000), ephemeral=True)
            return
        if isinstance(error, commands.MissingPermissions):
            print(error)
            await ctx.reply(embed=discord.Embed(description=f"You are missing permissions.", color=0xff0000), ephemeral=True)
            return
        if isinstance(error, commands.CommandInvokeError):
            print(error)
            await ctx.reply(embed=discord.Embed(description=f"An error has occured.", color=0xff0000), ephemeral=True)
            return
        if isinstance(error, commands.CheckFailure):
            print(error)
            await ctx.reply(embed=discord.Embed(description=f"You do not have permission to use this command.", color=0xff0000), ephemeral=True)
            return
        if isinstance(error, commands.CommandError):
            print(error)
            await ctx.reply(embed=discord.Embed(description=f"An error has occured.", color=0xff0000), ephemeral=True)
            return
        if isinstance(error, commands.UserInputError):
            print(error)
            await ctx.reply(embed=discord.Embed(description=f"Please provide a valid argument.", color=0xff0000), ephemeral=True)
            return
        if isinstance(error, commands.DisabledCommand):
            print(error)
            await ctx.reply(embed=discord.Embed(description=f"This command is disabled.", color=0xff0000), ephemeral=True)
            return


bot.run(config["Bot Token"])